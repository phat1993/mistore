<?php

namespace Database\Factories;

use App\Models\ShopMenu;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShopMenuFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShopMenu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
