<?php

namespace Database\Factories;

use App\Models\ShopGift;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShopGiftFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShopGift::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
