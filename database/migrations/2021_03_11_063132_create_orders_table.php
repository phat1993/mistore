<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->Integer('order_state_id');
            $table->Integer('order_gift_id')->nullable();
            $table->string('order_gift', 100)->nullable();
            $table->decimal('order_ship_fee', 18, 0);
            $table->decimal('order_gift_discount', 18, 0)->nullable();//order_gift(discount)
            $table->decimal('order_discount', 18, 0)->nullable();//order_detail(discount)
            $table->decimal('order_tax', 18, 0)->nullable();
            $table->decimal('order_total', 18, 0);//order_detail(total)
            $table->decimal('order_amount', 18, 0);//order_detail(amount)+shipfee-order_gift(discount)

            $table->Integer('customer_id')->nullable();
            $table->string('order_full_name', 100)->nullable();
            $table->string('order_email', 50)->nullable();
            $table->string('order_phone', 20)->nullable();
            $table->string('order_address', 200)->nullable();
            $table->string('order_delivery_time',200)->nullable();
            $table->string('order_note', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_orders');
    }
}
