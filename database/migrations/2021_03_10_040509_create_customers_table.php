<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_customers', function (Blueprint $table) {
            $table->increments('customer_id');
            $table->string('customer_user', 100);//Lay phone hoac email de dang nhap
            $table->string('customer_password', 100);
            $table->string('customer_avatar', 100)->nullable();
            $table->string('customer_full_name', 100)->nullable();
            $table->string('customer_gender', 20)->nullable();
            $table->timestamp('customer_brithday')->nullable();
            $table->string('customer_email', 50)->unique()->nullable();
            $table->string('customer_phone', 20)->unique()->nullable();
            $table->string('customer_address', 200)->nullable();
            $table->dateTime('last_login_time')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_customers');
    }
}
