<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_administrators', function (Blueprint $table) {
            $table->increments('admin_id');
            $table->string('admin_official_name', 100);
            $table->string('admin_supplement_name', 100)->nullable();
            $table->string('admin_abbreviation', 100);
            $table->string('admin_email', 50);
            $table->string('admin_address', 200);
            $table->string('admin_tel1', 20);
            $table->string('admin_tel2', 20)->nullable();
            $table->string('special_notes', 200)->nullable();
            $table->string('admin_avatar', 100);
            $table->string('user_name', 30);
            $table->string('password', 100);
            $table->string('old_password', 100)->nullable();
            $table->dateTime('last_login_time')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_administrators');
    }
}
