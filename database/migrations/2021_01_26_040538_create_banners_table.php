<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_banners', function (Blueprint $table) {
            $table->increments('banner_id');
            $table->string('banner_title', 100);
            $table->string('banner_url', 500);
            $table->text('banner_description')->nullable();
            $table->string('banner_img', 100)->nullable();
            $table->string('banner_link', 500)->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_banners');
    }
}
