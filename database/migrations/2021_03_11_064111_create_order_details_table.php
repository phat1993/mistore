<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_order_details', function (Blueprint $table) {
            $table->increments('order_detail_id');
            $table->Integer('order_id');
            $table->Integer('product_id');
            $table->Integer('order_detail_quantity');
            $table->decimal('order_detail_price', 18, 0)->nullable();//luu lai gia hien tai cua san pham
            $table->decimal('order_detail_discount', 18, 0)->nullable();//luu lai so tien giam gia (price - discount)*quantity
            $table->decimal('order_detail_total', 18, 0);//price*quantity
            $table->decimal('order_detail_amount', 18, 0);//total - discont
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_order_details');
    }
}
