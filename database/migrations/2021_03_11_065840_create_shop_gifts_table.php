<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_shop_gifts', function (Blueprint $table) {
            $table->increments('gift_id');
            $table->string('gift_code',100);
            $table->decimal('gift_discount', 18, 0);
            $table->tinyInteger('gift_pattern')->default(1);//cho biet tinh theo % hay gia tien 0:%, 1: tien
            $table->timestamp('gift_start_time');//thoi gian bat dau
            $table->timestamp('gift_end_time');//thoi gian khuyen mai
            $table->tinyInteger('gift_use_count')->default(1);//so lan su dung(khi khach hang nhap code thi 1 ma chi danh cho 1 phone nhat dinh.)
            $table->tinyInteger('gift_used')->default(0);//luu lai so lan su dung khi == voi count thi deactive
            $table->string('gift_note', 500)->nullable();
            $table->tinyInteger('active_status')->default(1);//khi het so lan su dung thi tu dong tat
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_shop_gifts');
    }
}
