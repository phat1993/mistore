<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_product_reviews', function (Blueprint $table) {
            $table->increments('product_review_id');
            $table->string('product_review_ip',100);
            $table->string('product_review_avatar', 100);
            $table->tinyInteger('product_review_star');
            $table->string('product_review_name', 100);
            $table->string('product_review_mail', 100);
            $table->string('product_review_description', 300);
            $table->Integer('product_id');
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_product_reviews');
    }
}
