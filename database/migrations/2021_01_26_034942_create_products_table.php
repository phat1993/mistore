<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('ms_products', function (Blueprint $table) {
//            $table->increments('product_id');
//            $table->string('product_name', 100);
//            $table->decimal('product_price', 18, 0);
//            $table->decimal('product_discount', 18, 0)->nullable()->default(-1);
//            $table->string('product_description', 120)->nullable();
//            $table->Integer('product_view_count');
//            $table->text('product_detail');
//            $table->string('product_img_1', 100)->nullable();
//            $table->string('product_img_2', 100)->nullable();
//            $table->string('product_img_3', 100)->nullable();
//            $table->string('product_img_4', 100)->nullable();
//            $table->string('product_img_5', 100)->nullable();
//            $table->string('product_img_6', 100)->nullable();
//            $table->string('product_img_7', 100)->nullable();
//            $table->string('product_video', 1000)->nullable();
//            //Seo
//            $table->string('web_keywords', 200)->nullable();
//            $table->string('web_description', 145)->nullable();
//            $table->string('web_title', 20)->nullable();
//            $table->string('web_canonical', 500)->nullable();
//            $table->Integer('product_category_id');
//            $table->tinyInteger('active_status')->default(1);
//            $table->softDeletes();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('ms_products');
    }
}
