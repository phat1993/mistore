<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_product_types', function (Blueprint $table) {
            $table->increments('product_type_id');
            $table->string('product_type_name', 100);
            $table->string('product_type_description', 200)->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->string('web_keywords', 200)->nullable();
            $table->string('web_description', 145)->nullable();
            $table->string('web_title', 20)->nullable();
            $table->string('web_canonical', 200)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_product_types');
    }
}
