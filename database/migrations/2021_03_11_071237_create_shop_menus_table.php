<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_shop_menus', function (Blueprint $table) {
            $table->increments('shop_menu_id');
            $table->tinyInteger('shop_menu_position');
            $table->string('shop_menu_name',100);
            $table->string('shop_menu_link',100)->nullable();
            $table->Integer('product_type_id')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_shop_menus');
    }
}
