<?php

namespace Database\Seeders;

use App\Models\Entities\ShopMenu;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ms_shop_menus')->delete();

        $types = [
            [
                'shop_menu_id' => '1',
                'shop_menu_name' => 'TÚI XÁCH',
                'shop_menu_position'     => '1',
                'shop_menu_link'     => '',
                'product_type_id'     => '1',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'shop_menu_id' => '2',
                'shop_menu_name' => 'BALO',
                'shop_menu_position'     => '2',
                'shop_menu_link'     => '',
                'product_type_id'     => '2',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'shop_menu_id' => '3',
                'shop_menu_name' => 'GIÀY DÉP',
                'shop_menu_position'     => '3',
                'shop_menu_link'     => '',
                'product_type_id'     => '3',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'shop_menu_id' => '4',
                'shop_menu_name' => 'VÍ CẦM TAY',
                'shop_menu_position'     => '4',
                'shop_menu_link'     =>'',
                'product_type_id'     => '4',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'shop_menu_id' => '5',
                'shop_menu_name' => 'MẮT KÍNH',
                'shop_menu_position'     => '5',
                'shop_menu_link'     => '',
                'product_type_id'     => '5',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        ShopMenu::insert($types);
    }
}
