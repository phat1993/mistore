<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Entities\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ms_order_states')->delete();

        $types = [
            [
                'order_state_id' => '1',
                'order_state_value' => 'Chờ nhân viên kiểm tra',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '2',
                'order_state_value' => 'Đang lấy hàng',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '3',
                'order_state_value' => 'Đang giao hàng',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '4',
                'order_state_value' => 'Đã giao hàng',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '5',
                'order_state_value' => 'Đơn hàng bị hủy',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '6',
                'order_state_value' => 'Trả Hàng',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'order_state_id' => '7',
                'order_state_value' => 'Hết Hàng',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        DB::table('ms_order_states')->insert($types);
    }
}
