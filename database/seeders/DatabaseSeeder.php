<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(AdministratorSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(OrderStateSeeder::class);
        $this->call(ShopMenuSeeder::class);
    }
}
