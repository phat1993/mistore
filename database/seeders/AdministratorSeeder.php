<?php

namespace Database\Seeders;

use App\Models\Entities\Administrator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::create(
            [
                'admin_official_name'     => 'Phạm Văn Trung',
                'admin_supplement_name'     => 'Trung IT',
                'admin_abbreviation'     => 'Phạm Trung',
                'admin_email'     => 'mail@gmail.com',
                'admin_address'     => 'Số 22/2/24 Đường Cao Tốc P.10 Quận Tân Bình, Tp HCM',
                'admin_tel1'     => '0909066066',
                'admin_tel2'     => '012301230123',
                'special_notes'     => 'Never give up',
                'admin_avatar'     => 'images/avatar-7.jpg',
                'user_name'     => 'trung001',
                'password'     => Hash::make('123'),
                'old_password'     => Hash::make('123'),
                'active_status'     => 1,
                'remember_token' => bcrypt(10)
            ]
        );
    }
}
