<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Entities\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ms_product_types')->delete();

        $types = [
            [
                'product_type_id' => '1',
                'product_type_name' => 'Túi Xách',
                'product_type_description'     => 'Cách loại túi xách',
                'web_title'     => 'Túi Xách',
                'web_description'     => 'Cách loại túi xách',
                'web_keywords'     => 'tui, tui xach, túi, túi xách, mikstore, túi xách chính hãng, tui xach chinh hang',
                'web_canonical'     => 'tui-xach',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'product_type_id' => '2',
                'product_type_name' => 'Balo',
                'product_type_description'     => 'Cách loại balo',
                'web_title'     => 'Balo',
                'web_description'     => 'Cách loại balo',
                'web_keywords'     => 'ba, lo, balo, mikstore, túi xách chính hãng, tui xach chinh hang',
                'web_canonical'     => 'balo',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'product_type_id' => '3',
                'product_type_name' => 'Giày Dép',
                'product_type_description'     => 'Cách loại Giày Dép',
                'web_title'     => 'Giày Dép',
                'web_description'     => 'Cách loại Giày Dép',
                'web_keywords'     => 'giay, dep, day giep, giày dép, mikstore, túi xách chính hãng, tui xach chinh hang',
                'web_canonical'     => 'giay-dep',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'product_type_id' => '4',
                'product_type_name' => 'Ví Cầm Tay',
                'product_type_description'     => 'Cách loại Ví',
                'web_title'     => 'Ví Cầm Tay',
                'web_description'     => 'Cách loại Ví',
                'web_keywords'     => 'vi, cam tay, vi cam tay, ví cầm tay, mikstore, túi xách chính hãng, tui xach chinh hang',
                'web_canonical'     => 'vi-cam-tay',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'product_type_id' => '5',
                'product_type_name' => 'Mắt Kính',
                'product_type_description'     => 'Cách loại Mắt Kính',
                'web_title'     => 'Mắt Kính',
                'web_description'     => 'Cách loại Mắt Kính',
                'web_keywords'     => 'mat, mắt, kinh, kính, mat kinh, mắt kính, mikstore, túi xách chính hãng, tui xach chinh hang',
                'web_canonical'     => 'mat-kinh',
                'active_status'     => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        ProductType::insert($types);
    }
}
