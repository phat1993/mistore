<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_customers';
    // customer_id
    protected $primaryKey = 'customer_id';
    const CUSTOMER_USER = 'customer_user';
    const CUSTOMER_PASSWORD = 'customer_password';

    const CUSTOMER_AVATAR = 'customer_avatar';
    const CUSTOMER_FULL_NAME = 'customer_full_name';
    const CUSTOMER_GENDER = 'customer_gender';
    const CUSTOMER_BRITHDAY = 'customer_brithday';
    const CUSTOMER_EMAIL = 'customer_email';
    const CUSTOMER_PHONE = 'customer_phone';
    const CUSTOMER_ADDRESS = 'customer_address';
    const PHONE_VERIFIED_AT = 'phone_verified_at';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const LAST_LOGIN_TIME = 'last_login_time';
    const REMEMBER_TOKEN = 'remember_token';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::CUSTOMER_USER,
        self::CUSTOMER_PASSWORD,
        self::CUSTOMER_AVATAR,
        self::CUSTOMER_FULL_NAME,
        self::CUSTOMER_GENDER,
        self::CUSTOMER_BRITHDAY,
        self::CUSTOMER_EMAIL,
        self::CUSTOMER_PHONE,
        self::CUSTOMER_ADDRESS,
        self::PHONE_VERIFIED_AT,
        self::EMAIL_VERIFIED_AT,
        self::LAST_LOGIN_TIME,
        self::REMEMBER_TOKEN,
        self::ACTIVE_STATUS,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'active_status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
