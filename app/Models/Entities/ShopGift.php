<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopGift extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_shop_gifts';
    // gift_id
    protected $primaryKey = 'gift_id';
    const GIFT_CODE = 'gift_code';
    const GIFT_DISCOUNT = 'gift_discount';
    const GIFT_PATTERN = 'gift_pattern';
    const GIFT_START_TIME = 'gift_start_time';
    const GIFT_END_TIME = 'gift_end_time';
    const GIFT_USE_COUNT = 'gift_use_count';
    const GIFT_USED = 'gift_used';
    const GIFT_NOTE = 'gift_note';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::GIFT_CODE,
        self::GIFT_DISCOUNT,
        self::GIFT_PATTERN,
        self::GIFT_START_TIME,
        self::GIFT_END_TIME,
        self::GIFT_USE_COUNT,
        self::GIFT_USED,
        self::GIFT_NOTE,
        self::ACTIVE_STATUS,
    ];
}
