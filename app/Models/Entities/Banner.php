<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_banners';
    // banner_id
    protected $primaryKey = 'banner_id';
    const BANNER_TITLE = 'banner_title';
    const BANNER_URL = 'banner_url';
    const BANNER_DESCRIPTION = 'banner_description';
    const BANNER_IMG = 'banner_img';
    const BANNER_LINK = 'banner_link';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::BANNER_TITLE,
        self::BANNER_URL,
        self::BANNER_DESCRIPTION,
        self::BANNER_IMG,
        self::BANNER_LINK,
        self::ACTIVE_STATUS,
    ];
}
