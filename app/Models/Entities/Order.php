<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'ms_orders';
    // order_id
    protected $primaryKey = 'order_id';
    const ORDER_STATE_ID = 'order_state_id';
    const ORDER_GIFT_ID = 'order_gift_id';
    const ORDER_GIFT = 'order_gift';
    const ORDER_SHIP_FEE = 'order_ship_fee';
    const ORDER_DISCOUNT = 'order_discount';
    const ORDER_GIFT_DISCOUNT = 'order_gift_discount';
    const ORDER_TAX = 'order_tax';
    const ORDER_TOTAL = 'order_total';
    const ORDER_AMOUNT = 'order_amount';

    const CUSTOMER_ID = 'customer_id';
    const ORDER_FULL_NAME = 'order_full_name';
    const ORDER_EMAIL = 'order_email';
    const ORDER_PHONE = 'order_phone';
    const ORDER_ADDRESS = 'order_address';
    const ORDER_DELIVERY_TIME = 'order_delivery_time';
    const ORDER_NOTE = 'order_note';

    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::ORDER_STATE_ID,
        self::ORDER_GIFT_ID,
        self::ORDER_GIFT,
        self::ORDER_SHIP_FEE,
        self::ORDER_DISCOUNT,
        self::ORDER_GIFT_DISCOUNT,
        self::ORDER_TAX,
        self::ORDER_TOTAL,
        self::ORDER_AMOUNT,
        self::CUSTOMER_ID,
        self::ORDER_FULL_NAME,
        self::ORDER_EMAIL,
        self::ORDER_PHONE,
        self::ORDER_ADDRESS,
        self::ORDER_DELIVERY_TIME,
        self::ORDER_NOTE,
    ];
}
