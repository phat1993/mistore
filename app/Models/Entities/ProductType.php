<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProductType extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_product_types';
    // product_type_id
    protected $primaryKey = 'product_type_id';
    const PRODUCT_TYPE_NAME = 'product_type_name';
    const PRODUCT_TYPE_DESCRIPTION = 'product_type_description';
    const WEB_CANONICAL = 'web_canonical';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::PRODUCT_TYPE_NAME,
        self::PRODUCT_TYPE_DESCRIPTION,
        self::WEB_CANONICAL,
        self::ACTIVE_STATUS,
    ];

    public static function boot(){
        parent::boot();

        //khi bat dau luu thi tu dong luu lai ten doi tuong duoi dang duong dan
        static::saving(function($model){
            $model->web_canonical=Str::slug($model->product_type_name);
        });
    }
}
