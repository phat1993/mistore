<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopMenu extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_shop_menus';
    // shop_menu_id
    protected $primaryKey = 'shop_menu_id';
    const SHOP_MENU_POSITION = 'shop_menu_position';
    const SHOP_MENU_NAME = 'shop_menu_name';
    const SHOP_MENU_LINK = 'shop_menu_link';
    const PRODUCT_TYPE_ID = 'product_type_id';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::SHOP_MENU_POSITION,
        self::SHOP_MENU_NAME,
        self::SHOP_MENU_LINK,
        self::PRODUCT_TYPE_ID,
        self::ACTIVE_STATUS,
    ];
}
