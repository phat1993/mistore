<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_products';
    // product_id
    protected $primaryKey = 'product_id';
    const PRODUCT_NAME = 'product_name';
    const PRODUCT_PRICE = 'product_price';
    const PRODUCT_DISCOUNT = 'product_discount';
    const PRODUCT_DESCRIPTION = 'product_description';
    const PRODUCT_DETAIL = 'product_detail';
    const PRODUCT_IMG_1 = 'product_img_1';
    const PRODUCT_IMG_2 = 'product_img_2';
    const PRODUCT_IMG_3 = 'product_img_3';
    const PRODUCT_IMG_4 = 'product_img_4';
    const PRODUCT_IMG_5 = 'product_img_5';
    const PRODUCT_IMG_6 = 'product_img_6';
    const PRODUCT_IMG_7 = 'product_img_7';
    const PRODUCT_VIDEO = 'product_video';
    const PRODUCT_CATEGORY_ID = 'product_category_id';

    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::PRODUCT_NAME,
        self::PRODUCT_PRICE,
        self::PRODUCT_DISCOUNT,
        self::PRODUCT_DESCRIPTION,
        self::PRODUCT_DETAIL,
        self::PRODUCT_IMG_1,
        self::PRODUCT_IMG_2,
        self::PRODUCT_IMG_3,
        self::PRODUCT_IMG_4,
        self::PRODUCT_IMG_5,
        self::PRODUCT_IMG_6,
        self::PRODUCT_IMG_7,
        self::PRODUCT_VIDEO,
        self::PRODUCT_CATEGORY_ID,
        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,
        self::ACTIVE_STATUS,
    ];
}
