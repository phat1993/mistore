<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $table = 'ms_order_details';
    // order_detail_id
    protected $primaryKey = 'order_detail_id';
    const ORDER_ID = 'order_id';
    const PRODUCT_ID = 'product_id';
    const ORDER_DETAIL_QUANTITY = 'order_detail_quantity';
    const ORDER_DETAIL_PRICE = 'order_detail_price';
    const ORDER_DETAIL_DISCOUNT = 'order_detail_discount';
    const ORDER_DETAIL_TOTAL = 'order_detail_total';
    const ORDER_DETAIL_AMOUNT = 'order_detail_amount';

    protected $fillable = [
        self::ORDER_ID,
        self::PRODUCT_ID,
        self::ORDER_DETAIL_QUANTITY,
        self::ORDER_DETAIL_DISCOUNT,
        self::ORDER_DETAIL_TOTAL,
        self::ORDER_DETAIL_AMOUNT,
    ];
}
