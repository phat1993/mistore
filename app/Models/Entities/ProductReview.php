<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductReview extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_product_reviews';
    // product_review_id
    protected $primaryKey = 'product_review_id';
    const PRODUCT_REVIEW_IP = 'product_review_ip';
    const PRODUCT_REVIEW_AVATAR = 'product_review_avatar';
    const PRODUCT_REVIEW_STAR = 'product_review_star';
    const PRODUCT_REVIEW_NAME = 'product_review_name';
    const PRODUCT_REVIEW_MAIL = 'product_review_mail';
    const PRODUCT_REVIEW_DESCRIPTION = 'product_review_description';
    const PRODUCT_ID = 'product_id';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::PRODUCT_REVIEW_IP,
        self::PRODUCT_REVIEW_AVATAR,
        self::PRODUCT_REVIEW_STAR,
        self::PRODUCT_REVIEW_NAME,
        self::PRODUCT_REVIEW_MAIL,
        self::PRODUCT_REVIEW_DESCRIPTION,
        self::PRODUCT_ID,
        self::ACTIVE_STATUS,
    ];
}
