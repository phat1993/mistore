<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Administrator extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_administrators';

    protected $primaryKey = 'admin_id';

    const ADMIN_OFFICIAL_NAME = 'admin_official_name';
    const ADMIN_SUPPLEMENT_NAME = 'admin_supplement_name';
    const ADMIN_ABBREVIATION = 'admin_abbreviation';
    const ADMIN_EMAIL = 'admin_email';
    const ADMIN_ADDRESS = 'admin_address';
    const ADMIN_TEL1 = 'admin_tel1';
    const ADMIN_TEL2 = 'admin_tel2';
    const SPECIAL_NOTES = 'special_notes';
    const ADMIN_AVATAR = 'admin_avatar';
    const USER_NAME = 'user_name';
    const PASSWORD = 'password';
    const OLD_PASSWORD = 'old_password';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $fillable = [
        self::ADMIN_OFFICIAL_NAME,
        self::ADMIN_SUPPLEMENT_NAME,
        self::ADMIN_ABBREVIATION,
        self::ADMIN_EMAIL,
        self::ADMIN_ADDRESS,
        self::ADMIN_TEL1,
        self::ADMIN_TEL2,
        self::SPECIAL_NOTES,
        self::ADMIN_AVATAR,
        self::USER_NAME,
        self::PASSWORD,
        self::OLD_PASSWORD,
        self::EMAIL_VERIFIED_AT,
        self::REMEMBER_TOKEN,
        self::ACTIVE_STATUS,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'old_password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
