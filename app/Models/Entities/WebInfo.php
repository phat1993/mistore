<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebInfo extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_web_infos';
    // web_info_id
    protected $primaryKey = 'web_info_id';
    const WEB_INFO_TITLE = 'web_info_title';
    const WEB_INFO_PHONE = 'web_info_phone';
    const WEB_INFO_MAIL = 'web_info_mail';
    const WEB_INFO_ADDRESS = 'web_info_address';
    const WEB_INFO_FACEBOOK = 'web_info_facebook';
    const WEB_INFO_ZALO = 'web_info_zalo';
    const WEB_INFO_YOUTUBE = 'web_info_youtube';
    const WEB_INFO_INSTAGRAM = 'web_info_instagram';
    const WEB_INFO_SHOPEE = 'web_info_shopee';
    const WEB_INFO_DETAIL = 'web_info_detail';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::WEB_INFO_TITLE,
        self::WEB_INFO_PHONE,
        self::WEB_INFO_MAIL,
        self::WEB_INFO_ADDRESS,
        self::WEB_INFO_FACEBOOK,
        self::WEB_INFO_ZALO,
        self::WEB_INFO_YOUTUBE,
        self::WEB_INFO_INSTAGRAM,
        self::WEB_INFO_SHOPEE,
        self::WEB_INFO_DETAIL,
        self::ACTIVE_STATUS,
    ];
}
