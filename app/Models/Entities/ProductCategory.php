<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'ms_product_categories';
    // product_category_id
    protected $primaryKey = 'product_category_id';
    const PRODUCT_CATEGORY_NAME = 'product_category_name';
    const PRODUCT_CATEGORY_DESCRIPTION = 'product_category_description';
    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';
    const ACTIVE_STATUS = 'active_status';
    const PRODUCT_TYPE_ID = 'product_type_id';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::PRODUCT_CATEGORY_NAME,
        self::PRODUCT_CATEGORY_DESCRIPTION,
        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,
        self::ACTIVE_STATUS,
        self::PRODUCT_TYPE_ID,
    ];
}
