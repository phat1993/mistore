<?php

namespace App\Models\ViewModels;


class OrderVM
{
    // product_id
    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PRICE = 'price';//don gia
    const DISCOUNT = 'discount';//khuyen mai
    const IMAGE = 'image';//hinh anh
    const QUANTITY = 'quantity';//so luong
    const TOTAL = 'total';//tong tien
    const AMOUNT = 'amount';//thanh tien


    protected $fillable = [
        self::ID,
        self::NAME,
        self::PRICE,
        self::DISCOUNT,
        self::IMAGE,
        self::QUANTITY,
        self::TOTAL,
        self::AMOUNT,
    ];
}
