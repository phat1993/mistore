<?php

namespace App\Models\ViewModels;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class LoginViewModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'ms_administrators';
    protected $guard = 'admins';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'password',
        'active_status',
    ];


    public $timestamps = false;

    /**
     * Set primary key
     *
     * @var int
     */
    protected $primaryKey = 'admin_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'active_status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function getAuthPassword()
    // {
    //     return $this->password;
    // }
}
