<?php

namespace App\Models\ViewModels;


class FavoriteVM
{
    const CLIENT_IP = 'client_ip';
    const CUSTOMER = 'customer';
    const PRODUCT = 'product';
    const LINK = 'link';
    const NAME = 'name';
    const PRICE = 'price';
    const DISCOUNT = 'discount';
    const IMAGE = 'image';


    protected $fillable = [
        self::CLIENT_IP,
        self::CUSTOMER,
        self::PRODUCT,
        self::LINK,
        self::NAME,
        self::PRICE,
        self::DISCOUNT,
        self::IMAGE,
    ];
}
