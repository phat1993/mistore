<?php

namespace App\Models\ViewModels;


class CheckoutInfoVM
{
    const STATE_ID = 'state_id';
    const GIFT_ID = 'gift_id';
    const GIFT = 'gift';
    const SHIP_FEE = 'ship_fee';
    const DISCOUNT = 'discount';
    const GIFT_DISCOUNT = 'gift_discount';
    const TAX = 'tax';
    const TOTAL = 'total';
    const AMOUNT = 'amount';

    const CUSTOMER_ID = 'customer_id';
    const FULLNAME = 'fullname';
    const EMAIL = 'email';
    const PHONENUMBER = 'phonenumber';
    const ADDRESS = 'address';
    const DELIVERY_TIME = 'delivery_time';
    const DELIVERY_NOTE = 'delivery_note';

    protected $fillable = [
        self::STATE_ID,
        self::GIFT_ID,
        self::GIFT,
        self::SHIP_FEE,
        self::DISCOUNT,
        self::GIFT_DISCOUNT,
        self::TAX,
        self::TOTAL,
        self::AMOUNT,

        self::CUSTOMER_ID,
        self::FULLNAME,
        self::EMAIL,
        self::PHONENUMBER,
        self::ADDRESS,
        self::DELIVERY_TIME,
        self::DELIVERY_NOTE,
    ];
}
