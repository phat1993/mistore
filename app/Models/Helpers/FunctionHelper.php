<?php

namespace App\Models\Helpers;

use App\Models\Entities\Banner;
use App\Models\Entities\ProductCategory;
use App\Models\Entities\ProductType;
use App\Models\Entities\ShopMenu;
use App\Models\Entities\WebInfo;
use Illuminate\Support\Facades\DB;

class FunctionHelper
{

    //Lay danh sach nhom san pham va viet hoa hien thi tren menu bar
    public static function getMenuSite()
    {
        $shopMenus = ShopMenu::where('active_status', '=', '1')->orderBy(ShopMenu::SHOP_MENU_POSITION)->orderByDesc(ShopMenu::UPDATED_AT)->get();
        // $types = ProductType::select('product_type_id', DB::raw('UPPER(product_type_name) AS product_type_name'))->get();
        $menus = collect();

        foreach ($shopMenus as $item) {
            if (isset($item->product_type_id)) {
                $type = ProductType::where('product_type_id',$item->product_type_id)->select('web_canonical')->firstOrFail();
                if (isset($type))
                    $menus->add([
                        'product_type_id' => $item->product_type_id,
                        'menu_name' => $item->shop_menu_name,
                        'menu_link' => '/cua-hang' . '/' . $type->web_canonical,
                        'categories' => ProductCategory::where([
                            ['product_type_id', '=', $item->product_type_id],
                            ['active_status', '=', '1'],
                        ])->select('web_canonical', 'product_category_name')->get()
                    ]);
            } else
                $menus->add([
                    'product_type_id' => null,
                    'menu_name' => $item->shop_menu_name,
                    'menu_link' => $item->shop_menu_link,
                    'categories' => array()
                ]);
        }

        // dd($menus);
        return $menus;
    }

    //lay danh sach nhom san pham hien thi cho footer
    public static function getMenuFooter()
    {
        // $types = ProductType::all();
        $types = ProductType::select('web_canonical', 'product_type_name')->get();
        return $types;
    }

    //lay thong tin web site
    public static function getWebSiteInfo()
    {
        // $types = ProductType::all();
        $info = WebInfo::where([[WebInfo::ACTIVE_STATUS, 1]])->select(
            'web_info_title',
            'web_info_phone',
            'web_info_mail',
            'web_info_address',
            'web_info_zalo',
            'web_info_facebook',
            'web_info_youtube',
            'web_info_instagram',
            'web_info_shopee'
        )->orderBy('updated_at')->first();

        if ($info == null) {
            $info = new WebInfo();
            $info->web_info_title = '';
            $info->web_info_phone = '09000000000';
            $info->web_info_mail = 'XXXXXXXXXXXXX@mail.com';
            $info->web_info_address = 'XXXXXXXXXXXXX HCM';
            $info->web_info_detail = 'chua co thong tin';
            $info->web_info_zalo = 'https://zalo.me';
            $info->web_info_facebook = 'https://www.facebook.com';
            $info->web_info_youtube = 'https://www.youtube.com';
            $info->web_info_instagram = 'https://www.instagram.com';
            $info->web_info_shopee = 'https://shopee.vn/';
        }
        return $info;
    }
}
