<?php

namespace App\Http\Requests\Clients;

use App\Models\Helpers\FunctionHelper;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //San pham da yeu thich khi chua login
        if (!$this->session()->has('favorites'))
            $this->session()->put('favorites', collect());

        //San pham da xem chi tiet
        if (!$this->session()->has('viewed'))
            $this->session()->put('viewed', collect());

        if (!$this->session()->has('orders'))
            $this->session()->put('orders', collect());

        if (!$this->session()->has('menu')) {
            $menu = FunctionHelper::getMenuSite();
            $this->session()->put('menu', $menu);
        }
        if (!$this->session()->has('menu_footer')) {
            $menuf = FunctionHelper::getMenuFooter();
            $this->session()->put('menu_footer', $menuf);
        }
        if (!$this->session()->has('web_info')) {
            $info = FunctionHelper::getWebSiteInfo();
            $this->session()->put('web_info', $info);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                $rules = [
                    'fullname'  => 'required|max:100',
                    'phonenumber'  => 'required|max:20',
                    'email'  => 'max:50',
                    'address'  => 'required|max:200',
                    'delivery_note'  => 'max:500',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Họ Tên: Bạn không được để trống.',
            'fullname.max' => 'Họ Tên: Số kí tự cho phép nhập tối đa là 100.',
            'phonenumber.required' => 'Số Điện Thoại: Bạn không được để trống.',
            'phonenumber.max' => 'Số Điện Thoại: Số kí tự cho phép nhập tối đa là 20.',
            'email.max' => 'Email: Số kí tự cho phép nhập tối đa là 20.',
            'address.required' => 'Địa chỉ: Bạn không được để trống.',
            'address.max' => 'Địa chỉ: Số kí tự cho phép nhập tối đa là 200.',
            'delivery_note.max' => 'Email: Số kí tự cho phép nhập tối đa là 500.',
        ];
    }
}
