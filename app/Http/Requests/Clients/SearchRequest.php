<?php

namespace App\Http\Requests\Clients;

use App\Models\Helpers\FunctionHelper;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //San pham da yeu thich khi chua login
        if (!$this->session()->has('favorites'))
            $this->session()->put('favorites', collect());

        //San pham da xem chi tiet
        if (!$this->session()->has('viewed'))
            $this->session()->put('viewed', collect());

        if (!$this->session()->has('orders'))
            $this->session()->put('orders', collect());

        if (!$this->session()->has('menu')) {
            $menu = FunctionHelper::getMenuSite();
            $this->session()->put('menu', $menu);
        }
        if (!$this->session()->has('menu_footer')) {
            $menuf = FunctionHelper::getMenuFooter();
            $this->session()->put('menu_footer', $menuf);
        }
        if (!$this->session()->has('web_info')) {
            $info = FunctionHelper::getWebSiteInfo();
            $this->session()->put('web_info', $info);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                    $rules = [
                        'keysearch'=> 'required',
                    ];
                break;
        }

        return $rules;
    }
}
