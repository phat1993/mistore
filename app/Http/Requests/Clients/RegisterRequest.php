<?php

namespace App\Http\Requests\Clients;

use App\Models\Helpers\FunctionHelper;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //San pham da yeu thich khi chua login
        if (!$this->session()->has('favorites'))
            $this->session()->put('favorites', collect());

        //San pham da xem chi tiet
        if (!$this->session()->has('viewed'))
            $this->session()->put('viewed', collect());

        if (!$this->session()->has('orders'))
            $this->session()->put('orders', collect());

        if (!$this->session()->has('menu')) {
            $menu = FunctionHelper::getMenuSite();
            $this->session()->put('menu', $menu);
        }
        if (!$this->session()->has('menu_footer')) {
            $menuf = FunctionHelper::getMenuFooter();
            $this->session()->put('menu_footer', $menuf);
        }
        if (!$this->session()->has('web_info')) {
            $info = FunctionHelper::getWebSiteInfo();
            $this->session()->put('web_info', $info);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                $rules = [
                    'email'  => 'required|email|max:50',
                    'password'  => 'required|max:16|confirmed',
                    'password_confirmation'  => 'required|max:16',
                    'fullname'  => 'required|max:100',
                    'phone'  => 'required|max:20',
                    'address'  => 'required|max:200',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'email.required' => 'Emai: Bạn không được để trống.',
            'email.email' => 'Emai: Bạn Cần Nhập vào địa chỉ email chính xác.',
            'email.max' => 'Emai: Số kí tự cho phép nhập tối đa là 50.',

            'password.required' => 'Mật Khẩu: Bạn không được để trống.',
            'password.max' => 'Mật Khẩu: Số kí tự cho phép nhập tối đa là 16.',
            'password.confirmed' => 'Nhập lại Mật Khẩu không chính xác.',

            'password_confirmation.required' => 'Xác Nhận Mật Khẩu: Bạn không được để trống.',
            'password_confirmation.max' => 'Xác Nhận Mật Khẩu: Số kí tự cho phép nhập tối đa là 16.',

            'fullname.required' => 'Họ Tên: Bạn không được để trống.',
            'fullname.max' => 'Họ Tên: Số kí tự cho phép nhập tối đa là 100.',

            'phone.required' => 'Số Điện Thoại: Bạn không được để trống.',
            // 'phone.numeric' => 'Số Điện Thoại: Bạn hãy nhập chính xác số điện thoại',
            'phone.max' => 'Số Điện Thoại: Số kí tự cho phép nhập tối đa là 20.',

            'address.required' => 'Địa chỉ: Bạn không được để trống.',
            'address.max' => 'Địa chỉ: Số kí tự cho phép nhập tối đa là 200.',
        ];
    }
}
