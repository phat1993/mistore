<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'product_category_name'  => 'required|max:100',
                        'product_category_description'  => 'max:200',
                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                        'web_title'  => 'max:20',
                        'web_canonical'  => 'max:200',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'product_category_name.required' => 'Tên Hạng Mục: Bạn không được để trống.',
            'product_category_name.max' => 'Tên Hạng Mục: Số kí tự cho phép nhập tối đa là 100.',

            'product_category_description.max' => 'Mô Tả: Số kí tự cho phép nhập tối đa là 200.',
            'web_title.max' => 'SEO Tiêu Đề: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.max' => 'SEO Từ Khóa Tìm Kiếm: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.max' => 'SEO Mô Tả: Số kí tự cho phép nhập tối đa là 120.',
            'web_canonical.max' => 'SEO Đường Dẫn: Số kí tự cho phép nhập tối đa là 200.',

        ];
    }
}
