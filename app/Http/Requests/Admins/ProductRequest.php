<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy' || $action == 'delete')
                    $rules = [];
                elseif ($action == 'create')
                    $rules = [
                        'product_category_id'=> 'required',
                        'product_name'  => 'required|max:100',
                        'product_description'  => 'required|max:120',
                        'product_price'  => 'required',
                        'product_detail'  => 'required',
                        'img_1'  => 'image|required',
                        'video'  => 'max:1000',

                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                        'web_title'  => 'max:20',
                        'web_canonical'  => 'max:200',
                    ];
                elseif ($action == 'update')
                    $rules = [
                        'product_category_id'=> 'required',
                        'product_name'  => 'required|max:100',
                        'product_description'  => 'required|max:120',
                        'product_price'  => 'required',
                        'product_detail'  => 'required',
                        'video'  => 'max:1000',

                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                        'web_title'  => 'max:20',
                        'web_canonical'  => 'max:200',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'product_category_id.required' => 'Danh Mục Sản Phẩm: Bạn không được để trống.',
            'product_name.required' => 'Tên Sản Phẩm: Bạn không được để trống.',
            'product_name.max' => 'Tên Sản Phẩm: Số kí tự cho phép nhập tối đa là 100.',
            'product_description.required' => 'Mô Tả: Bạn không được để trống.',
            'product_description.max' => 'Mô Tả: Số kí tự cho phép nhập tối đa là 100.',
            'product_price.required' => 'Giá Tiền: Bạn không được để trống.',
            'product_detail.required' => 'Chi tiết: Bạn không được để trống.',
            'img_1.image' => 'Hình Ảnh 1: Bạn cần chọn file là hình ảnh.',
            'img_1.required' => 'Hình Ảnh 1: Bạn không được để trống.',
            'img_2.image' => 'Hình Ảnh 2: Bạn cần chọn file là hình ảnh.',
            'img_2.required' => 'Hình Ảnh 2: Bạn không được để trống.',
            'img_3.image' => 'Hình Ảnh 3: Bạn cần chọn file là hình ảnh.',
            'img_3.required' => 'Hình Ảnh 3: Bạn không được để trống.',
            'video.max' => 'Video: Số kí tự cho phép nhập tối đa là 1000.',

            'web_title.max' => 'SEO Tiêu Đề: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.max' => 'SEO Từ Khóa Tìm Kiếm: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.max' => 'SEO Mô Tả: Số kí tự cho phép nhập tối đa là 120.',
            'web_canonical.max' => 'SEO Đường Dẫn: Số kí tự cho phép nhập tối đa là 200.',

        ];
    }
}
