<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($this->route()->getActionMethod() == 'destroy')
                    $rules = [];
                else
                    $rules = [
                        'user_name'  => 'required|max:30',
                        'password'  => 'required|max:20',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'user_name.required' => 'Tài Khoản: Bạn không được để trống.',
            'user_name.max' => 'Tài Khoản: Số kí tự cho phép nhập tối đa là 30.',

            'password.required' => 'Mật Khẩu: Bạn không được để trống.',
            'password.max' => 'Mật Khẩu: Số kí tự cho phép nhập tối đa là 20.',

        ];
    }
}
