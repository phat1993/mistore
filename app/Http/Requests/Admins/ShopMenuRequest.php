<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class ShopMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'shop_menu_name'  => 'required|max:100',
                        'shop_menu_position'  => 'required|numeric',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'shop_menu_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'shop_menu_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 100.',

            'shop_menu_position.required' => 'Vị Trí: Bạn không được để trống.',
            'shop_menu_position.numeric' => 'Vị Trí: Bạn cần nhập số.',

        ];
    }
}
