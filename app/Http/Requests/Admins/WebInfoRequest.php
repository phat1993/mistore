<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class WebInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'web_info_title'  => 'required|max:100',
                        'web_info_phone'  => 'required|max:50',
                        'web_info_mail'  => 'required|max:50|email',
                        'web_info_facebook'  => 'required|max:500',
                        'web_info_zalo'  => 'required|max:500',
                        'web_info_youtube'  => 'required|max:500',
                        'web_info_instagram'  => 'required|max:500',
                        'web_info_shopee'  => 'required|max:500',
                        'web_info_address'  => 'required|max:200',
                        'web_info_detail'  => 'required',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'web_info_title.required' => 'Tiêu Đề: Bạn không được để trống.',
            'web_info_title.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 100.',
            'web_info_phone.required' => 'Điện Thoại: Bạn không được để trống.',
            'web_info_phone.max' => 'Điện Thoại: Số kí tự cho phép nhập tối đa là 50.',
            'web_info_mail.required' => 'Email: Bạn không được để trống.',
            'web_info_mail.max' => 'Email: Số kí tự cho phép nhập tối đa là 50.',
            'web_info_mail.email' => 'Email: Bạn cần nhập chính xác email.',

            'web_info_facebook.required' => 'Đường Dẫn Facebook: Bạn không được để trống.',
            'web_info_facebook.max' => 'Đường Dẫn Facebook: Số kí tự cho phép nhập tối đa là 500.',

            'web_info_zalo.required' => 'Đường Dẫn Zalo: Bạn không được để trống.',
            'web_info_zalo.max' => 'Đường Dẫn Zalo: Số kí tự cho phép nhập tối đa là 500.',

            'web_info_youtube.required' => 'Đường Dẫn Youtube: Bạn không được để trống.',
            'web_info_youtube.max' => 'Đường Dẫn Youtube: Số kí tự cho phép nhập tối đa là 500.',

            'web_info_instagram.required' => 'Đường Dẫn Instagram: Bạn không được để trống.',
            'web_info_instagram.max' => 'Đường Dẫn Instagram: Số kí tự cho phép nhập tối đa là 500.',

            'web_info_shopee.required' => 'Đường Dẫn Shopee: Bạn không được để trống.',
            'web_info_shopee.max' => 'Đường Dẫn Shopee: Số kí tự cho phép nhập tối đa là 500.',
            'web_info_address.required' => 'Địa Chỉ: Bạn không được để trống.',
            'web_info_address.max' => 'Địa Chỉ: Số kí tự cho phép nhập tối đa là 200.',
            'web_info_detail.required' => 'Nội Dung: Bạn không được để trống.',

        ];
    }
}
