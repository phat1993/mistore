<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class ShopGiftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'gift_code'  => 'required|max:100',
                        // 'gift_discount'  => 'required',
                        'gift_start_time'  => 'required|date|after:yesterday',
                        'gift_end_time'  => 'required|date|after:gift_start_time',
                        'gift_use_count'  => 'required|numeric',
                        'gift_note'  => 'max:500',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'gift_code.required' => 'Mã Khuyến Mãi: Bạn không được để trống.',
            'gift_code.max' => 'Mã Khuyến Mãi: Số kí tự cho phép nhập tối đa là 100.',
            // 'gift_discount.required' => 'Tiền Khuyến Mãi: Bạn không được để trống.',

            'gift_start_time.required' => 'Thời Gian Bắt Đầu: Bạn không được để trống.',
            'gift_start_time.date' => 'Thời Gian Bắt Đầu: Bạn cần nhập thời gian chính xác.',
            'gift_start_time.after' => 'Thời Gian Bắt Đầu: Bạn cần nhập thời gian lớn hơn thời gian hiện tại.',

            'gift_end_time.required' => 'Thời Gian Kết Thúc: Bạn không được để trống.',
            'gift_end_time.date' => 'Thời Gian Kết Thúc: Bạn cần nhập thời gian chính xác.',
            'gift_end_time.after' => 'Thời Gian Kết Thúc: Bạn cần nhập thời gian lớn hơn thời gian bắt đầu.',

            'gift_use_count.required' => 'Lần Sử Dụng: Bạn không được để trống.',
            'gift_use_count.numeric' => 'Lần Sử Dụng: Bạn chỉ được nhập số.',
            'gift_note.max' => 'Ghi Chú: Số kí tự cho phép nhập tối đa là 500.',

        ];
    }
}
