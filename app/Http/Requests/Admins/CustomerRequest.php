<?php

namespace App\Http\Requests\Admins;;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        // 'banner_title'  => 'required|max:100',
                        // 'banner_url'  => 'required|max:500',
                        // 'banner_description'  => 'required',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            // 'banner_title.required' => 'Tiêu Đề: Bạn không được để trống.',
            // 'banner_title.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 100.',
            // 'banner_url.required' => 'Đường Dẫn: Bạn không được để trống.',
            // 'banner_url.max' => 'Đường Dẫn: Số kí tự cho phép nhập tối đa là 500.',

            // 'banner_description.required' => 'Mô Tả: Bạn không được để trống.',

        ];
    }
}
