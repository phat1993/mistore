<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ShopMenuRequest;
use App\Models\Entities\ShopMenu;
use App\Models\Entities\ProductType;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class ShopMenuController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ShopMenuRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // if (isset($data[ShopMenu::PRODUCT_CATEGORY_NAME])) {
        //     $param[] =  [ShopMenu::PRODUCT_CATEGORY_NAME, 'like', '%' . $data[ShopMenu::PRODUCT_CATEGORY_NAME] . '%'];
        // }

        if (isset($data[ShopMenu::ACTIVE_STATUS])) {
            if ($data[ShopMenu::ACTIVE_STATUS] == 1)
                $param[] = [ShopMenu::ACTIVE_STATUS, 1];
            elseif ($data[ShopMenu::ACTIVE_STATUS] == 2)
                $param[] = [ShopMenu::ACTIVE_STATUS, 0];
        }
        $ShopMenus = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $ShopMenus = ShopMenu::where($param)->select(
                'ms_shop_menus.*',
                //Lay thong tin loai san pham
                'ms_product_types.product_type_name',
            )->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_shop_menus.product_type_id')
                ->withTrashed()->orderByDesc(ShopMenu::UPDATED_AT)->paginate($perPage);
        else
            $ShopMenus = ShopMenu::where($param)->select(
                'ms_shop_menus.*',
                //Lay thong tin loai san pham
                'ms_product_types.product_type_name',
            )->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_shop_menus.product_type_id')
                ->orderByDesc(ShopMenu::UPDATED_AT)->paginate($perPage);

        $types = ProductType::all();
        return view('pages.admins.shopmenu.index', compact('ShopMenus', 'types', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ShopMenuRequest $request)
    {
        $shopmenu = new ShopMenu();
        $shopmenu->active_status = 1;
        $types = ProductType::all();
        return view('pages.admins.shopmenu.create')->with(compact('shopmenu', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopMenuRequest $request)
    {
        if ($request->isMethod('post')) {

            $data = $request->all();

            if ($data[ShopMenu::SHOP_MENU_LINK] == '' && $data[ShopMenu::PRODUCT_TYPE_ID] == '')
                return back()->withInput()->withErrors(['Đường Dẫn: Bạn cần chọn đường dẫn cho menu.']);

            $shopmenu = new ShopMenu();
            $shopmenu->{ShopMenu::SHOP_MENU_NAME} = $data[ShopMenu::SHOP_MENU_NAME];
            $shopmenu->{ShopMenu::SHOP_MENU_POSITION} = $data[ShopMenu::SHOP_MENU_POSITION];
            $shopmenu->{ShopMenu::SHOP_MENU_LINK} = $data[ShopMenu::SHOP_MENU_LINK];
            $shopmenu->{ShopMenu::PRODUCT_TYPE_ID} = $data[ShopMenu::PRODUCT_TYPE_ID];
            $shopmenu->{ShopMenu::ACTIVE_STATUS} = isset($data[ShopMenu::ACTIVE_STATUS]) ? 1 : 0;
            $shopmenu->{ShopMenu::PRODUCT_TYPE_ID} = $data[ShopMenu::PRODUCT_TYPE_ID];

            if ($shopmenu->save())
                return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
            else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function show(ShopMenuRequest $request)
    {
        $data = $request->all();
        $shopmenu = ShopMenu::where('shop_menu_id', $data['shop_menu_id'])->select(
            'ms_shop_menus.*',
            //Lay thong tin loai san pham
            'ms_product_types.product_type_name',
            'ms_product_types.web_canonical',
        )->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_shop_menus.product_type_id')
            ->withTrashed()->firstOrFail();

        if (isset($shopmenu)) {
            $admin = Auth::user();
            return view('pages.admins.shopmenu.show', compact('shopmenu', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopMenuRequest $request)
    {
        $data = $request->all();

        $shopmenu = ShopMenu::withTrashed()->where('shop_menu_id', $data['shop_menu_id'])->firstOrFail();
        if (isset($shopmenu)) {
            $types = ProductType::all();
            return view('pages.admins.shopmenu.edit')->with(compact('shopmenu', 'types'));
        }

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function update(ShopMenuRequest $request)
    {
        $data = $request->all();
        if ($data[ShopMenu::SHOP_MENU_LINK] == '' && $data[ShopMenu::PRODUCT_TYPE_ID] == '')
            return back()->withInput()->withErrors(['Đường Dẫn: Bạn cần chọn đường dẫn cho menu.']);

        $param = array();

        $param[ShopMenu::SHOP_MENU_NAME] = $data[ShopMenu::SHOP_MENU_NAME];
        $param[ShopMenu::SHOP_MENU_POSITION] = $data[ShopMenu::SHOP_MENU_POSITION];
        $param[ShopMenu::SHOP_MENU_LINK] = $data[ShopMenu::SHOP_MENU_LINK];

        $param[ShopMenu::PRODUCT_TYPE_ID] = $data[ShopMenu::PRODUCT_TYPE_ID];
        $param[ShopMenu::ACTIVE_STATUS] = isset($data[ShopMenu::ACTIVE_STATUS]) ? 1 : 0;

        $category = ShopMenu::where('shop_menu_id', $data['shop_menu_id']);
        if ($category->update($param))
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
        else
            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopMenuRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $category = ShopMenu::withTrashed()->where('shop_menu_id', $data['shop_menu_id'])->firstOrFail();
        if (!isset($category))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = ShopMenu::where('shop_menu_id', $data['shop_menu_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function recovered(ShopMenuRequest $request)
    {
        $data = $request->all();

        $deleted = ShopMenu::withTrashed()->where('shop_menu_id', $data['shop_menu_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ShopMenu  $ShopMenu
     * @return \Illuminate\Http\Response
     */
    public function delete(ShopMenuRequest $request)
    {
        $data = $request->all();

        $deleted = ShopMenu::withTrashed()->where('shop_menu_id', $data['shop_menu_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }
}
