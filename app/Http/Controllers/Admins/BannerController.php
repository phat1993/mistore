<?php

namespace App\Http\Controllers\Admins;;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\BannerRequest;
use App\Models\Entities\Banner;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BannerRequest $request)
    {

        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data[Banner::BANNER_TITLE])) {
            $param[] =  [Banner::BANNER_TITLE, 'like', '%' . $data[Banner::BANNER_TITLE] . '%'];
        }

        if (isset($data[Banner::BANNER_DESCRIPTION])) {
            $param[] = [Banner::BANNER_DESCRIPTION, 'like', '%' . $data[Banner::BANNER_DESCRIPTION] . '%'];
        }
        if (isset($data[Banner::ACTIVE_STATUS])) {
            if ($data[Banner::ACTIVE_STATUS] == 1)
                $param[] = [Banner::ACTIVE_STATUS, 1];
            elseif ($data[Banner::ACTIVE_STATUS] == 2)
                $param[] = [Banner::ACTIVE_STATUS, 0];
        }
        // if(isset($data[Banner::BANNER_DETAIL])){
        //     $param[] =  [Banner::BANNER_DETAIL, 'like', '%'.$data[Banner::BANNER_DETAIL].'%'];
        // }
        $Banners = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $Banners = Banner::where($param)->withTrashed()->orderByDesc(Banner::UPDATED_AT)->paginate($perPage);
        else
            $Banners = Banner::where($param)->orderByDesc(Banner::UPDATED_AT)->paginate($perPage);

        return view('pages.admins.banner.index', compact('Banners', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BannerRequest $request)
    {
        $banner = new Banner();
        $banner->active_status = 1;
        return view('pages.admins.banner.create')->with(compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        if ($request->isMethod('post')) {

            $path = '';
            if ($request->hasFile('image')) {
                $validImg = ['image'  => 'image|required']; //|dimensions:width=800,height=500
                $customMessages = [
                    'image.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    // 'image.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 800x500.',
                ];
                $request->validate($validImg, $customMessages);

                $path = $request->file('image')->store(
                    'images/banners',
                    'public'
                );
            } else {
                $validImg = ['banner_link'  => 'required|max:500']; //|dimensions:width=800,height=500
                $customMessages = [
                    'banner_link.required' => 'Hình Ảnh: Bạn cần nhập vào đường dẫn hoặc chọn đăng hình ảnh.',
                    'banner_link.max' => 'Đường Dẫn Hình Ảnh: Số kí tự cho phép nhập tối đa là 500.',
                ];
                $request->validate($validImg, $customMessages);
            }

            $data = $request->all();

            $banner = new Banner();
            $banner->{Banner::BANNER_TITLE} = $data[Banner::BANNER_TITLE];
            $banner->{Banner::BANNER_URL} = $data[Banner::BANNER_URL];
            $banner->{Banner::BANNER_IMG} = $path;
            $banner->{Banner::BANNER_LINK} = $path == '' ? $data[Banner::BANNER_LINK] : '';
            $banner->{Banner::BANNER_DESCRIPTION} = $data[Banner::BANNER_DESCRIPTION];
            $param[Banner::ACTIVE_STATUS] = isset($data[Banner::ACTIVE_STATUS]) ? 1 : 0;

            if ($banner->save())
                return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
            else {
                //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function show(BannerRequest $request)
    {
        $data = $request->all();
        $banner = Banner::withTrashed()
            ->where('banner_id', $data['banner_id'])->firstOrFail();

        if (isset($banner)) {
            $admin = Auth::user();
            return view('pages.admins.banner.show', compact('banner', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function edit(BannerRequest $request)
    {
        $data = $request->all();

        $banner = Banner::withTrashed()->where('banner_id', $data['banner_id'])->firstOrFail();
        if (isset($banner))
            return view('pages.admins.banner.edit')->with(compact('banner'));

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request)
    {
        $data = $request->all();

        $param = array();
        $path = '';

        if ($request->hasFile('image')) {
            $validImg = ['image'  => 'image'];
            $customMessages = [
                'image.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            //Truoc khi cap nhat xoa hinh anh cu
            if (Storage::disk('public')->exists($data[Banner::BANNER_IMG]))
                Storage::disk('public')->delete($data[Banner::BANNER_IMG]);

            $path = $request->file('image')->store(
                'images/banners',
                'public'
            );
            $param[Banner::BANNER_IMG] = $path == '' ? $data[Banner::BANNER_IMG] : $path;
            $param[Banner::BANNER_LINK] = '';
        } else if ($data['banner_img_clear'] == '') {
            $validImg = ['banner_link'  => 'required|max:500']; //|dimensions:width=800,height=500
            $customMessages = [
                'banner_link.required' => 'Đường Dẫn Hình Ảnh: Bạn không được để trống.',
                'banner_link.max' => 'Đường Dẫn Hình Ảnh: Số kí tự cho phép nhập tối đa là 500.',
            ];
            $request->validate($validImg, $customMessages);

            //Truoc khi cap nhat xoa hinh anh cu
            if (Storage::disk('public')->exists($data[Banner::BANNER_IMG]))
                Storage::disk('public')->delete($data[Banner::BANNER_IMG]);

            $param[Banner::BANNER_LINK] = $data[Banner::BANNER_LINK];
            $param[Banner::BANNER_IMG] = '';
        }


        $param[Banner::BANNER_TITLE] = $data[Banner::BANNER_TITLE];
        $param[Banner::BANNER_URL] = $data[Banner::BANNER_URL];
        $param[Banner::BANNER_DESCRIPTION] = $data[Banner::BANNER_DESCRIPTION];
        $param[Banner::ACTIVE_STATUS] = isset($data[Banner::ACTIVE_STATUS]) ? 1 : 0;

        $banner = Banner::where('banner_id', $data['banner_id']);
        if ($banner->update($param)) {
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
        } else {
            //Neu sua ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
            if ($path != '' && Storage::disk('public')->exists($path))
                Storage::disk('public')->delete($path);

            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannerRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $banner = Banner::withTrashed()->where('banner_id', $data['banner_id'])->firstOrFail();
        if (!isset($banner))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = Banner::where('banner_id', $data['banner_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function recovered(BannerRequest $request)
    {
        $data = $request->all();

        $deleted = Banner::withTrashed()->where('banner_id', $data['banner_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Banners  $Banners
     * @return \Illuminate\Http\Response
     */
    public function delete(BannerRequest $request)
    {
        $data = $request->all();

        $deleted = Banner::withTrashed()->where('banner_id', $data['banner_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }
}
