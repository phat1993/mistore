<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\CustomerRequest;
use App\Models\Entities\Customer;
use App\Models\Entities\Favorite;
use App\Models\Entities\Order;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomerRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data[Customer::CUSTOMER_FULL_NAME])) {
            $param[] =  [Customer::CUSTOMER_FULL_NAME, 'like', '%' . $data[Customer::CUSTOMER_FULL_NAME] . '%'];
        }
        if (isset($data[Customer::CUSTOMER_PHONE])) {
            $param[] =  [Customer::CUSTOMER_PHONE, 'like', '%' . $data[Customer::CUSTOMER_PHONE] . '%'];
        }
        if (isset($data[Customer::CUSTOMER_EMAIL])) {
            $param[] =  [Customer::CUSTOMER_EMAIL, 'like', '%' . $data[Customer::CUSTOMER_EMAIL] . '%'];
        }
        if (isset($data[Customer::ACTIVE_STATUS])) {
            if ($data[Customer::ACTIVE_STATUS] == 1)
                $param[] = [Customer::ACTIVE_STATUS, 1];
            elseif ($data[Customer::ACTIVE_STATUS] == 2)
                $param[] = [Customer::ACTIVE_STATUS, 0];
        }
        $Customers = array();
        // if (isset($data['view_all']) && $data['view_all'] == 1)
        //     $Customers = Customer::where($param)->withTrashed()->orderByDesc(Customer::UPDATED_AT)->paginate($perPage);
        // else
            $Customers = Customer::where($param)->orderByDesc(Customer::UPDATED_AT)->paginate($perPage);

        return view('pages.admins.customers.index', compact('Customers', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CustomerRequest $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerRequest $request)
    {
        $data = $request->all();
        $customer = Customer::where('customer_id', $data['customer_id'])->withTrashed()->firstOrFail();

        if (isset($customer)) {
            $admin = Auth::user();
            $Favorites = Favorite::where('ms_favorites.customer_id', $data['customer_id'])->select(
                'ms_favorites.*',
                //Lay thong san pham
                'ms_products.product_img_1',
                'ms_products.product_name',
            )
                ->leftJoin('ms_products', 'ms_products.product_id', '=', 'ms_favorites.product_id')
                ->orderByDesc('ms_favorites.updated_at')->get();

            $ShopOrders = Order::where('ms_orders.customer_id', $data['customer_id'])->select(
                'ms_orders.*',
                //Lay thong tin trang thai giao hang
                'ms_order_states.order_state_id',
                'ms_order_states.order_state_value',
            )
                ->leftJoin('ms_order_states', 'ms_order_states.order_state_id', '=', 'ms_orders.order_state_id')->orderByDesc('ms_orders.updated_at')->get();


            return view('pages.admins.customers.show', compact('customer', 'Favorites', 'ShopOrders', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerRequest $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerRequest $request)
    {
        // $data = $request->all();

        // //Kiem tra doi tuong co trong database khong
        // $category = DB::table('ms_product_categories')->where('product_category_id', $data['product_category_id'])->firstOrFail();
        // if (!isset($category))
        //     return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        // $deleted = Customer::where('product_category_id', $data['product_category_id'])->delete();
        // if ($deleted)
        //     return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        // else
        //     return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(CustomerRequest $request)
    {
        // $data = $request->all();

        // $deleted = Customer::withTrashed()->where('product_category_id', $data['product_category_id'])->restore();
        // if ($deleted)
        //     return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        // else
        //     return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(CustomerRequest $request)
    {
        // $data = $request->all();

        // $deleted = Customer::withTrashed()->where('product_category_id', $data['product_category_id'])->forceDelete();
        // if ($deleted)
        //     return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        // else
        //     return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }
}

