<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebInfoRequest;
use App\Models\Entities\WebInfo;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class WebInfoController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebInfoRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data[WebInfo::WEB_INFO_TITLE])) {
            $param[] =  [WebInfo::WEB_INFO_TITLE, 'like', '%' . $data[WebInfo::WEB_INFO_TITLE] . '%'];
        }
        if (isset($data[WebInfo::WEB_INFO_PHONE])) {
            $param[] =  [WebInfo::WEB_INFO_PHONE, 'like', '%' . $data[WebInfo::WEB_INFO_PHONE] . '%'];
        }
        if (isset($data[WebInfo::WEB_INFO_MAIL])) {
            $param[] =  [WebInfo::WEB_INFO_MAIL, 'like', '%' . $data[WebInfo::WEB_INFO_MAIL] . '%'];
        }
        if (isset($data[WebInfo::WEB_INFO_ADDRESS])) {
            $param[] =  [WebInfo::WEB_INFO_ADDRESS, 'like', '%' . $data[WebInfo::WEB_INFO_ADDRESS] . '%'];
        }
        if (isset($data[WebInfo::ACTIVE_STATUS])) {
            if ($data[WebInfo::ACTIVE_STATUS] == 1)
                $param[] = [WebInfo::ACTIVE_STATUS, 1];
            elseif ($data[WebInfo::ACTIVE_STATUS] == 2)
                $param[] = [WebInfo::ACTIVE_STATUS, 0];
        }
        $WebInfos = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $WebInfos = WebInfo::where($param)->withTrashed()->orderByDesc(WebInfo::UPDATED_AT)->paginate($perPage);
        else
            $WebInfos = WebInfo::where($param)->orderByDesc(WebInfo::UPDATED_AT)->paginate($perPage);

        return view('pages.admins.webinfo.index', compact('WebInfos', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebInfoRequest $request)
    {
        $webinfo = new WebInfo();
        $webinfo->active_status = 1;
        return view('pages.admins.webinfo.create')->with(compact('webinfo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebInfoRequest $request)
    {
        if ($request->isMethod('post')) {

            $data = $request->all();

            $WebInfo = new WebInfo();
            $WebInfo->{WebInfo::WEB_INFO_TITLE} = $data[WebInfo::WEB_INFO_TITLE];
            $WebInfo->{WebInfo::WEB_INFO_PHONE} = $data[WebInfo::WEB_INFO_PHONE];
            $WebInfo->{WebInfo::WEB_INFO_MAIL} = $data[WebInfo::WEB_INFO_MAIL];
            $WebInfo->{WebInfo::WEB_INFO_ZALO} = $data[WebInfo::WEB_INFO_ZALO];
            $WebInfo->{WebInfo::WEB_INFO_FACEBOOK} = $data[WebInfo::WEB_INFO_FACEBOOK];
            $WebInfo->{WebInfo::WEB_INFO_YOUTUBE} = $data[WebInfo::WEB_INFO_YOUTUBE];
            $WebInfo->{WebInfo::WEB_INFO_INSTAGRAM} = $data[WebInfo::WEB_INFO_INSTAGRAM];
            $WebInfo->{WebInfo::WEB_INFO_SHOPEE} = $data[WebInfo::WEB_INFO_SHOPEE];
            $WebInfo->{WebInfo::WEB_INFO_ADDRESS} = $data[WebInfo::WEB_INFO_ADDRESS];
            $WebInfo->{WebInfo::WEB_INFO_DETAIL} = $data[WebInfo::WEB_INFO_DETAIL];
            $WebInfo->{WebInfo::ACTIVE_STATUS} = isset($data[WebInfo::ACTIVE_STATUS]) ? 1 : 0;

            if ($WebInfo->save())
                return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');

            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function show(WebInfoRequest $request)
    {
        $data = $request->all();
        $webinfo = WebInfo::withTrashed()
            ->where('web_info_id', $data['web_info_id'])->firstOrFail();

        if (isset($webinfo)) {
            $admin = Auth::user();
            return view('pages.admins.webinfo.show', compact('webinfo', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function edit(WebInfoRequest $request)
    {
        $data = $request->all();

        $webinfo = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->firstOrFail();
        if (isset($webinfo))
            return view('pages.admins.webinfo.edit')->with(compact('webinfo'));

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function update(WebInfoRequest $request)
    {
        $data = $request->all();

        $param = array();

        $param[WebInfo::WEB_INFO_TITLE] = $data[WebInfo::WEB_INFO_TITLE];
        $param[WebInfo::WEB_INFO_PHONE] = $data[WebInfo::WEB_INFO_PHONE];
        $param[WebInfo::WEB_INFO_MAIL] = $data[WebInfo::WEB_INFO_MAIL];
        $param[WebInfo::WEB_INFO_ZALO] = $data[WebInfo::WEB_INFO_ZALO];
        $param[WebInfo::WEB_INFO_FACEBOOK] = $data[WebInfo::WEB_INFO_FACEBOOK];
        $param[WebInfo::WEB_INFO_YOUTUBE] = $data[WebInfo::WEB_INFO_YOUTUBE];
        $param[WebInfo::WEB_INFO_INSTAGRAM] = $data[WebInfo::WEB_INFO_INSTAGRAM];
        $param[WebInfo::WEB_INFO_SHOPEE] = $data[WebInfo::WEB_INFO_SHOPEE];
        $param[WebInfo::WEB_INFO_ADDRESS] = $data[WebInfo::WEB_INFO_ADDRESS];
        $param[WebInfo::WEB_INFO_DETAIL] = $data[WebInfo::WEB_INFO_DETAIL];
        $param[WebInfo::ACTIVE_STATUS] = isset($data[WebInfo::ACTIVE_STATUS]) ? 1 : 0;

        $WebInfo = WebInfo::where('web_info_id', $data['web_info_id']);
        if ($WebInfo->update($param))
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');

        return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebInfoRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $WebInfo = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->firstOrFail();
        if (!isset($WebInfo))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = WebInfo::where('web_info_id', $data['web_info_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebInfoRequest $request)
    {
        $data = $request->all();

        $deleted = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\WebInfos  $WebInfos
     * @return \Illuminate\Http\Response
     */
    public function delete(WebInfoRequest $request)
    {
        $data = $request->all();

        $deleted = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }

    public function demo(WebInfoRequest $request, $web_info_id)
    {
        $data = $request->all();
        $about = WebInfo::withTrashed()->where('web_info_id', $web_info_id)->firstOrFail();
        if (isset($about))
            return view('pages.clients.about', compact('about'));

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem!']);
    }
}
