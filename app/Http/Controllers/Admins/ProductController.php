<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ProductRequest;
use App\Models\Entities\Product;
use App\Models\Entities\ProductCategory;
use App\Models\Entities\ProductReview;
use App\Models\Entities\ProductType;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data[Product::PRODUCT_CATEGORY_ID])) {
            $param[] =  [Product::PRODUCT_CATEGORY_ID, '=', $data[Product::PRODUCT_CATEGORY_ID]];
        }
        if (isset($data[Product::PRODUCT_NAME])) {
            $param[] =  [Product::PRODUCT_NAME, 'like', '%' . $data[Product::PRODUCT_NAME] . '%'];
        }
        if (isset($data[Product::PRODUCT_PRICE])) {
            $price = str_replace(',', '', $data[Product::PRODUCT_PRICE]);
            $param[] = [Product::PRODUCT_PRICE, '=', $price];
        }
        if (isset($data[Product::PRODUCT_DISCOUNT])) {
            $discount = str_replace(',', '', $data[Product::PRODUCT_DISCOUNT]);
            $param[] = [Product::PRODUCT_DISCOUNT, '=', $discount];
        }

        if (isset($data[Product::PRODUCT_DESCRIPTION])) {
            $param[] = [Product::PRODUCT_DESCRIPTION, 'like', '%' . $data[Product::PRODUCT_DESCRIPTION] . '%'];
        }
        if (isset($data[Product::ACTIVE_STATUS])) {
            if ($data[Product::ACTIVE_STATUS] == 1)
                $param[] = [Product::ACTIVE_STATUS, 1];
            elseif ($data[Product::ACTIVE_STATUS] == 2)
                $param[] = [Product::ACTIVE_STATUS, 0];
        }
        // if(isset($data[Product::BANNER_DETAIL])){
        //     $param[] =  [Product::BANNER_DETAIL, 'like', '%'.$data[Product::BANNER_DETAIL].'%'];
        // }
        $Products = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $Products = Product::where($param)->withTrashed()->orderByDesc(Product::UPDATED_AT)->paginate($perPage);
        else
            $Products = Product::where($param)->orderByDesc(Product::UPDATED_AT)->paginate($perPage);

        $types = ProductType::all();
        return view('pages.admins.products.index', compact('Products', 'currentPage', 'perPage', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProductRequest $request)
    {
        $product = new Product();
        $product->active_status = 1;
        $types = ProductType::all();
        return view('pages.admins.products.create')->with(compact('product', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        if ($request->isMethod('post')) {

            $data = $request->all();
            $product = new Product();
            if ($request->hasFile('image_1')) {
                $img1 = $request->file('image_1')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_1} = $img1;
            }
            if ($request->hasFile('image_2')) {
                $img2 = $request->file('image_2')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_2} = $img2;
            }
            if ($request->hasFile('image_3')) {
                $img3 = $request->file('image_3')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_3} = $img3;
            }
            if ($request->hasFile('image_4')) {
                $img4 = $request->file('image_4')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_4} = $img4;
            }
            if ($request->hasFile('image_5')) {
                $img5 = $request->file('image_5')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_5} = $img5;
            }
            if ($request->hasFile('image_6')) {
                $img6 = $request->file('image_6')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_6} = $img6;
            }
            if ($request->hasFile('image_7')) {
                $img7 = $request->file('image_7')->store(
                    'images/products/' . $data[Product::PRODUCT_CATEGORY_ID],
                    'public'
                );
                $product->{Product::PRODUCT_IMG_7} = $img7;
            }


            $product->{Product::PRODUCT_CATEGORY_ID} = $data[Product::PRODUCT_CATEGORY_ID];
            $product->{Product::PRODUCT_NAME} = $data[Product::PRODUCT_NAME];
            $product->{Product::PRODUCT_DESCRIPTION} = $data[Product::PRODUCT_DESCRIPTION];
            $product->{Product::PRODUCT_DETAIL} = $data[Product::PRODUCT_DETAIL];
            $product->{Product::PRODUCT_PRICE} = str_replace(',', '', $data[Product::PRODUCT_PRICE]);
            $discount = str_replace(',', '', $data[Product::PRODUCT_DISCOUNT]);
            $product->{Product::PRODUCT_DISCOUNT} = $discount != '' ? $discount : -1;

            $product->{Product::PRODUCT_VIDEO} = $data['video'];
            $product->{Product::WEB_KEYWORDS} = $data[ProductCategory::WEB_KEYWORDS];
            $product->{Product::WEB_DESCRIPTION} = $data[ProductCategory::WEB_DESCRIPTION];
            $product->{Product::WEB_TITLE} = $data[ProductCategory::WEB_TITLE];
            $product->{Product::WEB_CANONICAL} = Str::slug($product->{Product::PRODUCT_NAME});
            $product->{Product::ACTIVE_STATUS} = isset($data[Product::ACTIVE_STATUS]) ? 1 : 0;

            if ($product->save())
                return redirect()->back()->withInput()->with('success', 'Tạo mới đối tượng thành công !');
            else {
                //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                if ($img1 != '' && Storage::disk('public')->exists($img1))
                    Storage::disk('public')->delete($img1);
                if ($img2 != '' && Storage::disk('public')->exists($img2))
                    Storage::disk('public')->delete($img2);
                if ($img3 != '' && Storage::disk('public')->exists($img3))
                    Storage::disk('public')->delete($img3);
                if ($img4 != '' && Storage::disk('public')->exists($img4))
                    Storage::disk('public')->delete($img4);
                if ($img5 != '' && Storage::disk('public')->exists($img5))
                    Storage::disk('public')->delete($img5);
                if ($img6 != '' && Storage::disk('public')->exists($img6))
                    Storage::disk('public')->delete($img6);
                if ($img7 != '' && Storage::disk('public')->exists($img7))
                    Storage::disk('public')->delete($img7);

                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banners  $Products
     * @return \Illuminate\Http\Response
     */
    public function show(ProductRequest $request)
    {
        $data = $request->all();
        $product = Product::where('product_id', $data['product_id'])->select(
            'ms_products.*',
            //Lay thong tin loai san pham
            'ms_product_categories.product_category_name',
            'ms_product_categories.web_canonical as category_web_canonical',
            //Lay thong tin danh muc san pham
            'ms_product_types.product_type_name',
            'ms_product_types.web_canonical as type_web_canonical',
        )
            ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_product_categories.product_type_id')->withTrashed()->firstOrFail();

        if (isset($product)) {
            $admin = Auth::user();
            //Lay so luong review moi nhat da kich hoat
            $Review = ProductReview::withTrashed()->where('active_status', 1)->orderByDesc(ProductReview::UPDATED_AT)->get();
            return view('pages.admins.products.show', compact('product', 'admin', 'Review'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $Products
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductRequest $request)
    {
        $data = $request->all();

        $product = Product::withTrashed()->select(
            'ms_products.*',
            'ms_product_categories.product_type_id',
        )
            ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->where('product_id', $data['product_id'])->firstOrFail();
        if (isset($product)) {
            $types = ProductType::all();
            return view('pages.admins.products.edit')->with(compact('product', 'types'));
        }

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $Products
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request)
    {
        $data = $request->all();

        $param = array();
        $img1 = '';
        $img2 = '';
        $img3 = '';
        $img4 = '';
        $img5 = '';
        $img6 = '';
        $img7 = '';
        //Kiem tra hinh anh
        if ($request->hasFile('image_1')) {
            $validImg = ['image_1'  => 'image'];
            $customMessages = [
                'image_1.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img1 = $request->file('image_1')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_1] = $img1 == '' ? $data[Product::PRODUCT_IMG_1] : $img1;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_1_del']) {
            if ($img1 != '' && Storage::disk('public')->exists($data['product_img_1_del']))
                Storage::disk('public')->delete($data['product_img_1_del']);
        }


        //Kiem tra hinh anh
        if ($request->hasFile('image_2')) {
            $validImg = ['image_2'  => 'image'];
            $customMessages = [
                'image_2.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img2 = $request->file('image_2')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_2] = $img2 == '' ? $data[Product::PRODUCT_IMG_2] : $img2;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_2_del']) {
            $param[Product::PRODUCT_IMG_2] = '';
            if ($img2 != '' && Storage::disk('public')->exists($data['product_img_2_del']))
                Storage::disk('public')->delete($data['product_img_2_del']);
        }

        //Kiem tra hinh anh
        if ($request->hasFile('image_3')) {
            $validImg = ['image_3'  => 'image'];
            $customMessages = [
                'image_3.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img3 = $request->file('image_3')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_3] = $img3 == '' ? $data[Product::PRODUCT_IMG_3] : $img3;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_3_del']) {
            $param[Product::PRODUCT_IMG_3] = '';
            if ($img3 != '' && Storage::disk('public')->exists($data['product_img_3_del']))
                Storage::disk('public')->delete($data['product_img_3_del']);
        }

        //Kiem tra hinh anh
        if ($request->hasFile('image_4')) {
            $validImg = ['image_4'  => 'image'];
            $customMessages = [
                'image_4.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img4 = $request->file('image_4')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_4] = $img4 == '' ? $data[Product::PRODUCT_IMG_4] : $img4;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_4_del']) {
            $param[Product::PRODUCT_IMG_4] = '';
            if ($img4 != '' && Storage::disk('public')->exists($data['product_img_4_del']))
                Storage::disk('public')->delete($data['product_img_4_del']);
        }

        //Kiem tra hinh anh
        if ($request->hasFile('image_5')) {
            $validImg = ['image_5'  => 'image'];
            $customMessages = [
                'image_5.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img5 = $request->file('image_5')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_5] = $img5 == '' ? $data[Product::PRODUCT_IMG_5] : $img5;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_5_del']) {
            $param[Product::PRODUCT_IMG_5] = '';
            if ($img5 != '' && Storage::disk('public')->exists($data['product_img_5_del']))
                Storage::disk('public')->delete($data['product_img_5_del']);
        }

        //Kiem tra hinh anh
        if ($request->hasFile('image_6')) {
            $validImg = ['image_6'  => 'image'];
            $customMessages = [
                'image_6.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img6 = $request->file('image_6')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_6] = $img6 == '' ? $data[Product::PRODUCT_IMG_6] : $img6;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_6_del']) {
            $param[Product::PRODUCT_IMG_6] = '';
            if ($img6 != '' && Storage::disk('public')->exists($data['product_img_6_del']))
                Storage::disk('public')->delete($data['product_img_6_del']);
        }

        //Kiem tra hinh anh
        if ($request->hasFile('image_7')) {
            $validImg = ['image_7'  => 'image'];
            $customMessages = [
                'image_7.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $img7 = $request->file('image_7')->store(
                'images/Products',
                'public'
            );
            $param[Product::PRODUCT_IMG_7] = $img7 == '' ? $data[Product::PRODUCT_IMG_7] : $img7;
        }
        //Neu xoa hinh anh thi mac dinh se xoa
        elseif ($data['product_img_7_del']) {
            $param[Product::PRODUCT_IMG_7] = '';
            if ($img7 != '' && Storage::disk('public')->exists($data['product_img_7_del']))
                Storage::disk('public')->delete($data['product_img_7_del']);
        }




        $param[Product::PRODUCT_NAME] = $data[Product::PRODUCT_NAME];
        $param[Product::PRODUCT_DESCRIPTION] = $data[Product::PRODUCT_DESCRIPTION];
        $param[Product::PRODUCT_DETAIL] = $data[Product::PRODUCT_DETAIL];
        $param[Product::PRODUCT_VIDEO] = $data[Product::PRODUCT_VIDEO];
        $param[Product::PRODUCT_CATEGORY_ID] = $data[Product::PRODUCT_CATEGORY_ID];
        $param[Product::WEB_TITLE] = $data[Product::WEB_TITLE];
        $param[Product::WEB_KEYWORDS] = $data[Product::WEB_KEYWORDS];
        $param[Product::WEB_DESCRIPTION] = $data[Product::WEB_DESCRIPTION];
        $param[Product::WEB_CANONICAL] = Str::slug($param[Product::PRODUCT_NAME]);

        $param[Product::PRODUCT_PRICE] = str_replace(',', '', $data[Product::PRODUCT_PRICE]);
        if ($data[Product::PRODUCT_DISCOUNT] != '')
            $param[Product::PRODUCT_DISCOUNT] = str_replace(',', '', $data[Product::PRODUCT_DISCOUNT]);
        else $param[Product::PRODUCT_DISCOUNT] = -1;

        $param[Product::ACTIVE_STATUS] = isset($data[Product::ACTIVE_STATUS]) ? 1 : 0;


        $product = Product::where('product_id', $data['product_id']);
        if ($product->update($param)) {
            //Cap nhat thanh cong xoa hinh anh cu
            if ($img1 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_1]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_1]);
            if ($img2 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_2]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_2]);
            if ($img3 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_3]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_3]);
            if ($img4 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_4]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_4]);
            if ($img5 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_5]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_5]);
            if ($img6 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_6]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_6]);
            if ($img7 != '' && Storage::disk('public')->exists($data[Product::PRODUCT_IMG_7]))
                Storage::disk('public')->delete($data[Product::PRODUCT_IMG_7]);

            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
        } else {
            //Neu sua ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
            if ($img1 != '' && Storage::disk('public')->exists($img1))
                Storage::disk('public')->delete($img1);
            if ($img2 != '' && Storage::disk('public')->exists($img2))
                Storage::disk('public')->delete($img2);
            if ($img3 != '' && Storage::disk('public')->exists($img3))
                Storage::disk('public')->delete($img3);
            if ($img4 != '' && Storage::disk('public')->exists($img4))
                Storage::disk('public')->delete($img4);
            if ($img5 != '' && Storage::disk('public')->exists($img5))
                Storage::disk('public')->delete($img5);
            if ($img6 != '' && Storage::disk('public')->exists($img6))
                Storage::disk('public')->delete($img6);
            if ($img7 != '' && Storage::disk('public')->exists($img7))
                Storage::disk('public')->delete($img7);

            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $Products
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $product = Product::withTrashed()->where('product_id', $data['product_id'])->firstOrFail();
        if (!isset($product))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = Product::where('product_id', $data['product_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Products  $Products
     * @return \Illuminate\Http\Response
     */
    public function recovered(ProductRequest $request)
    {
        $data = $request->all();

        $deleted = Product::withTrashed()->where('product_id', $data['product_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Products  $Products
     * @return \Illuminate\Http\Response
     */
    public function delete(ProductRequest $request)
    {
        $data = $request->all();
        //Kiem tra doi tuong co trong database khong
        $product = Product::withTrashed()->where('product_id', $data['product_id'])->firstOrFail();

        $deleted = Product::withTrashed()->where('product_id', $data['product_id'])->forceDelete();
        if ($deleted) {
            //Neu xoa vinh vien thanh cong thi xoa hinh anh
            if ($product->{Product::PRODUCT_IMG_1} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_1}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_1});
            if ($product->{Product::PRODUCT_IMG_2} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_2}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_2});
            if ($product->{Product::PRODUCT_IMG_3} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_3}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_3});
            if ($product->{Product::PRODUCT_IMG_4} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_4}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_4});
            if ($product->{Product::PRODUCT_IMG_5} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_5}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_5});
            if ($product->{Product::PRODUCT_IMG_6} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_6}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_6});
            if ($product->{Product::PRODUCT_IMG_7} != '' && Storage::disk('public')->exists($product->{Product::PRODUCT_IMG_7}))
                Storage::disk('public')->delete($product->{Product::PRODUCT_IMG_7});
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        } else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }

    /**
     * Ho tro lay doi tuong khi change product type
     */
    public function changeType(string $product_type_id)
    {
        $data = ProductCategory::where('product_type_id', '=', $product_type_id)->select('ms_product_categories.product_category_id', 'ms_product_categories.product_category_name')->orderBy('product_category_id')->get();
        echo json_encode($data);
        exit;
    }
}
