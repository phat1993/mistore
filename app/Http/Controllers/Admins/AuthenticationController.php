<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\LoginRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admins')->except('logout');
    }

    public function login(LoginRequest $request)
    {
        if ($request->isMethod('POST')) {
            $data = $request->all();
            $user = array(
                'user_name' => $data['user_name'],
                'password' => $data['password']
            );
            if (Auth::guard('admins')->attempt($user)) {
                if (Auth::user()->active_status == 1) {
                    return redirect('admins/profile');
                } else {
                    Auth::logout();
                    return redirect()->back()->withInput()->withErrors('Tài khoản của bạn không tồn tại hay chưa kích hoạt! Hãy liên hệ với quản trị viên !');
                }
            }
        }

        return view('pages.admins.admin_login');
    }

    public function logout(Request $request)
    {
        if (Auth::user() != null) {
            DB::table('ms_administrators')->where('admin_id', Auth::user()->admin_id)->update(['last_login_time'=>Carbon::now()]);
        }

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/admins');
    }
}
