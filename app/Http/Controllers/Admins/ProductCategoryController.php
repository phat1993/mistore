<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ProductCategoryRequest;
use App\Models\Entities\ProductCategory;
use App\Models\Entities\ProductType;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductCategoryController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductCategoryRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data[ProductCategory::PRODUCT_TYPE_ID]) && $data[ProductCategory::PRODUCT_TYPE_ID] != '') {
            $param[] =  ['ms_product_categories.product_type_id', '=', $data[ProductCategory::PRODUCT_TYPE_ID]];
        }
        if (isset($data[ProductCategory::PRODUCT_CATEGORY_NAME])) {
            $param[] =  [ProductCategory::PRODUCT_CATEGORY_NAME, 'like', '%' . $data[ProductCategory::PRODUCT_CATEGORY_NAME] . '%'];
        }

        if (isset($data[ProductCategory::PRODUCT_CATEGORY_DESCRIPTION])) {
            $param[] = [ProductCategory::PRODUCT_CATEGORY_DESCRIPTION, 'like', '%' . $data[ProductCategory::PRODUCT_CATEGORY_DESCRIPTION] . '%'];
        }
        if (isset($data[ProductCategory::ACTIVE_STATUS])) {
            if ($data[ProductCategory::ACTIVE_STATUS] == 1)
                $param[] = [ProductCategory::ACTIVE_STATUS, 1];
            elseif ($data[ProductCategory::ACTIVE_STATUS] == 2)
                $param[] = [ProductCategory::ACTIVE_STATUS, 0];
        }
        $ProductCategories = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $ProductCategories = ProductCategory::where($param)->select(
                'ms_product_categories.*',
                //Lay thong tin loai san pham
                'ms_product_types.product_type_name',
            )
                ->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_product_categories.product_type_id')->withTrashed()->orderByDesc(ProductCategory::UPDATED_AT)->paginate($perPage);
        else
            $ProductCategories = ProductCategory::where($param)->select(
                'ms_product_categories.*',
                //Lay thong tin loai san pham
                'ms_product_types.product_type_name',
            )
                ->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_product_categories.product_type_id')->orderByDesc(ProductCategory::UPDATED_AT)->paginate($perPage);

        $types = ProductType::all();
        return view('pages.admins.productcategories.index', compact('ProductCategories', 'types', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProductCategoryRequest $request)
    {
        $category = new ProductCategory();
        $category->active_status = 1;
        $types = ProductType::all();
        return view('pages.admins.productcategories.create')->with(compact('category', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();

            $product_category = new ProductCategory();
            $product_category->{ProductCategory::PRODUCT_CATEGORY_NAME} = $data[ProductCategory::PRODUCT_CATEGORY_NAME];
            $product_category->{ProductCategory::PRODUCT_CATEGORY_DESCRIPTION} = $data[ProductCategory::PRODUCT_CATEGORY_DESCRIPTION];
            $product_category->{ProductCategory::WEB_KEYWORDS} = $data[ProductCategory::WEB_KEYWORDS];
            $product_category->{ProductCategory::WEB_DESCRIPTION} = $data[ProductCategory::WEB_DESCRIPTION];
            $product_category->{ProductCategory::WEB_TITLE} = $data[ProductCategory::WEB_TITLE];
            $product_category->{ProductCategory::WEB_CANONICAL} = Str::slug($product_category->{ProductCategory::PRODUCT_CATEGORY_NAME});
            $product_category->{ProductCategory::ACTIVE_STATUS} = isset($data[ProductCategory::ACTIVE_STATUS]) ? 1 : 0;
            $product_category->{ProductCategory::PRODUCT_TYPE_ID} = $data[ProductCategory::PRODUCT_TYPE_ID];

            if ($product_category->save())
                return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
            else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategoryRequest $request)
    {
        $data = $request->all();
        $category = ProductCategory::withTrashed()->select(
            'ms_product_categories.*',
            //Lay thong tin loai san pham
            'ms_product_types.product_type_name',
            'ms_product_types.web_canonical as type_web_canonical',
        )->leftJoin('ms_product_types', 'ms_product_types.product_type_id', '=', 'ms_product_categories.product_type_id')
            ->where('product_category_id', $data['product_category_id'])->firstOrFail();

        if (isset($category)) {
            $admin = Auth::user();
            return view('pages.admins.productcategories.show', compact('category', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategoryRequest $request)
    {
        $data = $request->all();

        $category = ProductCategory::withTrashed()->where('product_category_id', $data['product_category_id'])->firstOrFail();
        if (isset($category)) {
            $types = ProductType::all();
            return view('pages.admins.productcategories.edit')->with(compact('category', 'types'));
        }

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryRequest $request)
    {
        $data = $request->all();

        $param = array();

        $param[ProductCategory::PRODUCT_CATEGORY_NAME] = $data[ProductCategory::PRODUCT_CATEGORY_NAME];
        $param[ProductCategory::PRODUCT_CATEGORY_DESCRIPTION] = $data[ProductCategory::PRODUCT_CATEGORY_DESCRIPTION];
        $param[ProductCategory::WEB_KEYWORDS] = $data[ProductCategory::WEB_KEYWORDS];
        $param[ProductCategory::WEB_DESCRIPTION] = $data[ProductCategory::WEB_DESCRIPTION];
        $param[ProductCategory::WEB_TITLE] = $data[ProductCategory::WEB_TITLE];
        $param[ProductCategory::WEB_CANONICAL] = Str::slug($param[ProductCategory::PRODUCT_CATEGORY_NAME]);
        $param[ProductCategory::PRODUCT_TYPE_ID] = $data[ProductCategory::PRODUCT_TYPE_ID];
        $param[ProductCategory::ACTIVE_STATUS] = isset($data[ProductCategory::ACTIVE_STATUS]) ? 1 : 0;

        $category = ProductCategory::where('product_category_id', $data['product_category_id']);
        if ($category->update($param))
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
        else
            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategoryRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $category = ProductCategory::withTrashed()->where('product_category_id', $data['product_category_id'])->firstOrFail();
        if (!isset($category))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = ProductCategory::where('product_category_id', $data['product_category_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(ProductCategoryRequest $request)
    {
        $data = $request->all();

        $deleted = ProductCategory::withTrashed()->where('product_category_id', $data['product_category_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(ProductCategoryRequest $request)
    {
        $data = $request->all();

        $deleted = ProductCategory::withTrashed()->where('product_category_id', $data['product_category_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }
}
