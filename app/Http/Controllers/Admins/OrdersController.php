<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\OrderRequest;
use App\Models\Entities\Order;
use App\Models\Entities\OrderDetail;
use App\Models\Entities\ProductType;
use App\Models\Entities\ShopGift;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if (isset($data['order_code'])) {
            $oid = str_replace('MHD', '', $data['order_code']);
            $param[] =  ['ms_orders.order_id',  $oid];
        }
        if (isset($data['order_state_id'])) {
            $param[] =  ['ms_orders.order_state_id', '=', $data['order_state_id']];
        }
        // if (isset($data[Order::PRODUCT_CATEGORY_NAME])) {
        //     $param[] =  [Order::PRODUCT_CATEGORY_NAME, 'like', '%' . $data[Order::PRODUCT_CATEGORY_NAME] . '%'];
        // }

        // if (isset($data[Order::PRODUCT_CATEGORY_DESCRIPTION])) {
        //     $param[] = [Order::PRODUCT_CATEGORY_DESCRIPTION, 'like', '%' . $data[Order::PRODUCT_CATEGORY_DESCRIPTION] . '%'];
        // }
        $ShopOrders = array();
        if (isset($data['order_date'])) {
            $ShopOrders = Order::where($param)->whereDate('ms_orders.created_at', $data['order_date'])->select(
                'ms_orders.*',
                //Lay thong tin trang thai giao hang
                'ms_order_states.order_state_id',
                'ms_order_states.order_state_value',
            )->leftJoin('ms_order_states', 'ms_order_states.order_state_id', '=', 'ms_orders.order_state_id')
                ->orderByDesc(Order::UPDATED_AT)->paginate($perPage);
        } else
            $ShopOrders = Order::where($param)->select(
                'ms_orders.*',
                //Lay thong tin trang thai giao hang
                'ms_order_states.order_state_id',
                'ms_order_states.order_state_value',
            )->leftJoin('ms_order_states', 'ms_order_states.order_state_id', '=', 'ms_orders.order_state_id')
                ->orderByDesc(Order::UPDATED_AT)->paginate($perPage);
        $states = DB::table('ms_order_states')->get();
        return view('pages.admins.orders.index', compact('ShopOrders', 'states', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(OrderRequest $request)
    {
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShopOrders  $ShopOrders
     * @return \Illuminate\Http\Response
     */
    public function show(OrderRequest $request)
    {
        $data = $request->all();
        $shopOrder = Order::where('order_id', $data['order_id'])->select(
            'ms_orders.*',
            //Lay thong tin trang thai giao hang
            'ms_order_states.order_state_id',
            'ms_order_states.order_state_value',
            //Lay thong tin gift
            'ms_shop_gifts.gift_id',
            'ms_shop_gifts.gift_code',
            'ms_shop_gifts.gift_discount',
        )
            ->leftJoin('ms_order_states', 'ms_order_states.order_state_id', '=', 'ms_orders.order_state_id')
            ->leftJoin('ms_shop_gifts', 'ms_shop_gifts.gift_id', '=', 'ms_orders.order_gift_id')->firstOrFail();

        if (isset($shopOrder)) {
            $admin = Auth::user();
            $details = OrderDetail::where('order_id', $data['order_id'])->select(
                'ms_order_details.*',
                //Lay thong tin san
                'ms_products.product_id',
                'ms_products.product_img_1',
                'ms_products.product_name',
                'ms_products.product_price',
                'ms_products.product_discount',
            )
                ->leftJoin('ms_products', 'ms_products.product_id', '=', 'ms_order_details.product_id')->get();
            return view('pages.admins.orders.show', compact('shopOrder', 'details', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShopOrders  $ShopOrders
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request)
    {
        $data = $request->all();

        $param = array();

        $param[Order::ORDER_STATE_ID] = $data[Order::ORDER_STATE_ID];

        $ckOrder = Order::where('order_id', $data['order_id'])->firstOrFail();
        if (!isset($ckOrder))
            return redirect()->back()->withInput()->withErrors(['Không tìm thấy đối tượng!']);
        // $param[Order::UPDATED_AT] = now();

        $order = Order::where('order_id', $data['order_id']);
        if ($order->update($param)) {
            //Neu trang thai la huy, tra, het hang thi ma/sdt khuyen mai su dung se duoc su dung lai
            if ($data[Order::ORDER_STATE_ID] == 5 || $data[Order::ORDER_STATE_ID] == 6 || $data[Order::ORDER_STATE_ID] == 7) {
                $gift =  ShopGift::where('gift_id', $ckOrder->{Order::ORDER_GIFT_ID})->firstOrFail();
                if (isset($gift)) {
                    $used = $gift->{ShopGift::GIFT_USED} - 1;
                    ShopGift::where('gift_id', $ckOrder->{Order::ORDER_GIFT_ID})->update(['gift_used'=> $used]);
                }
            }
            return redirect()->back()->with('success', 'Chuyển trạng thái đơn hàng thành công!');
        } else
            return redirect()->back()->withInput()->withErrors(['Chuyển trạng thái đơn hàng thất bại!']);
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ShopOrders  $ShopOrders
     * @return \Illuminate\Http\Response
     */
    public function delete(OrderRequest $request)
    {
        $data = $request->all();

        $deleted = Order::withTrashed()->where('order_id', $data['order_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }
}
