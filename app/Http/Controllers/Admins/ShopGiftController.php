<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ShopGiftRequest;
use App\Models\Entities\Order;
use App\Models\Entities\ShopGift;
use App\Models\Entities\ProductType;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ShopGiftController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ShopGiftRequest $request)
    {
        $data = $request->all();
        $param = array();
        $perPage = self::PER_PAGE;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        if (isset($data[ShopGift::GIFT_CODE]) && $data[ShopGift::GIFT_CODE] != '') {
            $param[] =  [ShopGift::GIFT_CODE, '=', $data[ShopGift::GIFT_CODE]];
        }
        if (isset($data[ShopGift::GIFT_DISCOUNT])) {
            $price = str_replace(',', '', $data[ShopGift::GIFT_DISCOUNT]);
            $param[] = [ShopGift::GIFT_DISCOUNT, '=', $price];
        }
        if (isset($data['gift_date'])) {
            $param[] = [ShopGift::GIFT_START_TIME, '<=', $data['gift_date']];
            $param[] = [ShopGift::GIFT_END_TIME, '>=', $data['gift_date']];
        }
        if (isset($data[ShopGift::ACTIVE_STATUS])) {
            if ($data[ShopGift::ACTIVE_STATUS] == 1)
                $param[] = [ShopGift::ACTIVE_STATUS, 1];
            elseif ($data[ShopGift::ACTIVE_STATUS] == 2)
                $param[] = [ShopGift::ACTIVE_STATUS, 0];
        }
        $ShopGifts = array();
        if (isset($data['view_all']) && $data['view_all'] == 1)
            $ShopGifts = ShopGift::where($param)->withTrashed()->orderByDesc(ShopGift::UPDATED_AT)->paginate($perPage);
        else
            $ShopGifts = ShopGift::where($param)->orderByDesc(ShopGift::UPDATED_AT)->paginate($perPage);;
        return view('pages.admins.shopgifts.index', compact('ShopGifts', 'currentPage', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ShopGiftRequest $request)
    {
        $shopgift = new ShopGift();
        $shopgift->gift_code = Str::random(rand(20, 25));
        $shopgift->gift_start_time = date_format(now(), 'Y-m-d');
        $shopgift->gift_end_time = date_format(now()->addMonth(1), 'Y-m-d');
        $shopgift->active_status = 1;
        $shopgift->gift_use_count = 1;
        return view('pages.admins.shopgifts.create')->with(compact('shopgift'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopGiftRequest $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();


            //Kiem tra gift nhap vao
            //cho phep trung ma hay sdt nhung khong duoc nam trong khoang thoi gian cua gift cu
            $check = ShopGift::where([
                //oldStart<=newStart<=oldEnd
                ['gift_code', $data[ShopGift::GIFT_CODE]],
                ['gift_start_time', '<=', $data[ShopGift::GIFT_START_TIME]],
                ['gift_end_time', '>=', $data[ShopGift::GIFT_START_TIME]],
                // ['gift_start_time', '<=', $data[ShopGift::GIFT_END_TIME]],
                // ['gift_end_time', '>=', $data[ShopGift::GIFT_END_TIME]],
            ])->orWhere(function ($query) use ($data) {
                //oldStart<=newEnd<=oldEnd
                $query->where('gift_code', $data[ShopGift::GIFT_CODE]);
                $query->where('gift_start_time', '<=', $data[ShopGift::GIFT_END_TIME]);
                $query->where('gift_end_time', '>=', $data[ShopGift::GIFT_END_TIME]);
            })->get();
            if ($check->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Mã/SĐT Khuyến mãi mà bạn nhập vào trùng với thời gian khuyến mãi của mã/sđt khác tương tự!']);
            $shopgift = new ShopGift();

            $shopgift->{ShopGift::GIFT_PATTERN} = $data[ShopGift::GIFT_PATTERN];
            $discount = $data[ShopGift::GIFT_PATTERN] == 1 ? str_replace(',', '', $data['gift_discount_money']) : $data['gift_discount_percent'];
            $shopgift->{ShopGift::GIFT_DISCOUNT} = $discount;

            $shopgift->{ShopGift::GIFT_CODE} = $data[ShopGift::GIFT_CODE];
            $shopgift->{ShopGift::GIFT_START_TIME} = $data[ShopGift::GIFT_START_TIME];
            $shopgift->{ShopGift::GIFT_END_TIME} = $data[ShopGift::GIFT_END_TIME];
            $shopgift->{ShopGift::GIFT_USE_COUNT} = $data[ShopGift::GIFT_USE_COUNT];
            $shopgift->{ShopGift::GIFT_NOTE} = $data[ShopGift::GIFT_NOTE];
            $shopgift->{ShopGift::ACTIVE_STATUS} = isset($data[ShopGift::ACTIVE_STATUS]) ? 1 : 0;

            if ($shopgift->save())
                return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
            else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function show(ShopGiftRequest $request)
    {
        $data = $request->all();
        $shopgift = ShopGift::withTrashed()->where('gift_id', $data['gift_id'])->firstOrFail();

        if (isset($shopgift)) {
            $admin = Auth::user();
            $orders = Order::where('order_gift_id', $shopgift->gift_id)->get();
            return view('pages.admins.shopgifts.show', compact('shopgift', 'orders', 'admin'));
        }
        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xem chi tiết!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopGiftRequest $request)
    {
        $data = $request->all();

        $shopgift  = ShopGift::withTrashed()->where('gift_id', $data['gift_id'])->firstOrFail();
        if (isset($shopgift)) {
            return view('pages.admins.shopgifts.edit')->with(compact('shopgift'));
        }

        return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn cập nhật!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function update(ShopGiftRequest $request)
    {
        $data = $request->all();


        //Kiem tra gift nhap vao
        //cho phep trung ma hay sdt nhung khong duoc nam trong khoang thoi gian cua gift cu
        $check = ShopGift::where([
            //oldStart<=newStart<=oldEnd
            ['gift_id','<>', $data['gift_id']],
            ['gift_code', $data[ShopGift::GIFT_CODE]],
            ['gift_start_time', '<=', $data[ShopGift::GIFT_START_TIME]],
            ['gift_end_time', '>=', $data[ShopGift::GIFT_START_TIME]],
            // ['gift_start_time', '<=', $data[ShopGift::GIFT_END_TIME]],
            // ['gift_end_time', '>=', $data[ShopGift::GIFT_END_TIME]],
        ])->orWhere(function ($query) use ($data) {
            //oldStart<=newEnd<=oldEnd
            $query->where('gift_id','<>', $data['gift_id']);
            $query->where('gift_code', $data[ShopGift::GIFT_CODE]);
            $query->where('gift_start_time', '<=', $data[ShopGift::GIFT_END_TIME]);
            $query->where('gift_end_time', '>=', $data[ShopGift::GIFT_END_TIME]);
        })->get();
        if ($check->count() > 0)
            return redirect()->back()->withInput()->withErrors(['Mã/SĐT Khuyến mãi mà bạn cập nhật vào trùng với thời gian khuyến mãi của mã/sđt khác tương tự!']);

        $param = array();
        $param[ShopGift::GIFT_PATTERN] = $data[ShopGift::GIFT_PATTERN];
        $discount = $data[ShopGift::GIFT_PATTERN] == 1 ? str_replace(',', '', $data['gift_discount_money']) : $data['gift_discount_percent'];
        $param[ShopGift::GIFT_DISCOUNT] = $discount;

        $param[ShopGift::GIFT_CODE] = $data[ShopGift::GIFT_CODE];
        $param[ShopGift::GIFT_START_TIME] = $data[ShopGift::GIFT_START_TIME];
        $param[ShopGift::GIFT_END_TIME] = $data[ShopGift::GIFT_END_TIME];
        $param[ShopGift::GIFT_USE_COUNT] = $data[ShopGift::GIFT_USE_COUNT];
        $param[ShopGift::GIFT_NOTE] = $data[ShopGift::GIFT_NOTE];
        $param[ShopGift::ACTIVE_STATUS] = isset($data[ShopGift::ACTIVE_STATUS]) ? 1 : 0;


        $category = ShopGift::where('gift_id', $data['gift_id']);
        if ($category->update($param))
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
        else
            return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopGiftRequest $request)
    {
        $data = $request->all();

        //Kiem tra doi tuong co trong database khong
        $category = ShopGift::withTrashed()->where('gift_id', $data['gift_id'])->firstOrFail();
        if (!isset($category))
            return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

        $deleted = ShopGift::where('gift_id', $data['gift_id'])->delete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(ShopGiftRequest $request)
    {
        $data = $request->all();

        $deleted = ShopGift::withTrashed()->where('gift_id', $data['gift_id'])->restore();
        if ($deleted)
            return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
        else
            return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $ProductCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(ShopGiftRequest $request)
    {
        $data = $request->all();

        $deleted = ShopGift::withTrashed()->where('gift_id', $data['gift_id'])->forceDelete();
        if ($deleted)
            return redirect()->back()->with('success', 'Xóa đối tượng vĩnh viễn thành công!');
        else
            return redirect()->back()->withErrors('Xóa đối tượng vĩnh viễn thất bại!');
    }

    public function getRandom(ShopGiftRequest $request)
    {
        echo json_encode(Str::random(rand(20, 25)));
        exit;
    }
}
