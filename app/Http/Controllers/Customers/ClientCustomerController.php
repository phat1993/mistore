<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\ClientRequest;
use App\Models\Entities\Customer;
use App\Models\Entities\Product;
use App\Models\ViewModels\FavoriteVM;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ClientCustomerController extends Controller
{
    public function login(ClientRequest $request)
    {
        return view('pages.clients.accounts.login');
    }

    public function register(ClientRequest $request)
    {
        return view('pages.clients.accounts.register');
    }
    public function forget(ClientRequest $request)
    {
        return view('pages.clients.accounts.forget');
    }

    public function changeInfo(ClientRequest $request)
    {
        return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
    }
    public function changePassword(ClientRequest $request)
    {
        return view('pages.clients.accounts.forget');
    }
    public function profile(ClientRequest $request)
    {
        return view('pages.clients.accounts.forget');
    }


    public function addFavorite(ClientRequest $request)
    {
        //lay danh sach yeu thich tu
        $favorites = session('favorites');
        //Get ip cua client hien tai
        $ip = $request->getClientIp();
        // San pham ma client yeu thich luu lai
        $product_id = $request->product_id;

        //Xu ly khi co khach hang
        $customer_id = 0;

        if ($favorites->where('product', $product_id)->count() <= 0) {
            $pro = Product::where('product_id', $product_id)->select(
                'ms_products.product_id',
                'ms_products.product_name',
                'ms_products.product_img_1',
                'ms_products.product_price',
                'ms_products.product_discount',
                //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
                //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
                //Con lai thi lay link binh thuong
                DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                               WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                               ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
            )
                ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
                ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->firstOrFail();
            if ($pro == null)
                return redirect()->back();
            $favorite = new FavoriteVM();
            $favorite->{FavoriteVM::CUSTOMER} = $customer_id;
            $favorite->{FavoriteVM::CLIENT_IP} = $ip;
            $favorite->{FavoriteVM::LINK} = $pro->url_seo;
            $favorite->{FavoriteVM::PRODUCT} = $pro->product_id;
            $favorite->{FavoriteVM::NAME} = $pro->{Product::PRODUCT_NAME};
            $favorite->{FavoriteVM::PRICE} = $pro->{Product::PRODUCT_PRICE};
            $favorite->{FavoriteVM::DISCOUNT} = $pro->{Product::PRODUCT_DISCOUNT} != -1 ? $pro->{Product::PRODUCT_DISCOUNT} : -1;
            $favorite->{FavoriteVM::IMAGE} = $pro->{Product::PRODUCT_IMG_1};
            $favorites->add($favorite);
        }
        //Luu lai session
        $request->session()->put('favorites', $favorites);
        return view('pages.clients.accounts.favorites', compact('favorites'));
    }

    public function productViewed(ClientRequest $request)
    {
        $viewed = session('viewed');
        return view('pages.clients.accounts.viewed', compact('viewed'));
    }

    public function productFavorites(ClientRequest $request)
    {
        $favorites = session('favorites');
        return view('pages.clients.accounts.favorites', compact('favorites'));
    }

    public function viewGift(ClientRequest $request)
    {
        return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
    }
    public function viewOrder(ClientRequest $request)
    {
        return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
    }
}
