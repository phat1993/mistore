<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\LoginRequest;
use App\Http\Requests\Clients\RegisterRequest;
use App\Models\Entities\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientAuthenticationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customers')->except('logout');
    }

    public function register(RegisterRequest $request)
    {
//        if ($request->isMethod('POST')) {
//            $data = $request->all();
//
//            $customer = new Customer();
//            $customer->{Customer::CUSTOMER_EMAIL} = $data['email'];
//            $customer->{Customer::CUSTOMER_USER} = $data['email'];
//            $customer->{Customer::CUSTOMER_PASSWORD} = $data['password'];
//            $customer->{Customer::CUSTOMER_FULL_NAME} = $data['fullname'];
//            $customer->{Customer::CUSTOMER_PHONE} = $data['phone'];
//            $customer->{Customer::CUSTOMER_ADDRESS} = $data['address'];
//            $customer->{Customer::CUSTOMER_AVATAR} = 'images/user-avatar.jpg';
//
//            if ($customer->save())
//                return redirect()->back()->with('success', 'Bạn đã đăng ký thành công!');
//            else {
//                return redirect()->back()->withInput()->withErrors(['Bạn đã đăng ký thất bại! Do lỗi của server bạn hãy thử đăng ký lại một lần nữa!']);
//            }
//        }

        return view('pages.clients.accounts.register');
    }

    public function login(LoginRequest $request)
    {
//        if ($request->isMethod('POST')) {
//            $data = $request->all();
//            $user = array(
//                'user_name' => $data['user_name'],
//                'password' => $data['password']
//            );
//            if (Auth::guard('admins')->attempt($user)) {
//                if (Auth::user()->active_status == 1) {
//                    return redirect('admins/profile');
//                } else {
//                    Auth::logout();
//                    return redirect()->back()->withInput()->withErrors('Tài khoản của bạn không tồn tại hay chưa kích hoạt! Hãy liên hệ với quản trị viên !');
//                }
//            }
//        }

        return view('pages.clients.accounts.login');
    }

    public function logout(Request $request)
    {
        if (Auth::user() != null) {
            DB::table('ms_administrators')->where('admin_id', Auth::user()->admin_id)->update(['last_login_time'=>Carbon::now()]);
        }

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/admins');
    }
}
