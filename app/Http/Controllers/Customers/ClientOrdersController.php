<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\ClientRequest;
use App\Http\Requests\Clients\OrderRequest;
use App\Models\Entities\Order;
use App\Models\Entities\OrderDetail;
use App\Models\Entities\Product;
use App\Models\Entities\ShopGift;
use App\Models\ViewModels\OrderVM;
use App\Models\ViewModels\CheckoutInfoVM;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClientOrdersController extends Controller
{
    public function listProduct(ClientRequest $request)
    {
        //Thông tin chưa thanh toán lưu lại
        $shopOrder = session('checkout');
        //Thông mã/sdt giảm giá
        $gift = session('gift');
        $order_total = 0;
        $order_amount = 0;
        //Danh sách sản phẩm trong giỏ hàng
        $orders = session('orders');
        if ($orders->count() > 0)
            foreach ($orders as $item) {
                $order_total += $item->{OrderVM::TOTAL};
                $order_amount += $item->{OrderVM::AMOUNT};
            }
        else
            $request->session()->put('gift', null);
        $order_discount = $order_total - $order_amount;
        if ($gift != null && $order_amount > 0) {
            $gift['discount'] = $gift['pattern'] == 1 ? $gift['discount'] : $order_amount * $gift['percent'] / 100;
            $order_amount = $order_amount - $gift['discount'] > 0 ? $order_amount - $gift['discount'] : 0;
        }

        return view('pages.clients.orders.listorder', compact('orders', 'order_amount', 'order_total', 'order_discount', 'gift', 'shopOrder'));
    }
    public function addProduct(ClientRequest $request)
    {
        $data = $request->all();

        $orders = session('orders');
        if ($orders->count() > 0 && $orders->where('id', $data['product_id'])->count() > 0) {
            foreach ($orders as $item) {
                if ($item->id == $data['product_id']) {
                    if (isset($data['quantity']))
                        $item->{OrderVM::QUANTITY} = $data['quantity'];
                    else
                        $item->{OrderVM::QUANTITY} = $item->{OrderVM::QUANTITY} + 1;

                    $item->{OrderVM::TOTAL} = $item->{OrderVM::PRICE} * $item->{OrderVM::QUANTITY};
                    $amount = $item->{OrderVM::DISCOUNT} != -1 ? $item->{OrderVM::DISCOUNT} : $item->{OrderVM::PRICE};
                    $item->{OrderVM::AMOUNT} = $amount * $item->{OrderVM::QUANTITY};
                    break;
                }
            }
        } else {
            $pro = Product::where('product_id', $data['product_id'])->select(
                'ms_products.product_id',
                'ms_products.product_name',
                'ms_products.product_description',
                'ms_products.product_img_1',
                'ms_products.product_price',
                'ms_products.product_discount'
            )->first();
            if ($pro == null)
                return redirect()->back();
            $order = new OrderVM();
            $order->{OrderVM::ID} = $pro->product_id;
            $order->{OrderVM::NAME} = $pro->{Product::PRODUCT_NAME};
            $order->{OrderVM::DESCRIPTION} = $pro->{Product::PRODUCT_DESCRIPTION};
            $order->{OrderVM::PRICE} = $pro->{Product::PRODUCT_PRICE};
            $order->{OrderVM::DISCOUNT} = $pro->{Product::PRODUCT_DISCOUNT} != -1 ? $pro->{Product::PRODUCT_DISCOUNT} : -1;
            $order->{OrderVM::IMAGE} = $pro->{Product::PRODUCT_IMG_1};
            $order->{OrderVM::QUANTITY} = 1;
            $order->{OrderVM::TOTAL} = $order->{OrderVM::PRICE};
            $order->{OrderVM::AMOUNT} = $pro->{Product::PRODUCT_DISCOUNT} != -1 ? $pro->{Product::PRODUCT_DISCOUNT} : $order->{OrderVM::PRICE};
            $orders->add($order);
        }

        $request->session()->put('orders', $orders);

        return redirect()->back();
    }
    public function minusProduct(ClientRequest $request)
    {
        $data = $request->all();

        $orders = session('orders');
        if ($orders->count() > 0 && $orders->where('id', $data['product_id'])->count() > 0) {
            foreach ($orders as $item) {
                if ($item->id == $data['product_id']) {
                    if (isset($data['quantity']))
                        $item->{OrderVM::QUANTITY} = $data['quantity'];
                    else
                        $item->{OrderVM::QUANTITY} = $item->{OrderVM::QUANTITY} - 1;

                    if ($item->{OrderVM::QUANTITY} <= 0) {
                        $newOrders = collect();
                        foreach ($orders as $item) {
                            if ($item->id == $data['product_id']) {
                                continue;
                            }
                            $newOrders->add($item);
                        }
                        $request->session()->put('orders', $newOrders);
                        return redirect()->back();
                    }

                    $item->{OrderVM::TOTAL} = $item->{OrderVM::PRICE} * $item->{OrderVM::QUANTITY};
                    $amount = $item->{OrderVM::DISCOUNT} != -1 ? $item->{OrderVM::DISCOUNT} : $item->{OrderVM::PRICE};
                    $item->{OrderVM::AMOUNT} = $amount * $item->{OrderVM::QUANTITY};
                    break;
                }
            }
        }

        $request->session()->put('orders', $orders);

        return redirect()->back();
    }

    public function removeProduct(ClientRequest $request)
    {
        $data = $request->all();

        $orders = session('orders');
        $newOrders = collect();
        if ($orders->count() > 0 && $orders->where('id', $data['product_id'])->count() > 0) {
            foreach ($orders as $item) {
                if ($item->id == $data['product_id']) {
                    continue;
                }
                $newOrders->add($item);
            }
        }

        $request->session()->put('orders', $newOrders);

        return redirect()->back();
    }

    public function discountProduct(ClientRequest $request)
    {
        $data = $request->all();
        $current = Carbon::now()->format('Y-m-d');
        $code = preg_replace('/[^A-Za-z0-9\-]/', '', $data['giftcode']);
        // $gift = ShopGift::where([
        //     ['gift_code', $code],
        //     ['gift_start_time', '<=', $current],
        //     ['gift_end_time', '>=', $current],
        //     ['gift_use_count', '>', 0],
        //     ['active_status', 1],
        // ])->select('ms_shop_gifts.gift_code', 'ms_shop_gifts.gift_discount')->first();
        //Lay danh sach code dang kich hoat
        $gifts = ShopGift::where([['gift_code', $code], ['active_status', 1]])
            ->select('gift_id', 'gift_code', 'gift_start_time', 'gift_end_time', 'gift_use_count', 'gift_pattern', 'gift_used', 'gift_discount')->get();
        if ($gifts == null)
            return redirect()->back()->withInput()->withErrors(['Mã hoặc SĐT Giảm Giá: <strong>' . $code . '</strong> không chính xác!<br /> Vui lòng liên hệ 0818777733 để được hỗ trợ']);
        else {
            //Kiểm tra thời gian sử dụng
            $giftDates = collect();
            foreach ($gifts as $item) {
                if (
                    date("Y-m-d", strtotime($item->gift_start_time)) <= $current
                    && date("Y-m-d", strtotime($item->gift_end_time)) >= $current
                ) {
                    $giftDates->add($item);
                }
            }
            if ($giftDates->count() <= 0)
                return redirect()->back()->withInput()->withErrors(['Mã hoặc SĐT Giảm Giá: <strong>' . $code . '</strong> đã hết thời gian sử dụng!<br /> Vui lòng liên hệ 0818777733 để được hỗ trợ']);
            foreach ($giftDates as $item) {
                if (($item->gift_use_count - $item->gift_used) > 0) {
                    $giftShow = ['id' => $item->gift_id, 'code' => $item->gift_code, 'pattern' => $item->gift_pattern, 'percent' => $item->gift_discount, 'discount' => $item->gift_discount];
                    $request->session()->put('gift', $giftShow);

                    return redirect()->back();
                }
            }
            return redirect()->back()->withInput()->withErrors(['Mã hoặc SĐT Giảm Giá: <strong>' . $code . '</strong> đã hết số lần sử dụng!<br /> Vui lòng liên hệ 0818777733 để được hỗ trợ']);
        }
    }

    public function viewCheckout(OrderRequest $request)
    {
        //Lay du lieu khoang thoi gian giao hang
        $selected = $request->dWeekdays;
        $deliveryDay = '';
        if (isset($selected)) {
            foreach ($selected as $value) {
                $deliveryDay .= $value . ' ,';
            }
        } else
            return redirect()->back()->withInput()->withErrors(['Bạn cần chọn ít nhất 1 ngày trong tuần.']);

        $deliveryTime = $request->dTimeday;
        //Tao doi tuong hoa don
        $data = $request->all();

        $shopOrder = new CheckoutInfoVM();
        $shopOrder->{CheckoutInfoVM::DELIVERY_TIME} = Str::replaceLast(' ,', ': ' . $deliveryTime, $deliveryDay);
        $shopOrder->{CheckoutInfoVM::FULLNAME} = $data[CheckoutInfoVM::FULLNAME];
        $shopOrder->{CheckoutInfoVM::EMAIL} = $data[CheckoutInfoVM::EMAIL];
        $shopOrder->{CheckoutInfoVM::PHONENUMBER} = $data[CheckoutInfoVM::PHONENUMBER];
        $shopOrder->{CheckoutInfoVM::ADDRESS} = $data[CheckoutInfoVM::ADDRESS];
        $shopOrder->{CheckoutInfoVM::DELIVERY_NOTE} = $data[CheckoutInfoVM::DELIVERY_NOTE];

        $gift = session('gift');
        $order_total = 0;
        $order_amount = 0;

        $orders = session('orders');
        if ($orders->count() > 0)
            foreach ($orders as $item) {
                $order_total += $item->{OrderVM::TOTAL};
                $order_amount += $item->{OrderVM::AMOUNT};
            }
        else
            $request->session()->put('gift', null);
        //So tien duoc giam
        $order_discount = $order_total - $order_amount;

        // Xu ly Ma/SDT giam gia
        if ($gift != null && $order_amount > 0) {
            $gift['discount'] = $gift['pattern'] == 1 ? $gift['discount'] : $order_amount * $gift['percent'] / 100;
            $order_amount = $order_amount - $gift['discount'] > 0 ? $order_amount - $gift['discount'] : 0;

            $shopOrder->{CheckoutInfoVM::GIFT_ID} = $gift['id'];
            $shopOrder->{CheckoutInfoVM::GIFT} = $gift['code'];
            $shopOrder->{CheckoutInfoVM::GIFT_DISCOUNT} =  $gift['discount'];
        }

        $shopOrder->{CheckoutInfoVM::STATE_ID} = 1; //Trang thai Hoa Don
        $shopOrder->{CheckoutInfoVM::SHIP_FEE} = 0; //Phi van chuyen
        $shopOrder->{CheckoutInfoVM::TAX} = 0;     //Thue
        $shopOrder->{CheckoutInfoVM::DISCOUNT} =  $order_discount;
        $shopOrder->{CheckoutInfoVM::TOTAL} = $order_total;
        $shopOrder->{CheckoutInfoVM::AMOUNT} = $order_amount;

        $request->session()->put('checkout', $shopOrder);

        return view('pages.clients.orders.checkout', compact('shopOrder', 'orders'));
    }


    public function checkout(ClientRequest $request)
    {
        $shopOrder = session('checkout');
        $orders = session('orders');
        try {
            if (isset($shopOrder)) {
                //Tao order va luu lai
                $newOrder = new Order();

                $newOrder->{Order::ORDER_STATE_ID} = $shopOrder->{CheckoutInfoVM::STATE_ID};
                $newOrder->{Order::ORDER_SHIP_FEE} = $shopOrder->{CheckoutInfoVM::SHIP_FEE};
                $newOrder->{Order::ORDER_TAX} = $shopOrder->{CheckoutInfoVM::TAX};
                $newOrder->{Order::ORDER_DISCOUNT} = $shopOrder->{CheckoutInfoVM::DISCOUNT};
                $newOrder->{Order::ORDER_TOTAL} = $shopOrder->{CheckoutInfoVM::TOTAL};
                $newOrder->{Order::ORDER_AMOUNT} = $shopOrder->{CheckoutInfoVM::AMOUNT};

                $newOrder->{Order::ORDER_FULL_NAME} = $shopOrder->{CheckoutInfoVM::FULLNAME};
                $newOrder->{Order::ORDER_EMAIL} = $shopOrder->{CheckoutInfoVM::EMAIL};
                $newOrder->{Order::ORDER_PHONE} = $shopOrder->{CheckoutInfoVM::PHONENUMBER};
                $newOrder->{Order::ORDER_ADDRESS} = $shopOrder->{CheckoutInfoVM::ADDRESS};
                $newOrder->{Order::ORDER_NOTE} = $shopOrder->{CheckoutInfoVM::DELIVERY_NOTE};
                $newOrder->{Order::ORDER_DELIVERY_TIME} = $shopOrder->{CheckoutInfoVM::DELIVERY_TIME};

                $ssgift = session('gift');
                if (isset($ssgift)) {
                    $newOrder->{Order::ORDER_GIFT_ID} = $shopOrder->{CheckoutInfoVM::GIFT_ID};
                    $newOrder->{Order::ORDER_GIFT} = $shopOrder->{CheckoutInfoVM::GIFT};
                    $newOrder->{Order::ORDER_GIFT_DISCOUNT} = $shopOrder->{CheckoutInfoVM::GIFT_DISCOUNT};
                }
                DB::transaction(function () use ($newOrder, $orders, $request) {
                    if ($newOrder->save()) {
                        if (isset($newOrder->{Order::ORDER_GIFT})) {
                            $gift =  ShopGift::where('gift_id', $newOrder->{Order::ORDER_GIFT_ID})->first();
                            if (isset($gift)) {
                                $used = $gift->{ShopGift::GIFT_USED} + 1;
                                ShopGift::where('gift_id', $newOrder->{Order::ORDER_GIFT_ID})->update(['gift_used' => $used]);
                            }
                        }
                        $id = $newOrder->order_id;
                        $request->session()->put('orID', $id);
                        $orderDetails = collect();
                        foreach ($orders as $item) {
                            $order = new OrderDetail();
                            $order->{OrderDetail::ORDER_ID} =  $id;
                            $order->{OrderDetail::PRODUCT_ID} =  $item->{OrderVM::ID};
                            $order->{OrderDetail::ORDER_DETAIL_PRICE} = $item->{OrderVM::PRICE};
                            $order->{OrderDetail::ORDER_DETAIL_DISCOUNT} = $item->{OrderVM::DISCOUNT};
                            $order->{OrderDetail::ORDER_DETAIL_QUANTITY} =  $item->{OrderVM::QUANTITY};
                            $order->{OrderDetail::ORDER_DETAIL_TOTAL} = $item->{OrderVM::TOTAL};
                            $order->{OrderDetail::ORDER_DETAIL_AMOUNT} =  $item->{OrderVM::AMOUNT};
                            $orderDetails->add($order);
                        }
                        OrderDetail::insert($orderDetails->toArray());
                    }
                }, 5);

                DB::commit();
                $id = session('orID');
                $showOr = Str::padRight('MHD', 9 - strlen($id), 0) . $id;
                $request->session()->forget('checkout');
                $request->session()->forget('gift');
                $request->session()->put('orders', collect());
                $request->session()->forget('orID');
                $msgSuccess = 'Gửi Đơn Hàng Thành Công! Mã đơn hàng của bạn là: <strong class="font-weight-bold">' . $showOr . '</strong>';
                return view('pages.clients.orders.success', compact('shopOrder', 'orders', 'msgSuccess', 'showOr'));
            }
            return redirect()->route('index');
        } catch (\Exception $e) {
            DB::rollBack();
            $msgFail = 'Xin chân thành xin lỗi quý khách!<br/> Bên serve đã xảy lỗi, Mong quý khách hãy thực hiện lại 1 lần nữa hoặc liên hệ 0818777733 để được hỗ trợ!';
            return view('pages.clients.orders.success', compact('shopOrder', 'orders', 'msgFail'));
        }
    }

    public function getCount(ClientRequest $request)
    {
        $orders = session('orders');
        $data = $orders->count();
        echo json_encode($data);
        exit;
    }
}
