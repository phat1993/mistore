<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\ClientRequest;
use App\Http\Requests\Clients\SearchRequest;
use App\Models\Entities\Banner;
use App\Models\Entities\Product;
use App\Models\Entities\ProductCategory;
use App\Models\Entities\ProductType;
use App\Models\Entities\WebInfo;
use App\Models\ViewModels\FavoriteVM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index(ClientRequest $request)
    {
        $banners = Banner::where(Banner::ACTIVE_STATUS, 1)->get();
        // dd($banners);
        //San pham khuyen mai
        $lsDiscount = Product::where([
            ['ms_products.active_status', 1],
            ['ms_products.product_discount', '<>', '-1']
        ])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->orderBy('updated_at')->take(8)->get();

        //san pham moi nhat
        $lsUpdate = Product::where([['ms_products.active_status', 1]])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->orderBy('updated_at')->take(8)->get();

        //san pham tim kiem nhieu nhat
        $lsSearch = null;

        //san pham mua nhieu nhat
        $lsHot = null;

        //san pham goi y
        $lsRandom = Product::where([['ms_products.active_status', 1]])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->inRandomOrder()->take(8)->get();

        return view('pages.clients.index', compact('banners', 'lsDiscount', 'lsUpdate', 'lsSearch', 'lsHot', 'lsRandom'));
    }
    public function type(ClientRequest $request)
    {
        $type_slug = $request->type_slug;
        $type_obj = ProductType::where('web_canonical', 'like', $type_slug)
            ->select('web_canonical', 'product_type_id', DB::raw('UPPER(product_type_name) AS product_type_name'))->first();
        if ($type_obj == null)
            abort(404);
        $sql = '';
        $lock = true; //
        $types = ProductCategory::where('product_type_id', $type_obj->product_type_id)->get();
        $categories = array();
        if ($types->count() > 0) {
            foreach ($types as $item) {
                if ($lock) {
                    $sql .= 'ms_products.product_category_id = ' . $item->product_category_id;
                    $lock = false;
                } else
                    $sql .= ' OR ms_products.product_category_id = ' . $item->product_category_id;
            }
            $categories = Product::whereRaw($sql)->select(
                'ms_products.*',
                //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
                //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
                //Con lai thi lay link binh thuong
                DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                               WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                               ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
            )
                ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
                ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->get();
        }

        return view('pages.clients.type', compact('categories', 'type_obj'));
    }
    public function category(ClientRequest $request)
    {
        $type_obj = null;
        $type_slug = $request->type_slug;
        if (isset($type_slug))
            $type_obj = ProductType::where('web_canonical', 'like', $type_slug)
                ->select('web_canonical', 'product_type_id', DB::raw('UPPER(product_type_name) AS product_type_name'))->firstOrFail();

        $category_obj = null;
        $category_slug = $request->category_slug;
        if ($type_obj == null)
            $category_obj = ProductCategory::where('ms_product_categories.web_canonical', $category_slug)
                ->select(
                    'ms_product_categories.product_category_id',
                    'ms_product_categories.web_title',
                    'ms_product_categories.web_canonical',
                    'ms_product_categories.web_keywords',
                    'ms_product_categories.web_description',
                    DB::raw('UPPER(ms_product_categories.product_category_name) AS product_category_name')
                )->firstOrFail();
        else
            $category_obj = ProductCategory::where([['ms_product_categories.web_canonical', $category_slug], ['product_type_id', $type_obj->product_type_id]])
                ->select(
                    'ms_product_categories.product_category_id',
                    'ms_product_categories.web_title',
                    'ms_product_categories.web_canonical',
                    'ms_product_categories.web_keywords',
                    'ms_product_categories.web_description',
                    DB::raw('UPPER(ms_product_categories.product_category_name) AS product_category_name')
                )->firstOrFail();
        if ($category_obj == null)
            abort(404);

        $categories = Product::where('ms_products.product_category_id', $category_obj->product_category_id)->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->get();
        return view('pages.clients.category', compact('categories', 'type_obj', 'category_obj'));
    }

    public function detail(ClientRequest $request)
    {
        $type_obj = null;
        $type_slug = $request->type_slug;
        if (isset($type_slug))
            $type_obj = ProductType::where('web_canonical', 'like', $type_slug)
                ->select('web_canonical', 'product_type_id', DB::raw('UPPER(product_type_name) AS product_type_name'))->firstOrFail();
        $category_obj = null;
        $category_slug = $request->category_slug;
        $category_obj = null;
        if ($type_obj == null)
            $category_obj = ProductCategory::where('ms_product_categories.web_canonical', $category_slug)
                ->select(
                    'ms_product_categories.product_category_id',
                    'ms_product_categories.web_canonical',
                    DB::raw('UPPER(ms_product_categories.product_category_name) AS product_category_name')
                )->firstOrFail();
        else
            $category_obj = ProductCategory::where([['ms_product_categories.web_canonical', $category_slug], ['product_type_id', $type_obj->product_type_id]])
                ->select(
                    'ms_product_categories.product_category_id',
                    'ms_product_categories.web_canonical',
                    DB::raw('UPPER(ms_product_categories.product_category_name) AS product_category_name')
                )->firstOrFail();

        $slug = $request->slug;
        $product = null;
        if ($category_obj == null)
            $product = Product::where('ms_products.web_canonical', $slug)->select(
                'ms_products.*',
                //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
                //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
                //Con lai thi lay link binh thuong
                DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
            )
                ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
                ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->firstOrFail();
        else
            $product = Product::where([['ms_products.web_canonical', $slug], ['ms_products.product_category_id', $category_obj->product_category_id]])->select(
                'ms_products.*',
                //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
                //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
                //Con lai thi lay link binh thuong
                DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
            )
                ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
                ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')->firstOrFail();
        if ($product == null) {
            abort(404);
        } else {
            //Update luot view san pham
            $updated = DB::table('ms_products')->where('product_id', $product->product_id);
            $param = array();
            $param['product_view_count'] = $product->product_view_count + 1;
            $updated->update($param);
        }

        //Luu doi tuong da xem vao session
        $viewed = session('viewed');
        //Get ip cua client hien tai
        $ip = $request->getClientIp();

        if ($viewed->where('product', $product->product_id)->count() <= 0) {
            $view = new FavoriteVM();
            $view->{FavoriteVM::CUSTOMER} = 0;
            $view->{FavoriteVM::CLIENT_IP} = $ip;
            $view->{FavoriteVM::LINK} = $product->url_seo;
            $view->{FavoriteVM::PRODUCT} = $product->product_id;
            $view->{FavoriteVM::NAME} = $product->{Product::PRODUCT_NAME};
            $view->{FavoriteVM::PRICE} = $product->{Product::PRODUCT_PRICE};
            $view->{FavoriteVM::DISCOUNT} = $product->{Product::PRODUCT_DISCOUNT} != -1 ? $product->{Product::PRODUCT_DISCOUNT} : -1;
            $view->{FavoriteVM::IMAGE} = $product->{Product::PRODUCT_IMG_1};
            $viewed->add($view);
        }
        //Luu lai session
        $request->session()->put('viewed', $viewed);
        $favorites = session('favorites');
        return view('pages.clients.detail', compact('product', 'type_obj', 'category_obj', 'favorites', 'viewed'));
    }

    public function trend(ClientRequest $request)
    {
        $lsTrend = null;
        $navName = '';
        $trend_obj = $request->trend_obj;
        switch ($trend_obj) {
            case 'san-pham-khuyen-mai':
                //San pham khuyen mai
                $navName = 'SẢN PHẨM KHUYẾN MÃI';
                $lsTrend = Product::where([['ms_products.active_status', 1]])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')
                    ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->get();
                break;
            case 'san-pham-moi-nhat':
                //san pham moi nhat
                $navName = 'SẢN PHẨM MỚI NHẤT';
                $lsTrend = Product::where([['ms_products.active_status', 1]])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')
                    ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->get();

                break;
            case 'san-pham-goi-y':
                //san pham goi y
                $navName = 'SẢN PHẨM GỢI Ý';
                $lsTrend = Product::where([['ms_products.active_status', 1]])->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')
                    ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->get();

                break;
            case 'san-pham-duoc-tim-kiem-nhieu-nhat':
                //san pham tim kiem nhieu nhat
                // $navName ='SẢN PHẨM TÌM KIẾM NHIỀU NHẤT';
                $lsTrend = null;
                break;
            case 'san-pham-duoc-ban-nhieu-nhat':
                //san pham mua nhieu nhat
                // $navName ='SẢN PHẨM MUA NHIỀU NHẤT';
                $lsTrend = null;
                break;
        }
        if ($navName != '')
            return view('pages.clients.trend', compact('lsTrend', 'navName'));
        else
            abort(404);
    }

    public function about(ClientRequest $request)
    {
        $about =  WebInfo::where([[WebInfo::ACTIVE_STATUS, 1]])->orderBy('updated_at')->first();
        if ($about == null)
            abort(404);
        return view('pages.clients.about', compact('about'));
    }

    public function search(SearchRequest $request)
    {
        $data = $request->all();
        $key=$data['keysearch'];
        $rkey = preg_replace('/[^A-Za-z0-9\-]/', '', $key);
        $categories = Product::where('product_name', 'like', '%' . $rkey . '%')->select(
            'ms_products.*',
            //Neu truong hop(category.web_canonical is null) khong co category thi show link co ban
            //Neu truong hop(type.web_canonical is null) khong co type thi show link co category
            //Con lai thi lay link binh thuong
            DB::Raw("(CASE WHEN category.web_canonical IS NULL THEN CONCAT('san-pham/',ms_products.web_canonical)
                           WHEN type.web_canonical IS NULL THEN CONCAT('danh-muc/',category.web_canonical,'/',ms_products.web_canonical)
                           ELSE CONCAT('cua-hang/',type.web_canonical,'/',category.web_canonical,'/',ms_products.web_canonical) END) AS url_seo")
        )
            ->leftJoin('ms_product_categories AS category', 'category.product_category_id', '=', 'ms_products.product_category_id')
            ->leftJoin('ms_product_types AS type', 'type.product_type_id', '=', 'category.product_type_id')
            ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->get();
        return view('pages.clients.search', compact('categories', 'key'));
    }
    // //lay hien thi danh sach san pham tim kiem

    // public function searchProduct(ClientRequest $request, $key)
    // {
    //     // $data = $request->all();
    //     $rkey = preg_replace('/[^A-Za-z0-9\-]/', '', $key);
    //     $products = Product::where('product_name', 'like', '%' . $rkey . '%')
    //         ->select('product_name AS PRODUCTNAME', 'product_img_1 AS IMAGE', 'ms_products.product_category_id AS CID', DB::raw('CASE
    //     WHEN product_discount = -1 THEN product_price AS PRICE
    //     ELSE product_discount AS PRICE END'), 'ms_product_categories.product_type_id AS TID')
    //         ->leftJoin('ms_product_categories', 'ms_product_categories.product_category_id', '=', 'ms_products.product_category_id')->get();
    //     return json_decode($products, true);
    // }
}
