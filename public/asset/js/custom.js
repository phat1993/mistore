(function($) {
    "use strict"; // Start of use strict
    $('#sidebarCollapse').on('click', function() {

        if (typeof(Storage) !== "undefined") {
            if (sessionStorage.getItem("menuToggle") != null) {
                sessionStorage.removeItem("menuToggle");
            } else {
                sessionStorage.setItem("menuToggle", "collapsed");
            }
        }
        $('#sidebar').toggleClass("active");

        // var mnToggle = sessionStorage.getItem("menuToggle");
        // alert(mnToggle);

    });
    /*Loader Javascript
    -------------------*/
    var window_var = $(window);
    window_var.on('load', function() {
        $(".loading").fadeOut("fast");
    });
    // scroll to top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    // scroll to top End

    // theme color change
    var theme_settings = $(".theme-settings").find(".theme-color");
    theme_settings.on('click', function() {
        var val = $(this).attr('data-color');
        $('#style_theme').attr('href', 'css/' + val + '.css');
        $(".logo-change").attr('src', 'img/logo-' + val + '.png');

        theme_settings.removeClass('theme-active');
        theme_settings.addClass('theme-active');
        return false;
    });
    $(".theme-click").on('click', function() {
        $("#switcher").toggleClass("active");
        return false;
    });
    // theme color change End

    // var url = window.location.href;
    // var lcMenu = $("#msMenu li").find("a[href=\"" + url + "\"]");

    var url = window.location.pathname.split('/');
    var link = window.location.protocol + "//" + window.location.host + "/" + url[1] + "/" + url[2];
    var lcMenu = $("#msMenu li").find("a[href=\"" + link + "\"]");
    if (lcMenu != null) {
        var menuActive = lcMenu.parent().parent().parent();
        menuActive.addClass("active");
        menuActive.children('a').attr('aria-expanded', 'true');
        menuActive.children('ul').addClass('show');
    }
    var mnToggle = sessionStorage.getItem("menuToggle");
    if (mnToggle == "collapsed") {
        $('#sidebar').addClass('active');
    } else {
        $('#sidebar').removeClass('active');
    }


})(jQuery);