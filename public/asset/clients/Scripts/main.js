$(document).ready(function() {

    function n() {
        document.getElementById("to_top").style.display = document.body.scrollTop > 20 || document.documentElement.scrollTop > 20 ? "block" : "none"
    }


    $("#to_top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 200)
    })

    $(".owl-carousel-sale").owlCarousel({
        autoplay: !1,
        loop: !0,
        margin: 10,
        nav: !0,
        slideBy: 3,
        dots: !1,
        lazyLoad: !0,
        lazyLoadEager: !0,
        responsive: {
            0: {
                items: 2
            },
            993: {
                items: 4
            }
        }
    });

    $(".owl-carousel-news").owlCarousel({
        autoplay: !0,
        loop: !0,
        margin: 10,
        nav: !0,
        slideBy: 2,
        dots: !1,
        lazyLoad: !0,
        lazyLoadEager: !0,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            }
        }
    });

    $(".owl-carousel-event").owlCarousel({
        loop: !0,
        margin: 10,
        autoplay: !0,
        autoplayHoverPause: !0,
        nav: !0,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            }
        }
    });



    window.onscroll = function() {
        n();
    };


    // var o = null,
    //     u = $("#rsSearchItem");

    // $("#search_textbox").keyup(function() {
    //     o && clearTimeout(o);
    //     var n = "/search-product/"+$(this).val();
    //     n.length >= 2 ? o = setTimeout(function() {
    //         $.ajax({
    //             url: n,
    //             type: "GET",
    //             // dataType: "json",
    //             // contentType: "application/json;charset=utf-8;",
    //             // async: !1,
    //             // data: n,
    //             // JSON.stringify({
    //             //     strKeyword: n
    //             // }),
    //             success: function(data) {
    //                 alert(data);
    //                 // u.show().empty();
    //                 // n.rs ? n.listProduct.length > 0 ? n.listProduct.forEach(function(n) {
    //                 //     var i = $("<img/>").attr("src", n.IMAGE),
    //                 //         r = $("<p/>").text(n.PRODUCTNAME),
    //                 //         f = $("<sup/>").text("đ"),
    //                 //         e = $("<span/>").text(formatMoney(n.PRICE, 0)).append(f),
    //                 //         o = $('<div class="clear"/>'),
    //                 //         t = $('<a class="item-search-result" />').attr("href", n.PRODUCTLINK).append(i).append(r).append(e).append(o);
    //                 //     console.log(t);
    //                 //     u.append(t)
    //                 // }) : u.hide() : u.hide()
    //             }
    //         });
    //         $("#search_logo_icon .search-icon").hide();
    //         $("#search_logo_icon .close-x").show()
    //     }, 300) : (u.hide(), $("#search_logo_icon .search-icon").show(), $("#search_logo_icon .close-x").hide())
    // });
    // $("#search_textbox").focus(function() {
    //     $(this).trigger("keyup")
    // });
    // $("#search_logo_icon").click(function() {
    //     $("#rsSearchItem").hide();
    //     $("#search_logo_icon .search-icon").show();
    //     $("#search_logo_icon .close-x").hide();
    //     $("#search_textbox").val("")
    // });

    $.ajax({
        contentType: "application/json",
        dataType: "json",
        url: '/don-hang/count',
        success: function(data) {
            $('#cartCountMB').empty();
            $('#cartCountMB').append(data);

            $('#cartCount').empty();
            $('#cartCount').append(data);
        }
    });
});