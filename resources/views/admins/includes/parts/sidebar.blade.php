<!-- Sidebar -->
<nav id="sidebar" class="lorvens-bg">
    <div class="sidebar-header">

        <a href="#" class="logo">
            <div class="row align-items-center">
                <div class="col-3">
                    <img src="{{asset('/asset/images/minimal-logo.png')}}" alt="logo">
                </div>
                <div class="col-9">
                    <span class="logo" style="font-size: 1.5rem">AMIKSTORE</span>
                </div>
            </div>
        </a>
        <a href="#" class="minimal-logo"><img src="{{asset('/asset/images/minimal-logo.png')}}" alt="logo"></a>
    </div>
    <ul id="msMenu" class="list-unstyled components">

        <li>
            <a href="#nav-dashboard" data-toggle="collapse" aria-expanded="false">
                <span class="ti-home"></span> Thống kê
            </a>
            <ul class="collapse list-unstyled" id="nav-dashboard">
                <li>
                    <a href="#">Doanh Thu Theo Tháng</a>
                </li>
                <li>
                    <a href="#">Số Lượng Khách hàng</a>
                </li>
                <li>
                    <a href="#">Sản Phẩm Yêu Thích</a>
                </li>
            </ul>
        </li>

        {{-- <li class="nav-level-one"> --}}
        <li>
            <a href="#nav-uiSetting" data-toggle="collapse" aria-expanded="false">
                <span class="ti-settings"></span> Cài đặt chung
            </a>
            <ul class="list-unstyled collapse" aria-expanded="true" id="nav-uiSetting">
                <li>
                    <a href="{{url('/admins/webinfo')}}">Thông tin Website</a>
                </li>
                <li>
                    <a href="{{url('/admins/shopmenu')}}">Menu</a>
                </li>
                <li>
                    <a href="{{url('/admins/shopgift')}}">Gift</a>
                </li>
                <li>
                    <a href="{{url('/admins/banner')}}">Băng Rôn</a>
                </li>
                {{-- <li>
                    <a href="{{url('/admins/banner')}}">Cài Đặt Mặc Định</a>
                </li> --}}
            </ul>
        </li>
        <li>
            <a href="#nav-uiProduct" data-toggle="collapse" aria-expanded="false">
                <span class="ti-package"></span> Quản lý Sản Phẩm
            </a>
            <ul class="list-unstyled collapse" aria-expanded="true" id="nav-uiProduct">
                <li>
                    <a href="{{url('/admins/product-category')}}">Danh Mục</a>
                </li>
                <li>
                    <a href="{{url('/admins/product')}}">Sản Phẩm</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#nav-uiCustomer" data-toggle="collapse" aria-expanded="false">
                <span class="ti-package"></span> Quản lý Khách Hàng
            </a>
            <ul class="list-unstyled collapse" aria-expanded="true" id="nav-uiCustomer">
                <li>
                    <a href="{{url('/admins/customer')}}">Khách Hàng</a>
                </li>
                <li>
                    <a href="{{url('/admins/order')}}">Đơn Hàng</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- /Sidebar -->
