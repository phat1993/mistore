
<!-- Fav  Icon Link -->
<link rel="shortcut icon" type="image/png" href="{{asset('/asset/images/fav.png')}}">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="{{asset('/asset/css/bootstrap.min.css')}}">
<!-- themify icons CSS -->
<link rel="stylesheet" href="{{asset('/asset/css/themify-icons.css')}}">
<!-- font-awesome icons CSS -->
<link rel="stylesheet" href="{{asset('/asset/css/font-awesome.min.css')}}">
<!-- Animations CSS -->
<link rel="stylesheet" href="{{asset('/asset/css/animate.css')}}">
<!-- Main CSS -->
<link rel="stylesheet" href="{{asset('/asset/css/styles.css')}}">
<link rel="stylesheet" href="{{asset('/asset/css/red.css')}}" id="style_theme">
<link rel="stylesheet" href="{{asset('/asset/css/responsive.css')}}">
<link rel="stylesheet" href="{{asset('/asset/css/custom.css')}}">

<script src="{{asset('/asset/js/modernizr.min.js')}}"></script>
