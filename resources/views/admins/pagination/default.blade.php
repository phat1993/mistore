@if ($paginator->hasPages())
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-left">
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Trước</a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" tabindex="-1">Trước</a>
            </li>
        @endif

        @foreach ($elements as $element)

                @if (is_string($element))
                    <li class="page-item disabled">
                        <a class="page-link" href="#">{{ $element }}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                @endif

                @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="page-item active">
                                    <a class="page-link" href="#">{{ $page }}
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="page-link" href="{{ $url }}">{{ $page }}
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                @endif

        @endforeach

        @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}">Sau</a>
                </li>
        @else
                <li class="page-item disabled">
                    <a class="page-link" href="#">Sau</a>
                </li>
        @endif



    </ul>
</nav>
@endif

