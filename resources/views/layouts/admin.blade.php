<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Mikstore Admins - @yield('title')</title>
    @include('admins.includes.head')
    @yield('styles')
</head>
<body>

<div class="page_loader"></div>

@include('admins.includes.header')

<div class="wrapper">

    @include('admins.includes.parts.sidebar')

    <div id="content">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="ti-menu" id="sidebarCollapse"></span>
                </div>
                <ul class="nav justify-content-end">
                    {{-- <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target=".lorvens-modal-lg">
                            <span class="ti-search"></span>
                        </a>
                        <div class="modal fade lorvens-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog modal-lorvens">
                                <div class="modal-content lorvens-box-shadow">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Serach Client/Project:</h5>
                                        <span class="ti-close" data-dismiss="modal" aria-label="Close">
											</span>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="search-term" placeholder="Type text here">
                                                <button type="button" class="btn btn-lorvens lorvens-bg">
                                                    <span class="ti-location-arrow"></span> Search</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="ti-announcement"></span>
                        </a>
                        <div class="dropdown-menu lorvens-box-shadow notifications animated flipInY">
                            <h5>Thông Báo</h5>
                            {{-- <a class="dropdown-item" href="#">
                                <span class="ti-comment-alt"></span> New User Registered</a>
                            <a class="dropdown-item" href="#">
                                <span class="ti-help-alt"></span> Client asked to send Quotaion</a>
                            <a class="dropdown-item" href="#">
                                <span class="ti-time"></span> Have schedule meeting today</a>
                            <a class="dropdown-item" href="#">
                                <span class="ti-comment-alt"></span> New User Registered</a> --}}
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="ti-user"></span>
                        </a>
                        <div class="dropdown-menu lorvens-box-shadow profile animated flipInY">
                            <h5>John Willing</h5>
                            <a class="dropdown-item" href="/admins/profile">
                                <span class="ti-id-badge"></span> Hồ Sơ</a>
                            {{-- <a class="dropdown-item" href="#">
                                <span class="ti-settings"></span> Cài Đặt</a>
                            <a class="dropdown-item" href="#">
                                <span class="ti-help-alt"></span> Hỗ Trợ</a> --}}
                            <a class="dropdown-item" href="{{ asset('/admins/logout') }}">
                                <span class="ti-power-off"></span> Đăng Xuất</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /Top Navigation -->
        <!-- Breadcrumb -->
        @yield('topnavigation')
        <!-- /Breadcrumb -->
        <!-- Main Content -->
        <div class="container-fluid">
            <!-- Page Content -->
            @yield('content')
            <!-- /Page Content -->
        </div>
    </div>

    <!-- Back to Top -->
        <a id="back-to-top" href="#" class="back-to-top">
            <span class="ti-angle-up"></span>
        </a>
    <!-- /Back to Top -->

</div>


@include('admins.includes.footer')
@yield('javascript')

</div>
</body>
</html>
