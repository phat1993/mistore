<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>Mistore -  @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{asset('/asset/clients/Images/favicon.png')}}" type="image/x-icon">
    @include('clients.includes.head')
    @yield('styles')
</head>

<body>


<button id="to_top" title="Go to top" class="btn btn-default btn-sm">
    <img src="{{asset('/asset/clients/Images/app-icon/arrow-up.png')}}" />
</button>
<div class="topheader">
    <div class="topbarheader">

    </div>

    @include('clients.includes.header')

</div>

<div class="height-topbar"></div>
<div class="height-menu"></div>
@yield('content')

@include('clients.includes.footer')

@yield('javascript')
</body>
</html>


