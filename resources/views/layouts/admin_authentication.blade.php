<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mikstore Admins  - Đăng Nhập</title>
    @include('admins.includes.head')
</head>
<body style="height: 100%;background-color: #f1f1f1">
<div class="page_loader"></div>

@include('admins.includes.header')

<div class="wrapper">
<!-- Page Content -->
@yield('content')
<!-- /Page Content -->
</div>

@include('admins.includes.footer')

@yield('javascript')
</div>
</body>
</html>
