
    <div class="baner-web">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner ">
                @forelse ($banners as $item)
                <div  @if($loop->first) class="carousel-item active" @else  class="carousel-item" @endif>
                    <a href="{{ url($item->banner_url) }}">
                        @if ($item->banner_img!='')
                            <img class="d-block w-100" src="{{ asset('storage/'.$item->banner_img) }}">
                        @else
                            <img class="d-block w-100" src="{{ $item->banner_link }}">
                        @endif
                    </a>
                </div>
                @empty
                @endforelse
            </div>
            <a class="carousel-control-prev justify-content-start pl-2" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next justify-content-end pr-2" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="baner-mobile">
        <div class="owl-carousel owl-carousel-banermobile" style="margin-top: 0px">
            @forelse ($banners as $item)
            <a href="{{ url($item->banner_url) }}">
                @if ($item->banner_img!='')
                    <img class="d-block w-100" src="{{ asset('storage/'.$item->banner_img) }}">
                @else
                    <img class="d-block w-100" src="{{ $item->banner_link }}">
                @endif
            </a>
            @empty
            @endforelse
        </div>
    </div>
    {{-- <div class="home-baner3">
        <div class="owl-carousel owl-carousel-newv2">
                <div class="cover-item-owl-carousel">
                    <a href=tui-xach.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213830711567.jpg alt=T&#250;i x&#225;ch />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">T&#250;i x&#225;ch</div>
                        </div>
                    </a>
                </div>
                <div class="cover-item-owl-carousel">
                    <a href=giay-dep.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213855307172.jpg alt=Gi&#224;y d&#233;p />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">Gi&#224;y d&#233;p</div>
                        </div>
                    </a>
                </div>
                <div class="cover-item-owl-carousel">
                    <a href=tui-xach/danh-muc/backpack.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/ImageNews/Banner%2Ficon%2Fbalo.jpg alt=Balo nữ />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">Balo nữ</div>
                        </div>
                    </a>
                </div>
                <div class="cover-item-owl-carousel">
                    <a href=vi-cam-tay.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213943792165.jpg alt=V&#237; cầm tay />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">V&#237; cầm tay</div>
                        </div>
                    </a>
                </div>
                <div class="cover-item-owl-carousel">
                    <a href=mat-kinh.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220214006214800.jpg alt=Mắt k&#237;nh nữ />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">Mắt k&#237;nh nữ</div>
                        </div>
                    </a>
                </div>
                <div class="cover-item-owl-carousel">
                    <a href=pages/giay-sandal-nu.html class="disable-text-decoration">
                            <img class="img-banner-3" src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220214013683534.jpg alt=Sandal nữ />
                        <div class="m-2">
                            <div class="text-baner3 disable-text-decoration">Sandals</div>
                        </div>
                    </a>
                </div>

        </div>
    </div> --}}
    {{-- <div class="banner3-img">
            <div class="baner2-mob-card">
                <a href="tui-xach.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213830711567.jpg />
                    </div>
                    <div class="text-baner3">T&#250;i x&#225;ch</div>
                </a>
            </div>
            <div class="baner2-mob-card">
                <a href="giay-dep.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213855307172.jpg />
                    </div>
                    <div class="text-baner3">Gi&#224;y d&#233;p</div>
                </a>
            </div>
            <div class="baner2-mob-card">
                <a href="tui-xach/danh-muc/backpack.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/ImageNews/Banner%2Ficon%2Fbalo.jpg />
                    </div>
                    <div class="text-baner3">Balo nữ</div>
                </a>
            </div>
            <div class="baner2-mob-card">
                <a href="vi-cam-tay.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220213943792165.jpg />
                    </div>
                    <div class="text-baner3">V&#237; cầm tay</div>
                </a>
            </div>
            <div class="baner2-mob-card">
                <a href="mat-kinh.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220214006214800.jpg />
                    </div>
                    <div class="text-baner3">Mắt k&#237;nh nữ</div>
                </a>
            </div>
            <div class="baner2-mob-card">
                <a href="pages/giay-sandal-nu.html">
                    <div>
                            <img src=https://cdn.sablanca.vn/SmallBannerImage/smallbanner_020220214013683534.jpg />
                    </div>
                    <div class="text-baner3">Sandals</div>
                </a>
            </div>
    </div> --}}
