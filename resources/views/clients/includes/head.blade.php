
	{{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89204504-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-89204504-1');
    </script>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
	<script>
	  window.OneSignal = window.OneSignal || [];
	  OneSignal.push(function() {
		OneSignal.init({
		  appId: "e36cf3e1-d4f4-4c22-aa6e-3eb688ed98b7",
		  notifyButton: {
			enable: true,
		  }
		});
	  });
	</script>
    --}}
{{--
    <meta name=keywords content="gi&#224;y d&#233;p nữ thời trang, t&#250;i x&#225;ch nữ thời trang, v&#237; nữ thời trang, ba l&#244; nữ thời trang">
    <meta data-react-helmet="true" name="description" content="Sablanca thương hiệu thời trang ✅ Gi&#224;y, ✅ T&#250;i, ✅ V&#237; h&#224;ng đầu Việt Nam. Nguồn cảm hứng thời trang quốc tế được Sablanca thổi v&#224;o từng thiết kế, để mỗi phụ nữ Việt tự tin sở hữu những sản phẩm mang vẻ đẹp đơn giản nhưng vẫn tinh tế; tự do trải nghiệm sự &#234;m &#225;i v&#224; thời trang" />
    <link rel=canonical href="index.html">
    <meta http-equiv=x-dns-prefetch-control content="on"> --}}
    {{-- <link rel=dns-prefetch href="https://cdn.sablanca.vn/">
    <link rel="dns-prefetch" href="https://www.facebook.com/">
    <link rel="dns-prefetch" href="https://connect.facebook.net/">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com/">
    <link rel="dns-prefetch" href="https://chimpstatic.com/"> --}}
    {{-- <meta name=robots content="index, follow">
    <meta name=theme-color content="#FF8080">
    <meta name=format-detection content="telephone=no">
    <meta http-equiv=audience content="General">
    <meta name=resource-type content="Document">
    <meta name=distribution content="Global">
    <meta name=revisit-after content="1 days">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta http-equiv=x-dns-prefetch-control content="on"> --}}

    {{-- <!-- FACEBOOK OPEN GRAPH -->
    <meta property="og:site_name" content="sablanca.vn" />
    <meta property="og:rich_attachment" content="true" />
    <meta property="article:publisher" content="https://www.facebook.com/Sablanca.vn" />
    <meta property="fb:app_id" content="1140916179441507" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://sablanca.vn/Images/banner_facebook.jpg" />

    <meta property="og:url" content="https://sablanca.vn/" />
    <meta property="og:title" content="Sablanca - Phong c&#225;ch thời trang theo xu hướng quốc tế" />
    <meta property="og:description" content="Sablanca thương hiệu thời trang gi&#224;y, t&#250;i, v&#237; h&#224;ng đầu Việt Nam. Nguồn cảm hứng thời trang quốc tế được Sablanca thổi v&#224;o từng thiết kế, để mỗi phụ nữ Việt tự tin sở hữu những sản phẩm mang vẻ đẹp đơn giản nhưng vẫn tinh tế; tự do trải nghiệm sự &#234;m &#225;i v&#224; thời trang" />
    <meta property="article:tag" content="Sablanca - Phong c&#225;ch thời trang theo xu hướng quốc tế" />

    <meta itemprop="name" content="Sablanca - Phong c&#225;ch thời trang theo xu hướng quốc tế">
    <meta itemprop="url" content="https://sablanca.vn/">
    <meta itemprop="image" content="https://sablanca.vn/Images/banner_facebook.jpg"> --}}

    <link rel="stylesheet" href="{{asset('/asset/clients/Content/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/asset/clients/Content/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('/asset/clients/Content/styles.min.css')}}">
    <link rel="stylesheet" href="{{asset('/asset/clients/Content/lightsliderc36f.css')}}">
	<link href="{{asset('/asset/clients/Content/jquery.datetimepicker.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/asset/clients/Content/jquery.fancybox.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/asset/clients/Content/font-awesome.min.css')}}" rel="stylesheet" />


    <script src="{{asset('/asset/clients/Scripts/numeral.min.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/sweetalert.min.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/main.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/lightslider.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/jquery.zoom.js')}}"></script>
	<script src="{{asset('/asset/clients/Scripts/jquery.datetimepicker.js')}}"></script>
    <script src="{{asset('/asset/clients/Scripts/jquery.lazy.min.js')}}"></script>
	<script src="{{asset('/asset/clients/Scripts/jquery.fancybox.min.js')}}"></script>
{{--
    <!-- Google Tag Manager SABLANCA -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5JHQNZX');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager -->
    <!-- <script> -->
        <!-- (function (w, d, s, l, i) { -->
            <!-- w[l] = w[l] || []; -->
            <!-- w[l].push({ -->
                <!-- 'gtm.start': new Date().getTime(), event: 'gtm.js' -->
            <!-- }); -->
            <!-- var f = d.getElementsByTagName(s)[0], -->
                <!-- j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; -->
            <!-- j.async = true; -->
            <!-- j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; -->
            <!-- f.parentNode.insertBefore(j, f); -->
        <!-- })(window, document, 'script', 'dataLayer', 'GTM-T7PBRNX'); -->
    <!-- </script> -->

    <!-- End Google Tag Manager -->
    <script id="mcjs">!function (c, h, i, m, p) { m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p) }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/835634381f04969dd9b7e655c/65417eea1f1cca524c0ed5190.js");</script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
            n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '267496850801528');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=267496850801528&amp;ev=PageView&amp;noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89204504-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-89204504-1', { 'optimize_id': 'GTM-TK2DK49'});
	</script>
	<!-- End global site tag (gtag.js) - Google Analytics -->

	<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-89204504-1', 'auto');
        ga('require', 'ec');
    </script>

	<script>
	  (function() {
		var ta = document.createElement('script'); ta.type = 'text/javascript'; ta.async = true;
		ta.src = 'https://analytics.tiktok.com/i18n/pixel/sdk.js?sdkid=BUQBOUGUL34RKGR4FB2G';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ta, s);
	  })();
	</script> --}}
