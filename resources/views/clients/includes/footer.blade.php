
@php
    $info = session('web_info');
    $menus = session('menu_footer');
@endphp
<footer id="footer-wrapper">
    <div class="footer-web">
        <div class="footer-v1-backgroud">
            <div class="container v1-footer-cover">
                <div class="txt-connect-us">Kết nối với chúng tôi</div>
                <div class="icon-footer-us">
                    <a href="{{ $info->web_info_facebook }}" target="_blank">
                        <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/facebook-icon_v2.png')}}" />
                    </a>
                    <a href="{{ $info->web_info_youtube }}" target="_blank">
                        <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/youtobe-icon_v2.png')}}" />
                    </a>
                    <a href="{{ $info->web_info_shopee }}" target="_blank">
                        <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/shopee-icon_v2.png')}}" />
                    </a>
                    {{--<a href="{{ $info->web_info_instagram }}" target="_blank">--}}
                        {{--<img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/instagram-icon_v2.png')}}" />--}}
                    {{--</a>--}}
                    {{--<a href="{{ $info->web_info_zalo }}" target="_blank">--}}
                        {{--<img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/zalo-icon_v2.png')}}" />--}}
                    {{--</a>--}}
                </div>
                <div class="cover-phone-footer">
                    <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/phone-icon_v2.png')}}" />
                    <span class="phone-footer">{{ $info->web_info_phone }}
                        {{-- <span>(Miễn phí)</span> --}}
                </span>
                </div>
            </div>
        </div>
        <div class="footer-v2-backgroud">
            <div class="container v2-footer-cover">
                <div class="width20">
                    <div class="title-footer-v2">NHÓM SẢN PHẨM</div>
                    @forelse ($menus as $item)
                        <div><a href="{{ url('cua-hang/'.$item->web_canonical) }}">{{ $item->product_type_name }}</a></div>
                    @empty
                    @endforelse
                </div>
                <div class="width20">
                    <div class="title-footer-v2">THÔNG TIN</div>
                    <div><a href="{{ url('/about') }}">Giới Thiệu</a></div>
                    <div><a href="#">Liên Hệ</a></div>
                </div>
                {{-- <div class="width15">
                    <div class="title-footer-v2">CHÍNH SÁCH</div>
                    <div><a href="chinh-sach-chi-tiet/chinh-sach-sinh-nhat-khach-hang-than-thiet-sablanca.html"><span>Khách hàng thân thiết</span></a></div>
                    <div><a href="chinh-sach-chi-tiet/dieu-khoan-su-dung.html"><span>Điều khoản sử dụng</span></a></div>
                    <div><a href="chinh-sach-chi-tiet/chinh-sach-thanh-toan---van-chuyen.html"><span>Thanh toán - vận chuyển</span></a></div>
                    <div><a href="chinh-sach-chi-tiet/chinh-sach-bao-hanh-san-pham-tron-doi.html"><span>Bảo hành trọn đời</span></a></div>
                    <div><a href="chinh-sach-chi-tiet/chinh-sach-bao-mat.html"><span>Chính sách bảo mật</span></a></div>
                    <div><a href="chinh-sach-chi-tiet/chinh-sach-doi-san-pham-trong-vong-30-ngay-sablanca.html"><span>Chính sách đổi sản phẩm</span></a></div>
                </div>
                <div class="width15">
                    <div class="title-footer-v2">HƯỚNG DÃN</div>
                    <div><a href="huong-dan-chi-tiet/huong-dan-mua-hang-online.html"><span>Hướng dẫn mua hàng</span></a></div>
                    <div><a href="huong-dan-chi-tiet/huong-dan-chon-size-giay-dep.html"><span>Hướng dẫn chọn size</span></a></div>
                    <div><a href="huong-dan-chi-tiet/huong-dan-dang-ky-tai-khoan-thanh-vien-moi-va-doi-mat-khau-tai-khoan-cho-vip-member-sablanca.html"><span>Hướng dẫn tạo tài khoản</span></a></div>

                </div> --}}
                <div class="width40">
                    <div class="d-flex mb-5">
                        <div class="width50 d-flex align-self-center">
                            {{-- <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/visa-icon_v2.png')}}" />
                            <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/master-icon_v2.png')}}" />
                            <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/atm-pay-icon_v2.png')}}" />
                            <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/zalo-pay-icon_v2.png')}}" /> --}}
                        </div>
                        {{--<div class="width50">--}}
                            {{--<a href="#" target="_blank"><img src="{{asset('/asset/clients/Images/BoCongThuong.png')}}" class="bct-footer-v2 "></a>--}}
                        {{--</div>--}}
                    </div>
                    <div class="v2-footer-info">
                        <div>
                            Bản quyền thuộc Mistore
                        </div>
                        <div>
                            Địa chỉ: {{ $info->web_info_address }}
                        </div>
                        {{--<div>--}}
                            {{--Mã số thuế: XXXXXXXXXXXXXXXXXXXX--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-mob">
        <div class="footer-v1-backgroud" style="height: 20px;">

        </div>
        <div class="footer-v2-backgroud">
            <div class="padding-30">
                <div class="d-flex">
                    <div class="width50">
                        <div class="footer-mob-title title-footer-click" id="footer-group-item"><i class="fa fa-angle-right"></i> <span> Nhóm sản phẩm</span></div>
                        <div class="footer-mob-subtitle footer-group-item">
                            @forelse ($menus as $item)
                            <div><a href="{{ url('cua-hang/'.$item->web_canonical) }}">{{ $item->product_type_name }}</a></div>
                            @empty
                            @endforelse
                        </div>
                        <div class="footer-mob-title title-footer-click" id="footer-info"><i class="fa fa-angle-right"></i> <span> Thông tin</span></div>
                        <div class="footer-mob-subtitle footer-info">
                            <div><a href="{{ url('/about') }}">Giới Thiệu</a></div>
                            <div><a href="#">Liên Hệ</a></div>
                        </div>
                        {{-- <div class="footer-mob-title title-footer-click" id="footer-bolicy"><i class="fa fa-angle-right"></i> <span>Chính sách</span></div>
                        <div class="footer-mob-subtitle footer-bolicy">

                        </div>
                        <div class="footer-mob-title title-footer-click" id="footer-guide"><i class="fa fa-angle-right"></i> <span> Hướng đẫn</span></div>
                        <div class="footer-mob-subtitle footer-guide"></div> --}}
                    </div>
                    <div class="width50">
                        <div class="phone-mob-footer"><span class="pr-1"><i class="fa fa-phone"></i></span>
                            {{ $info->web_info_phone }}
                            {{-- <span style="display: inline-block;">(Miễn phí)</span> --}}
                        </div>
                    </div>
                </div>
                <hr />
                <div>
                    <div class="footer-mob-title">Kết nói với chúng tôi</div>
                    <div class="icon-footer-us">
                        <a href="{{ $info->web_info_facebook }}" target="_blank">
                            <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/facebook-icon_v2.png')}}" />
                        </a>
                        <a href="{{ $info->web_info_youtube }}" target="_blank">
                            <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/youtobe-icon_v2.png')}}" />
                        </a>
                        <a href="{{ $info->web_info_shopee }}" target="_blank">
                            <img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/shopee-icon_v2.png')}}" />
                        </a>
                        {{--<a href="{{ $info->web_info_instagram }}" target="_blank">--}}
                            {{--<img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/instagram-icon_v2.png')}}" />--}}
                        {{--</a>--}}
                        {{--<a href="{{ $info->web_info_zalo }}" target="_blank">--}}
                            {{--<img class="icon-footer-v1" src="{{asset('/asset/clients/Images/icon/zalo-icon_v2.png')}}" />--}}
                        {{--</a>--}}
                    </div>
                </div>
                <hr />
                <div>
                    <div class="width50 d-flex align-self-center">
                        {{-- <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/visa-icon_v2.png')}}" />
                        <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/master-icon_v2.png')}}" />
                        <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/atm-pay-icon_v2.png')}}" />
                        <img class="icon-footer-v2" src="{{asset('/asset/clients/Images/icon/zalo-pay-icon_v2.png')}}" /> --}}
                    </div>
                    <div class="d-flex">
                        <div class="width60 ownner-footer">
                            <div>
                                Bản quyền thuộc Mistore
                            </div>
                            <div>
                                Địa chỉ: {{ $info->web_info_address }}
                            </div>
                            {{--<div>--}}
                                {{--Mã số thuế: XXXXXXXXXXX--}}
                            {{--</div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

