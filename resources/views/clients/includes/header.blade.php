
@php
    $info = session('web_info');
    $menus = session('menu');
@endphp

    <header id="back_top" class="row no-gutters">
        <script>
            var aboveHeight = $('header')[0].clientHeight;
            $(window).scroll(function () {
                if ($(window).scrollTop() > aboveHeight) {
                    $('sticknav').addClass('boxed-group-fixed').css('top', '0').next().css('padding-top', '60px');
                } else {
                    $('sticknav').removeClass('boxed-group-fixed').next().css('padding-top', '0');
                }
            });
        </script>


        <div id="logo" class="col-xl-2">
            <a href="{{ url('/trang-chu') }}">
                <img  src="{{asset('/asset/clients/Images/logo-web.jpg')}}" alt="MISTORE" />
            </a>
        </div>
        <div id="right_logo" class="col-xl-10">
            <div id="top_right_logo" class="row align-items-center justify-content-around" style="margin-top: 20px;">
                <div class="@*col-12 col-sm-12 col-md-12*@ col-lg-3 search-web">
                    <form action="{{ url('/tim-kiem') }}" method="get">
                        {{csrf_field()}}
                        <input type="text" id="search_textbox" name="keysearch" autocomplete="off" placeholder="Nhập vào tên sản phẩm..." />
                        <button type="submit" id="search_logo_icon" class="btn btn-link">
                            <i class="fa fa-search"></i>
                        </button>
                        {{-- <div id="search_logo_icon">
                            <svg viewBox="0 0 512 512" class="search-icon">
                                <path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"
                                      class=""></path>
                            </svg>
                            <svg viewBox="0 0 40 40" class="close-x">
                                <path stroke="#aaa" fill="#ccc" stroke-width="5"
                                      d="M 5,5 L 35,35 M 35,5 L 5,35" />
                            </svg>
                        </div> --}}
                        {{-- <div id="rsSearchItem">
                        </div> --}}
                    </form>
                </div>

                <div id="hStore">
                    <a href="{{ url('/about') }}" class="txt-color">
                        <span class="text-info h3 pr-2"><i class="fa fa-info"></i></span>
                        <span class="header-font">
                            GIỚI THIỆU
                        </span>
                    </a>
                </div>
                <div id="hOrder">
                    <a href="mailto:{{$info->web_info_mail}}" class="txt-color">
                        <span class="text-info h3 pr-2"><i class="fa fa-envelope"></i></span>
                        <span class="header-font">
                            {{$info->web_info_mail}}
                        </span>
                    </a>
                </div>
                <div>
                    <span class="text-info h3 pr-2"><i class="fa fa-phone"></i></span>
                    <a href="tel:{{$info->web_info_phone}}" class="header-font">{{$info->web_info_phone}}</a>
                </div>

                <div id="hLogin">
                    <a href="{{ url('/dang-nhap') }}" class="header-font">
                        <span class="text-info h3 pr-2"><i class="fa fa-user"></i></span>
                        ĐĂNG NHẬP
                    </a>
                </div>
                <div id="hCart">
                    <a href="{{ url('/don-hang/danh-sach') }}" class="header-font">
                        <span class="text-info h3"><i class="fa fa-shopping-cart"></i></span>
                        <small class="p2 rounded-circle text-white badge bg-danger" id="cartCount">0</small>
                        <span>GIỎ HÀNG</span>
                    </a>
                    {{-- <span class="numSumProduct munber-card" id="numSumProduct">0</span>
                    <a href="#" class="txt-color">
                        <img class="header-icon" src="{{asset('/asset/clients/Images/user/icon_cart.png')}}" />
                        <span class="header-font">GIỎ HÀNG</span>
                    </a> --}}
                </div>
            </div>
        </div>
        <div class="nav-tab-menu">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 search-mobile">
                <form action="{{ url('/tim-kiem') }}" method="get" id="top_right_logo_search">
                    <input type="text" class="MBSearch" name="keysearch" autocomplete="off" placeholder="Bạn cần tìm gì?" />
                    <div id="search_logo_icon">
                        <svg viewBox="0 0 512 512" class="search-icon">
                            <path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"
                                  class=""></path>
                        </svg>
                        <svg viewBox="0 0 40 40" class="close-x">
                            <path stroke="#aaa" fill="#ccc" stroke-width="5"
                                  d="M 5,5 L 35,35 M 35,5 L 5,35" />
                        </svg>
                    </div>
                    <div id="rsSearchItem">
                    </div>
                </form>
            </div>
            <div id="right_logo_menu">
                <nav class="navbar navbar-expand-lg navbar-light" style="padding: 0;">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div id="img-logo-mobile">
                        <a href="{{ url('/trang-chu') }}"><img src="{{asset('/asset/clients/Images/logo-mobile.jpg?v=2')}}" /></a>
                    </div>

                    <div id="search_logo_icon-mobile">
                        <svg aria-hidden="true" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-3x">
                            <path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class=""></path>
                        </svg>
                    </div>

                    <div class="icon-mobile-login">
                        <a href="{{ url('/dang-nhap') }}"> <span class="text-info h3 pr-2"><i class="fa fa-user"></i></span>
                        </a>
                        <a href="{{ url('/don-hang/danh-sach') }}">
                            <span class="text-info h3"><i class="fa fa-shopping-cart"></i></span>
                            <small class="p2 rounded-circle text-white badge bg-danger" id="cartCountMB">0</small>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav w-100 justify-content-between">
                            @forelse ($menus as $item)

                            <li class="nav-item product-content news-content">
                                <a class="nav-link menu-category" href="{{ $item["menu_link"] }}">{{ $item["menu_name"] }}</a>
                                    <ul class="sublist-news">
                                        @forelse ($item["categories"] as $category)
                                        <li>
                                            <a class="nav-link" href="{{ url($item["menu_link"].'/'.$category->web_canonical) }}"><span>{{ $category->product_category_name }}</span></a>
                                        </li>
                                        @empty
                                        @endforelse
                                    </ul>
                                @isset($item["product_type_id"])
                                    <a class="nav-link menu-category-mobile" style="color: !important;" data-toggle="collapse" href="#t{{ $item["product_type_id"] }}" role="button" aria-expanded="false" aria-controls="40">
                                        {{ $item["menu_name"] }}
                                        <img src="{{asset('/asset/clients/Images/icon/bottom-arrow.png')}}" />
                                    </a>
                                    <div class="collapse" id="t{{ $item["product_type_id"] }}">
                                        <ul class="list-items-mobile">
                                            @forelse ($item["categories"] as $category)
                                            <li>
                                                <a class="nav-link" href="{{ url($item["menu_link"].'/'.$category->web_canonical) }}"><span>{{ $category->product_category_name }}</span></a>
                                            </li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    </div>
                                @else
                                    <li>
                                        <a class="nav-link menu-category-mobile" href="{{ $item["menu_link"] }}">{{ $item["menu_name"] }}</a>
                                    </li>
                                @endisset

                            </li>
                            @empty

                            @endforelse
                        </ul>
                    </div>

                </nav>

            </div>
        </div>

    </header>
