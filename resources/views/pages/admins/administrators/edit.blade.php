@extends('layouts.admin')


@section('title', 'Quản Trị Viên - Cập Nhật')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/administrator">Quản Trị Viên</a></li>
        <li class="breadcrumb-item" active>Cập Nhật</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Cập Nhật Quản Trị Viên <span class="text-primary">{{$Administrator->admin_official_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/administrator/update')}}" method="POST" enctype="multipart/form-data" name="update-administrator">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="admin_id" value="{{Request::get('admin_id')}}">
                            <input type="hidden" class="form-control" name="admin_avatar" value="{{$Administrator->admin_avatar}}">
                        </div>

                        <div class="fomr-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Quyền<span class="text-danger">(*)</span>: </label>
                                    <select class="form-control" name="role_group_code">
                                        @forelse ($lsRoles as $item)
                                            <option value="{{ $item->role_group_code }}" @if($Administrator->role_group_code==$item->role_group_code){{ 'selected' }} @endif>
                                                {{ $item->title }}</option>
                                        @empty
                                            <option value="">  </option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-sm-4">
                                    <label>Tài Khoản<span class="text-danger">(*)</span>: </label>
                                    <input type="text" class="form-control" maxlength="30" name="user_name" value="{{$Administrator->user_name}}" placeholder="Nhập vào tên tài khoản">
                                </div>
                                <div class="col-sm-4">
                                    <label>Mật Khẩu<span class="text-danger">(*)</span>: </label>
                                    <input type="password" class="form-control" maxlength="12" name="password" value="" placeholder="Nhập vào mật khẩu">
                                </div>
                                <div class="col-sm-4">
                                    <label>Xác Nhận Mật Khẩu<span class="text-danger">(*)</span>: </label>
                                    <input type="password" class="form-control" maxlength="12" name="password_confirmation" placeholder="Nhập vào mật khẩu xác nhận">
                                </div>
                            </div> --}}
                        </div>
                        <hr style="background-color: blue">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <label>Avatar<span class="text-danger">(*)</span>: </label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="customFile">
                                            <label class="custom-file-label" for="customFile">Chọn Avatar</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage" style="object-fit: cover;width: 100%"
                                                src="{{asset('storage/'.$Administrator->admin_avatar)}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Họ Tên<span class="text-danger">(*)</span>: </label>
                                            <input type="text" placeholder="Nhập vào họ tên đầy đủ" class="form-control" maxlength="100" name="admin_official_name" value="{{$Administrator->admin_official_name}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Họ Tên Bổ Sung: </label>
                                            <input type="text" placeholder="Nhập vào họ tên bổ sung" class="form-control" maxlength="100" name="admin_supplement_name" value="{{$Administrator->admin_supplement_name}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Tên Hiển Thị<span class="text-danger">(*)</span>: </label>
                                            <input type="text" placeholder="Nhập vào tên hiển thị" class="form-control" maxlength="100" name="admin_abbreviation" value="{{$Administrator->admin_abbreviation}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Tên Liên Lạc: </label>
                                            <input type="text" placeholder="Nhập vào tên liên lạc" class="form-control" maxlength="100" name="admin_contact_staff_name" value="{{$Administrator->admin_contact_staff_name}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Email<span class="text-danger">(*)</span>: </label>
                                            <input type="text" placeholder="Nhập vào email" class="form-control" maxlength="50" name="admin_email" value="{{$Administrator->admin_email}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Fax: </label>
                                            <input type="text" placeholder="Nhập vào fax" class="form-control" maxlength="20" name="admin_fax" value="{{$Administrator->admin_fax}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Điện Thoại 1<span class="text-danger">(*)</span>: </label>
                                            <input type="text" placeholder="Nhập vào số điện thoại 1" class="form-control" maxlength="20" name="admin_tel1" value="{{$Administrator->admin_tel1}}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Điện Thoại 2: </label>
                                            <input type="text" placeholder="Nhập vào số điện thoại 2" class="form-control" maxlength="20" name="admin_tel2" value="{{$Administrator->admin_tel2}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Tỉnh/Thành Phố thuộc TƯ<span class="text-danger">(*)</span>: </label>
                                            <select class="form-control" onchange="changeProvincial()" name="provincial">
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Quận/Huyện<span class="text-danger">(*)</span>: </label>
                                            <select class="form-control" onchange="changeDistrict()" name="district">
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Thị Trấn/Xã/Phường<span class="text-danger">(*)</span>: </label>
                                            <select class="form-control" onchange="changeCommune()" name="commune">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Mã Bưu Điện: </label>
                                            <input type="text" class="form-control" maxlength="10" name="admin_zip_code" value="{{$Administrator->admin_zip_code}}" placeholder="Nhập vào mã bưu điện">
                                        </div>
                                        <div class="col-sm-9">
                                            <label>Địa chỉ<span class="text-danger">(*)</span>: </label>
                                            <input type="text" class="form-control" maxlength="200" name="admin_address" value="{{$Administrator->admin_address}}" placeholder="Nhập vào địa chỉ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Sơ Lược Bản Thân:</label>
                            <textarea class="form-control" placeholder="Nhập vào sơ lược bản thân"  maxlength="200" name="special_notes" rows="4" cols="50">{{ $Administrator->special_notes }}</textarea>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" @if($Administrator->active_status == 1) checked @endif value="1" id="active_status" name="active_status">
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Cập Nhật"/>
                        <a href="/admins/administrator" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        $(function() {
            $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/provincial',
                success: function(data){
                   var selectElment = $("select[name='provincial']");
                   selectElment.find('option').remove().end();
                   $.each(data, function(i,item){
                       var op =$('<option>', {
                                value: item['provincial_code'],
                                text : item['provincial']
                            });
                            if(item['provincial_code']=='{{substr($Administrator->admin_address_code,0,2)}}')
                            {
                                op.prop("selected", "selected");
                            }
                       selectElment.append(op);
                    });
                    changeProvincial();
                }
            })
        });
        function changeProvincial() {
            var pCdoe = $("select[name='provincial']").children("option:selected").val();
            $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/provincial/'+pCdoe,
                success: function(data){
                   var selectElment = $("select[name='district']");
                   selectElment.find('option').remove().end();
                   $.each(data, function(i,item){
                       var op =$('<option>', {
                                value: item['district_code'],
                                text : item['district']
                            });
                            if(item['district_code']=='{{substr($Administrator->admin_address_code,2,3)}}')
                            {
                                op.prop("selected", "selected");
                            }
                       selectElment.append(op);
                    });
                    changeDistrict();
                }
            })
        }
        function changeDistrict() {
            var pCdoe = $("select[name='provincial']").children("option:selected").val();
            var dCdoe = $("select[name='district']").children("option:selected").val();
            $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/provincial/'+pCdoe+'/'+dCdoe,
                success: function(data){
                   var selectElment = $("select[name='commune']");
                   selectElment.find('option').remove().end();
                   $.each(data, function(i,item){
                       var op =$('<option>', {
                                value: item['commune_code'],
                                text : item['commune']
                            });
                            if(item['commune_code']=='{{substr($Administrator->admin_address_code,5,5)}}')
                            {
                                op.prop("selected", "selected");
                            }
                       selectElment.append(op);
                    });
                }
            })
        }

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#loadImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input[name='image']").change(function() {readURL(this);});
    </script>
@endsection
