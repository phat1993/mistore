@extends('layouts.admin')

@section('title', 'Quản Trị Viên - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/administrator">Quản Trị Viên</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Quản Trị Viên <span class="text-primary">{{$admin->admin_official_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->ad_admin_avatar) }}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $admin->update_mode_content }}</h4>
                    <span class="color-red">{{$admin->ad_admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($admin->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <i class="{{ $admin->update_mode_icon }}"></i>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Trạng Thái</h4>
                    <span class="color-red" style="text-transform:uppercase">
                        {{ $admin->update_mode_content }}
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($admin->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($admin->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-reload"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Cập Nhật</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($admin->updated_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($admin->updated_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col">CODE</th>
                            <td width="85%">{{$admin->admin_code}}</td>
                        </tr>
                        <tr>
                            <th scope="col">HÌNH ẢNH</th>
                            <td>
                                <div style="width:100px;height:100px">
                                    <img style="object-fit: cover;width:100%;" src="{{asset('storage/'.$admin->admin_avatar)}}" alt="{{$admin->admin_avatar}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="col">QUYỀN</th>
                            <td>{{$admin->role_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">HỌ TÊN</th>
                            <td>{{$admin->admin_official_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TÊN BỔ SUNG</th>
                            <td>{{$admin->admin_supplement_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TÊN HIỂN THỊ</th>
                            <td>{{$admin->admin_abbreviation}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TÊN LIÊN LẠC</th>
                            <td>{{$admin->admin_contact_staff_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">MAIL</th>
                            <td>{{$admin->admin_email}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TG ĐĂNG NHẬP</th>
                            <td>
                                @if ($admin->last_login_time == null)
                                    <span class="color-red">Chưa Đăng Nhập</span>
                                @else
                                    <span class="color-red">{{date('H:i:s', strtotime($admin->last_login_time))}}</span>
                                    <span class="color-red">{{date('d-m-Y', strtotime($admin->last_login_time))}}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="col">MAIL</th>
                            <td>{{$admin->admin_email}}</td>
                        </tr>
                        <tr>
                            <th scope="col">FAX</th>
                            <td>{{$admin->admin_email}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐIỆN THOẠI 1</th>
                            <td>{{$admin->admin_tel1}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐIỆN THOẠI 2</th>
                            <td>{{$admin->admin_tel2}}</td>
                        </tr>
                        <tr>
                            <th scope="col">MÃ BƯU ĐIỆN</th>
                            <td>{{$admin->admin_zip_code}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐỊA CHỈ</th>
                            <td>{{$admin->address_text}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SƠ LƯỢC BẢN THÂN</th>
                            <td>{{$admin->special_notes}}</td>
                        </tr>
                        @if ($admin->email_verified_at==null)
                        <tr>
                            <th scope="col">KÍCH HOẠT MAIL</th>
                            <td>Chưa</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <a href="/admins/administrator" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

    </div>
    <!-- /Main Content -->
@stop


