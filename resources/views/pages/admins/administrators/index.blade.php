@extends('layouts.admin')
@section('title', 'Quản Trị Viên')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Quản Trị Viên</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Quản Trị Viên </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/administrator')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="role_name" value="{{Request::get('role_name')}}" placeholder="Tìm kiếm theo quyền">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="admin_abbreviation" value="{{Request::get('admin_abbreviation')}}" placeholder="Tìm kiếm theo Tên hiển thị">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="admin_official_name" value="{{Request::get('admin_official_name')}}" placeholder="Tìm kiếm theo Tên đầy đủ">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="admin_email" value="{{Request::get('admin_email')}}" placeholder="Tìm kiếm theo Email">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Hoạt Động</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th><a href="/admins/administrator/create" class="btn btn-outline-primary"><i class="ti-plus"></i></a></th>
                            <th>AVATAR</th>
                            <th>QUYỀN</th>
                            <th>TÊN HIỂN THỊ</th>
                            <th>HỌ TÊN</th>
                            <th>MAIL</th>
                            <th>TG ĐĂNG NHẬP</th>
                            <th>HOẠT ĐỘNG</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($Administrators as $admin)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>
                                        <img src="{{ asset('storage/'.$admin->admin_avatar) }}" width="50" height="50" alt="">
                                    </td>
                                    <td>{{$admin->role_name}}</td>
                                    {{-- <td>{{$admin->role_group_code=='001'?'Super Admin':$admin->roleName}}</td> --}}
                                    <td>{{$admin->admin_abbreviation}}</td>
                                    <td>{{$admin->admin_official_name}}</td>
                                    <td>{{$admin->admin_email}}</td>
                                    <td>
                                        @if ($admin->last_login_time == null)
                                            <span class="color-red">Chưa Đăng Nhập</span>
                                        @else
                                            <span class="color-red">{{date('H:i:s', strtotime($admin->last_login_time))}}</span>
                                            <span class="color-red">{{date('d-m-Y', strtotime($admin->last_login_time))}}</span>
                                        @endif
                                    </td>
                                    <td>@if($admin->active_status == 1) BẬT @else TẮT @endif </td>
                                    <td>
                                        <a class="btn btn-info" href="javascript:void(0)" onclick="form_action('view', {{$admin->admin_id}});" role="button">
                                            <i class="ti-info"></i>
                                        </a>
                                        <a class="btn btn-warning" href="javascript:void(0)" onclick="form_action('edit', {{$admin->admin_id}});" role="button">
                                            <i class="ti-pencil"></i>
                                        </a>
                                        <a class="btn btn-danger" href="javascript:void(0)" onclick="form_action('delete', {{$admin->admin_id}});" role="button">
                                            <i class="ti-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $Administrators->appends([
                    'admin_official_name' => Request::get('admin_official_name'),
                    'active_status' => Request::get('active_status'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="admin_id" value="">
            </div>
        </form>

    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, admin_id) {
           let frm = document.form1;

           if(mode == 'delete'){
               frm.action = "{{url('/admins/administrator/destroy')}}";
               frm.method = 'post';
           }else if(mode == 'edit'){
               frm.action = "{{url('/admins/administrator/edit')}}";
               frm.method = 'get';
           }else if(mode == 'view'){
               frm.action = "{{url('/admins/administrator/show')}}";
               frm.method = 'get';
           }

            frm.admin_id.value = admin_id
            frm.submit();
        }
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        initialSetup();


    </script>
@stop


