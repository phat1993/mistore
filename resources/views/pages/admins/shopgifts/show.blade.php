@extends('layouts.admin')

@section('title', 'Mã Khuyến Mãi - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/shopgift">Mã Khuyến Mãi</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Mã Khuyến Mãi  <span class="text-primary">{{$shopgift->gift_code}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $shopgift->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($shopgift->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($shopgift->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($shopgift->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($shopgift->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($shopgift->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($shopgift->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($shopgift->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($shopgift->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông Tin Chi Tiết</h3>
                <div class="table-div">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col" width="15%">MÃ KHUYẾN MÃI</th>
                            <td>{{$shopgift->gift_code}}</td>
                        </tr>
                        <tr>
                            <th>GIÁ TRỊ</th>
                            <td>
                                <span class="text-danger">
                                    @if ($shopgift->gift_pattern == 1)
                                    {{ number_format($shopgift->gift_discount,0,'',',') }}<sup>vnđ</sup>
                                    @else
                                        {{ $shopgift->gift_discount }}%
                                    @endif
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th>BẮT ĐẦU</th>
                            <td>{{date('d-m-Y', strtotime($shopgift->gift_start_time))}}</td>
                        </tr>
                        <tr>
                            <th>KẾT THÚC</th>
                            <td>{{date('d-m-Y', strtotime($shopgift->gift_end_time))}}</td>
                        </tr>
                        <tr>
                            <th>LẦN SỬ DỤNG</th>
                            <td>{{$shopgift->gift_use_count}} Lần</td>
                        </tr>
                        <tr>
                            <th>ĐÃ SỬ DỤNG</th>
                            <td>{{$shopgift->gift_used}} Lần</td>
                        </tr>
                        <tr>
                            <th>GHI CHÚ</th>
                            <td>{{$shopgift->gift_note}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Danh sách Hóa Đơn Sử Dụng</h3>
                <div class="table-div">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>MÃ HÓA ĐƠN</th>
                                <th>THỜI GIAN</th>
                                <th>TRẠNG THÁI</th>
                                <th>KHÁCH HÀNG</th>
                                <th>SĐT</th>
                                <th>MÃ/SĐT KHUYẾN MÃI</th>
                                <th>THÀNH TIỀN(VNĐ)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $idx = 0; ?>
                            @forelse ($orders as $or)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>
                                        <a class="text-info" href="javascript:void(0)" onclick="form_action('view', {{$or->order_id}});" role="button">
                                        {{Str::padRight('MHD',9-strlen($or->order_id),0).$or->order_id}}
                                        </a>
                                    </td>
                                    <td>{{ date('d-m-Y', strtotime($or->created_at)) }}</td>
                                    <td>@if ($or->order_state_id ==4)
                                        <span class="text-success">Đã sử dụng</span>
                                        @elseif($or->order_state_id > 4)
                                        <span class="text-danger">Hủy bỏ sử dụng</span>
                                        @else
                                        <span class="text-primary">Đang sử dụng</span>
                                    @endif</td>
                                    <td>{{$or->order_full_name}}</td>
                                    <td>{{$or->order_phone}}</td>
                                    <td>{{$or->order_gift}}</td>
                                    <td>{{number_format($or->order_amount,0,'',',') }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <br>
                <a href="/admins/shopgift" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

        <form name="form1" action="" method="" target="_blank">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="order_id" value="">
            </div>
        </form>
    </div>
    <!-- /Main Content -->
@stop


@section('javascript')
    <script>
        function form_action (mode, order_id) {
           let frm = document.form1;
                if(mode == 'view'){
               frm.action = "{{url('/admins/order/show')}}";
               frm.method = 'get';
           }

            frm.order_id.value = order_id
            frm.submit();
        }

    </script>
@stop
