@extends('layouts.admin')


@section('title', 'Mã Khuyến Mãi - Cập Nhật')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/shopgift">Mã Khuyến Mãi</a></li>
        <li class="breadcrumb-item" active>Cập Nhật</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Cập Nhật Mã Khuyến Mãi</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/shopgift/update')}}" method="POST" name="create-shopgift">
                        {{csrf_field()}}

                        <div class="form-group">
                            <input type="hidden" class="form-control" name="gift_id" value="{{Request::get('gift_id')}}">
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-5">
                                <label>Mã/SĐT Khuyến Mãi<span class="text-danger">(*)</span>: </label>
                                <div class="d-flex flex-row">
                                    <input type="text" placeholder="Nhập vào mã khuyến mãi" maxlength="100" class="form-control" name="gift_code" aria-describedby="btnRandom" value="{{ $shopgift->gift_code }}">
                                    <a  href="javascript:void(0)" onclick="giftRandom();" role="button" class="btn btn-outline-dark pt-2"><i class="fa fa-random"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-2 form-group">
                                <label>Hình Thức Khuyến Mãi<span class="text-danger">(*)</span>: </label>
                                <select class="form-control" onchange="changePattern()" name='gift_pattern'>
                                    <option value="1" {{$shopgift->gift_pattern == 1 ? 'selected': ''}}>Giá Trị Tiền</option>
                                    <option value="0" {{$shopgift->gift_pattern == 0 ? 'selected': ''}}>Giá Trị Phần Trăm</option>
                                </select>
                                {{-- <input type="text" placeholder="Nhập vào số điện thoại" maxlength="20" class="form-control" name="gift_phone" value="{{ $shopgift->gift_phone }}"> --}}
                            </div>
                            <div class="col-sm-2 form-group d-none" id="percent-discount">
                                <label>Phần Trăm Khuyến Mãi<span class="text-danger">(*)</span>: </label>
                                <div class="input-group">
                                    <span class="input-group-text bg-secondary text-weight-bold text-white" id="percent-addon2">%</span>
                                    <input type="number" placeholder="Nhập vào số phần trăm" class="form-control text-right"
                                    name="gift_discount_percent" value="{{$shopgift->gift_discount}}" aria-label="Giá"
                                        aria-describedby="price-addon2">
                                </div>
                            </div>
                            <div class="col-sm-3 form-group" id="money-discount">
                                <label>Tiền Khuyến Mãi<span class="text-danger">(*)</span>: </label>
                                <div class="input-group">
                                    <span class="input-group-text bg-secondary text-weight-bold text-white" id="price-addon2">VNĐ</span>
                                    <input type="text" placeholder="Nhập vào số tiền khuyến mãi" class="form-control text-right" data-type='currency'
                                    name="gift_discount_money" value="{{$shopgift->gift_discount}}"  aria-label="Giá"
                                        aria-describedby="price-addon2">
                                </div>
                            </div>
                            <div class="col-sm-2 form-group">
                                <label>Số Lần Sử Dụng<span class="text-danger">(*)</span>: </label>
                                <input type="number" placeholder="Nhập vào số lần sử dụng" class="form-control text-right" name="gift_use_count" value="{{ $shopgift->gift_use_count }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Thời Gian Bắt Đầu<span class="text-danger">(*)</span>: </label>
                                <input type="date" placeholder="Nhập vào thời gian bắt đầu" class="form-control" name="gift_start_time" value="{{ date('Y-m-d', strtotime($shopgift->gift_start_time)) }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Thời Gian Kết Thúc<span class="text-danger">(*)</span>: </label>
                                <input type="date" placeholder="Nhập vào thời gian kết thúc" class="form-control" name="gift_end_time" value="{{ date('Y-m-d', strtotime($shopgift->gift_end_time)) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ghi Chú:</label>
                            <textarea class="form-control" maxlength="500" placeholder="Nhập vào ghi chú" name="gift_note" rows="4" cols="50">{{ $shopgift->gift_note }}</textarea>
                        </div>


                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status" name="active_status" {{ $shopgift->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Cập Nhật"/>
                        <a href="/admins/shopgift" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>
    function giftRandom() {
        $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/shopgift/coderandom',
                success: function(data){
                    $('input[name="gift_code"]').val(data);
                }
        })
    }

    function changePattern() {
        var proType = $("select[name='gift_pattern']").children("option:selected").val();
        if(proType == 1){
            $('#percent-discount').addClass('d-none');
            $('#money-discount').removeClass('d-none');
            $('input[name="gift_discount_percent"]').val('0');
            $('input[name="gift_discount_money"]').val('0');
        }
        else{
            $('#percent-discount').removeClass('d-none');
            $('#money-discount').addClass('d-none');
            $('input[name="gift_discount_percent"]').val('0');
            $('input[name="gift_discount_money"]').val('');
        }

    }
    $( document ).ready( function () {
        var proType = $("select[name='gift_pattern']").children("option:selected").val();
        if(proType == 1){
            $('#percent-discount').addClass('d-none');
            $('#money-discount').removeClass('d-none');
        }
        else{
            $('#percent-discount').removeClass('d-none');
            $('#money-discount').addClass('d-none');
        }

        formatCurrency($("input[name='gift_discount_money']"));
        jQuery.validator.addMethod("greaterThan",
        function(value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }

            return isNaN(value) && isNaN($(params).val())
                || (Number(value) > Number($(params).val()));
        },'Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        $( "form[name=create-shopgift]" ).validate( {
            rules: {
                gift_code: {
                    required: true,
                    maxlength: 100
                },
                gift_discount_percent: {
                    required: true,
                    min: 1,
                    max: 100
                },
                gift_discount_money: "required",
                gift_start_time: {
                    required: true,
                    date: true
                },
                gift_end_time: {
                    required: true,
                    date: true,
                    greaterThan: "input[name='gift_start_time']"
                },
                gift_use_count: {
                    required: true,
                    min: 1,
                    max: 1000
                },
                gift_note: {
                    maxlength: 500
                },
            },
            messages: {
                gift_code: {
                    required: "Hãy nhập vào mã khuyến mãi.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                gift_discount_percent: {
                    required: "Hãy nhập vào giá trị phần trăm",
                    max: "Giá trị tối đa là 100",
                    min: "Giá trị tối thiểu là 1"
                },
                gift_discount_money: "Hãy nhập vào giá trị tiền",
                gift_start_time: {
                    required: "Hãy nhập vào thời gian bắt đầu.",
                    date: "Bạn hãy nhập đúng thời gian."
                },
                gift_end_time: {
                    required: "Hãy nhập vào thời gian kết thúc.",
                    date: "Bạn hãy nhập đúng thời gian."
                },
                gift_use_count: {
                    required: "Hãy nhập vào mã khuyến mãi.",
                    max: "Giá trị tối đa là 1000",
                    min: "Giá trị tối thiểu là 1"
                },
                gift_note: {
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });

        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        var original_len = input_val.length;

        // initial caret position
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // // get position of first decimal
            // // this prevents multiple decimals from
            // // being entered
            // var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            // var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // // validate right side
            // right_side = formatNumber(right_side);

            // // On blur make sure 2 numbers after decimal
            // if (blur === "blur") {
            // right_side += "00";
            // }

            // // Limit decimal to only 2 digits
            // right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // // final formatting
            // if (blur === "blur") {
            // input_val += ".00";
            // }
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        }
    } );
</script>
@endsection
