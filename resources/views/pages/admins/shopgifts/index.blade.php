@extends('layouts.admin')
@section('title', 'Mã Khuyến Mãi')

@section('styles')
    <style>
        input[type=checkbox] {
            transform: scale(1.8);
        }
    </style>
@endsection
@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Mã Khuyến Mãi</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Mã Khuyến Mãi </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/shopgift')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row align-items-center">
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="gift_code" value="{{Request::get('gift_code')}}" placeholder="Mã khuyến mãi">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="gift_discount" value="{{Request::get('gift_discount')}}" placeholder="Giá Trị khuyến mãi">
                            </div>
                            <div class="col-sm-2">
                                <input type="date" title="Khoảng Thời Gian" class="form-control" name="gift_date" value="{{Request::get('gift_date')}}" placeholder="Khoảng Thời Gian">
                            </div>
                            <div class="col-sm-1">
                                <input type="number" class="form-control" name="gift_use_count" value="{{Request::get('gift_use_count')}}" placeholder="Số lần sử dụng">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name='active_status'>
                                    <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Hoạt Động</option>
                                    <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                    <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-inline"><input type="checkbox" class="form-control" name="view_all" value="1" {{ Request::get('view_all')==1?'checked':'' }}>
                                    <label for="view_all" class="ml-3" id="ckTitle">Hiển Thị Tất Cả</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th><a href="/admins/shopgift/create" class="btn btn-outline-primary"><i class="ti-plus"></i></a></th>
                            <th>MÃ KHUYẾN MÃI</th>
                            <th>GIÁ TRỊ</th>
                            <th>THỜI GIAN</th>
                            <th>LẦN SỬ DỤNG</th>
                            <th>HOẠT ĐỘNG</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($ShopGifts as $shopgift)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>{{$shopgift->gift_code}}</td>
                                    <td>
                                        <span class="text-danger">
                                            @if ($shopgift->gift_pattern == 1)
                                            {{ number_format($shopgift->gift_discount,0,'',',') }}<sup>vnđ</sup>
                                            @else
                                                {{ $shopgift->gift_discount }}%
                                            @endif
                                        </span>
                                    </td>
                                    <td>
                                        {{date('d-m-Y', strtotime($shopgift->gift_start_time))}}
                                        <small class="text-danger">đến</small>
                                        {{date('d-m-Y', strtotime($shopgift->gift_end_time))}}</td>
                                    <td>{{$shopgift->gift_use_count}} Lần</td>
                                    <td>@if($shopgift->active_status == 1) BẬT @else TẮT @endif </td>
                                    <td>
                                        @if ($shopgift->deleted_at!=null)
                                            <a class="btn btn-outline-info" href="javascript:void(0)" onclick="form_action('view', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-outline-success" href="javascript:void(0)" onclick="form_action('recovered', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-reload"></i>
                                            </a>
                                            <a class="btn btn-outline-danger" href="javascript:void(0)" onclick="form_action('delete', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-close"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="form_action('view', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-warning" href="javascript:void(0)" onclick="form_action('edit', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="form_action('destroy', {{$shopgift->gift_id}});" role="button">
                                                <i class="ti-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $ShopGifts->appends([
                    'gift_code' => Request::get('gift_code'),
                    'gift_discount' => Request::get('gift_discount'),
                    'gift_date' => Request::get('gift_date'),
                    'gift_use_count' => Request::get('gift_use_count'),
                    'active_status' => Request::get('active_status'),
                    'view_all' => Request::get('view_all'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="gift_id" value="">
            </div>
        </form>

    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, gift_id) {
           let frm = document.form1;

           if(mode == 'destroy'){
               var cf = confirm('Bạn có chắc chắn muốn xóa đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/shopgift/destroy')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'delete'){
               var cf = confirm('Bạn có chắc chắn muốn xóa vĩnh viễn đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/shopgift/delete')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'recovered'){
               frm.action = "{{url('/admins/shopgift/recovered')}}";
               frm.method = 'get';
           }else if(mode == 'edit'){
               frm.action = "{{url('/admins/shopgift/edit')}}";
               frm.method = 'get';
           }else if(mode == 'view'){
               frm.action = "{{url('/admins/shopgift/show')}}";
               frm.method = 'get';
           }

            frm.gift_id.value = gift_id
            frm.submit();
        }
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        $("#ckTitle").click(function () {
            var $checkbox = $('input[name=view_all]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
        });


        initialSetup();


    </script>
@stop


