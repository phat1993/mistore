@extends('layouts.admin')

@section('title', 'Khách Hàng - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/customer">Khách Hàng</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Khách Hàng <span class="text-primary">{{$customer->customer_full_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $customer->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($customer->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($customer->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($customer->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-reload"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Cập Nhật</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($customer->updated_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($customer->updated_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="lorvens-widget">
                    <div class="bd-example bd-example-tabs">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Yêu Thích</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Đơn Hàng</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table class="table table-hover">
                                    {{-- <tr>
                                        <th scope="col">CODE</th>
                                        <td width="85%">{{$customer->customer_code}}</td>
                                    </tr> --}}
                                    <tr>
                                        <th width="15%" scope="col">HÌNH ẢNH</th>
                                        <td colspan="2">
                                            <div style="width:100px;height:100px">
                                                <img style="object-fit: cover;width:100%;" src="{{asset('storage/'.$customer->customer_avatar)}}" alt="{{$customer->customer_avatar}}">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col">HỌ TÊN</th>
                                        <td colspan="2">{{$customer->customer_full_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">GIỚI TÍNH</th>
                                        <td colspan="2">{{$customer->customer_gender}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">NGÀY SINH</th>
                                        <td colspan="2">
                                            @if ($customer->customer_brithday == null)
                                                <span></span>
                                            @else
                                                <span>{{date('d-m-Y', strtotime($customer->customer_brithday))}}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col">SỐ ĐIỆN THOẠI</th>
                                        <td>{{$customer->customer_phone}}</td>
                                        <td width="5%">
                                            @if ($customer->phone_verified_at==null)
                                                <span class="badge badge-danger text-light">chưa kích hoạt</span>
                                            @else
                                                <span class="badge badge-success text-light">đã kích hoạt</span>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="col">MAIL</th>
                                        <td>{{$customer->customer_email}}</td>
                                        <td width="5%">
                                            @if ($customer->email_verified_at==null)
                                                <span class="badge badge-danger text-light">chưa kích hoạt</span>
                                            @else
                                                <span class="badge badge-success text-light">đã kích hoạt</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col">TG ĐĂNG NHẬP</th>
                                        <td colspan="2">
                                            @if ($customer->last_login_time == null)
                                                <span>Chưa Đăng Nhập</span>
                                            @else
                                                <span>{{date('H:i:s', strtotime($customer->last_login_time))}}</span>
                                                <span>{{date('d-m-Y', strtotime($customer->last_login_time))}}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col">ĐỊA CHỈ</th>
                                        <td colspan="2">{{$customer->customer_address}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>AVATAR</th>
                                        <th>TÊN SẢN PHẨM</th>
                                        <th>THỜI GIAN</th>
                                        <th>YÊU THÍCH</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $ids = 0; ?>
                                        @forelse ($Favorites as $favorite)
                                            <tr>
                                                <?php $ids++; ?>
                                                <td>{{$ids}}</td>
                                                <td>
                                                    <img src="{{ asset('storage/'.$favorite->product_img_1) }}" width="50" height="50" alt="">
                                                </td>
                                                <td>{{$favorite->product_name}}</td>
                                                <td>
                                                    <span>{{date('H:i:s', strtotime($favorite->updated_at))}}</span>
                                                    <span>{{date('d-m-Y', strtotime($favorite->updated_at))}}</span>
                                                </td>
                                                <td class="pl-5 text-danger"><h3>@if($favorite->active_status == 1) <i class="ti-heart"></i> @else <i class="ti-heart-broken" title="Đã từng thích"></i> @endif </h3></td>

                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                            </tr>
                                        @endforelse
                                    </tbody>

                                </table>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <table class="table table-hover table-data" id="banner-table">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>MÃ ĐƠN HÀNG</th>
                                        <th>TRẠNG THÁI</th>
                                        <th>THỜI GIAN ĐẶT</th>
                                        <th>KHÁCH HÀNG</th>
                                        <th>ĐIỆN THOẠI</th>
                                        <th>TỔNG TIỀN(VNĐ)</th>
                                        <th>XỬ LÝ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $ido = 0; ?>
                                        {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                                        @forelse ($ShopOrders as $shopOrder)
                                            <tr>
                                                <?php $ido++; ?>
                                                <td>{{$ido}}</td>
                                                <td>{{Str::padRight('MHD',9-strlen($shopOrder->order_id),0).$shopOrder->order_id}}</td>
                                                <td>
                                                    <span class="@switch($shopOrder->order_state_id)
                                                        @case(1)
                                                            text-warning
                                                            @break
                                                        @case(2) @case(3)
                                                            text-info
                                                            @break
                                                        @case(4)
                                                            text-success
                                                            @break
                                                        @case(5) @case(6) @case(7)
                                                            text-danger
                                                            @break
                                                        @default
                                                    @endswitch" >
                                                        {{$shopOrder->order_state_value}}
                                                    </span>
                                                </td>
                                                <td>
                                                    <span>{{date('H:i:s', strtotime($shopOrder->updated_at))}}</span>
                                                    <span>{{date('d-m-Y', strtotime($shopOrder->updated_at))}}</span>
                                                </td>
                                                <td>{{$shopOrder->order_full_name}}</td>
                                                <td>{{$shopOrder->order_phone}}</td>
                                                <td>{{ number_format($shopOrder->order_total,0,'',',') }}</td>
                                                <td>
                                                    <a class="btn btn-outline-info" href="javascript:void(0)" onclick="form_action('viewOrder', {{$shopOrder->order_id}});" role="button">
                                                        <i class="ti-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                            </tr>
                                        @endforelse
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow text-center pb-5">
                <a href="/admins/order" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>
        <form name="form" action="" method="" target="_blank">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="order_id" value="">
            </div>
        </form>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, order_id) {
           let frm = document.form;
            if(mode == 'viewOrder'){
               frm.action = "{{url('/admins/order/show')}}";
               frm.method = 'get';
           }

            frm.order_id.value = order_id
            frm.submit();
        }

    </script>
@stop
