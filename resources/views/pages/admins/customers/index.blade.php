@extends('layouts.admin')
@section('title', 'Khách Hàng')

@section('styles')
    <style>
        input[type=checkbox] {
            transform: scale(1.8);
        }
    </style>
@endsection
@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Khách Hàng</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Khách Hàng </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/customer')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group row align-items-center">
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="customer_full_name" value="{{Request::get('customer_full_name')}}" placeholder="Tìm kiếm theo Họ Tên">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="customer_phone" value="{{Request::get('customer_phone')}}" placeholder="Tìm kiếm theo Số Điện Thoại">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="customer_email" value="{{Request::get('customer_email')}}" placeholder="Tìm kiếm theo Email">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Hoạt Động</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        {{-- <div class="col-sm-2">
                            <div class="form-inline"><input type="checkbox" class="form-control" name="view_all" value="1" {{ Request::get('view_all')==1?'checked':'' }}>
                                <label for="view_all" class="ml-3" id="ckTitle">Hiển Thị Tất Cả</label>
                            </div>
                        </div> --}}
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>AVATAR</th>
                            <th>HỌ TÊN</th>
                            <th>PHONE</th>
                            <th>MAIL</th>
                            <th>TG ĐĂNG NHẬP</th>
                            <th>HOẠT ĐỘNG</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($Customers as $customer)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>
                                        <img src="{{ asset('storage/'.$customer->customer_avatar) }}" width="50" height="50" alt="">
                                    </td>
                                    <td>{{$customer->customer_full_name}}</td>
                                    <td><span class="{{ $customer->email_verified_at == null?'text-danger':'text-success'}}">{{$customer->customer_phone}}</span></td>
                                    <td><span class="{{ $customer->email_verified_at == null?'text-danger':'text-success'}}">{{$customer->customer_email}}</span></td>
                                    <td>
                                        @if ($customer->last_login_time == null)
                                            <span>Chưa Đăng Nhập</span>
                                        @else
                                            <span>{{date('H:i:s', strtotime($customer->last_login_time))}}</span>
                                            <span>{{date('d-m-Y', strtotime($customer->last_login_time))}}</span>
                                        @endif
                                    </td>
                                    <td>@if($customer->active_status == 1) BẬT @else TẮT @endif </td>
                                    <td>
                                        <a class="btn btn-info" title="Thông tin chi tiết" href="javascript:void(0)" onclick="form_action('view', {{$customer->customer_id}});" role="button">
                                            <i class="ti-info"></i>
                                        </a>
                                        {{-- <a class="btn btn-danger" title="Danh sách yêu thích" href="javascript:void(0)" onclick="form_action('favorite', {{$customer->customer_id}});" role="button">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a class="btn btn-success" title="Danh sách Đơn Hàng" href="javascript:void(0)" onclick="form_action('order', {{$customer->customer_id}});" role="button">
                                            <i class="ti-shopping-cart-full"></i>
                                        </a> --}}
                                        {{-- <a class="btn btn-warning" title="Khóa hoạt động" href="javascript:void(0)" onclick="form_action('active', {{$customer->customer_id}});" role="button">
                                            <i class="ti-lock"></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $Customers->appends([
                    'customer_full_name' => Request::get('customer_full_name'),
                    'customer_phone' => Request::get('customer_phone'),
                    'customer_email' => Request::get('customer_email'),
                    'active_status' => Request::get('active_status'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="customer_id" value="">
            </div>
        </form>

    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, customer_id) {
           let frm = document.form1;
           if(mode == 'view'){
               frm.action = "{{url('/admins/customer/show')}}";
               frm.method = 'get';
           }else if(mode == 'favorite'){
               frm.action = "{{url('/admins/customer/favorite')}}";
               frm.method = 'get';
           }else if(mode == 'order'){
               frm.action = "{{url('/admins/customer/order')}}";
               frm.method = 'get';
           }else if(mode == 'active'){
               frm.action = "{{url('/admins/customer/active')}}";
               frm.method = 'post';
           }

            frm.customer_id.value = customer_id
            frm.submit();
        }
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        initialSetup();


    </script>
@stop


