@extends('layouts.admin')

@section('title', 'Thông Tin WebSite - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/webinfo">Thông Tin WebSite</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Thông Tin WebSite <span class="text-primary">{{$webinfo->web_info_title}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $webinfo->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($webinfo->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($webinfo->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($webinfo->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($webinfo->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($webinfo->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($webinfo->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($webinfo->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($webinfo->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col">TIÊU ĐỀ</th>
                            <td>{{$webinfo->web_info_title}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐIỆN THOẠI</th>
                            <td>{{$webinfo->web_info_phone}}</td>
                        </tr>
                        <tr>
                            <th scope="col">EMAIL</th>
                            <td>{{$webinfo->web_info_mail}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ZALO</th>
                            <td><a class="text-primary" href="{{$webinfo->web_info_zalo}}">{{$webinfo->web_info_zalo}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">FACEBOOK</th>
                            <td><a class="text-primary" href="{{$webinfo->web_info_facebook}}">{{$webinfo->web_info_facebook}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">YOUTUBE</th>
                            <td><a class="text-primary" href="{{$webinfo->web_info_youtube}}">{{$webinfo->web_info_youtube}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">INSTAGRAM</th>
                            <td><a class="text-primary" href="{{$webinfo->web_info_instagram}}">{{$webinfo->web_info_instagram}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">SHOPEE</th>
                            <td><a class="text-primary" href="{{$webinfo->web_info_shopee}}">{{$webinfo->web_info_shopee}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">ĐỊA CHỈ</th>
                            <td>{{$webinfo->web_info_address}}</td>
                        </tr>
                        <tr>
                            <th scope="col">MÔ TẢ </th>
                            <td><a class="btn btn-danger" href="{{ url('/admins/webinfo/demo/'.$webinfo->web_info_id) }}" target="_blank">Demo</a>
                                <hr>
                                <div class="border border-dark p-3">{!!$webinfo->web_info_detail!!}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <a href="/admins/webinfo" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

    </div>
    <!-- /Main Content -->
@stop


