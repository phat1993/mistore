@extends('layouts.admin')


@section('title', 'Thông Tin WebSite - Cập Nhật')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/webinfo">Thông Tin WebSite</a></li>
        <li class="breadcrumb-item" active>Cập Nhật</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Cập Nhật Thông Tin WebSite</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/webinfo/update')}}" method="POST" enctype="multipart/form-data" name="create-webinfo">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="web_info_id" value="{{Request::get('web_info_id')}}">
                        </div>
                        <div class="form-group">
                            <label>Tiêu Đề<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào tiêu đề" maxlength="100" class="form-control" name="web_info_title" value="{{ $webinfo->web_info_title }}">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Điện Thoại<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào số điện thoại" maxlength="50" class="form-control" name="web_info_phone" value="{{ $webinfo->web_info_phone }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Email<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào email" maxlength="50" class="form-control" name="web_info_mail" value="{{ $webinfo->web_info_mail }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Đường Dẫn Facebook<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào đường dẫn đến facebook"  maxlength="500" class="form-control" name="web_info_facebook" value="{{ $webinfo->web_info_facebook }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Đường Dẫn Zalo<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào đường dẫn đến Zalo"  maxlength="500" class="form-control" name="web_info_zalo" value="{{ $webinfo->web_info_zalo }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Đường Dẫn Youtube<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào đường dẫn đến youtube"  maxlength="500" class="form-control" name="web_info_youtube" value="{{ $webinfo->web_info_youtube }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Đường Dẫn Instagram<span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào đường dẫn đến instagram"  maxlength="500" class="form-control" name="web_info_instagram" value="{{ $webinfo->web_info_instagram }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Đường Dẫn Shopee<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào đường dẫn đến Shoppe"  maxlength="500" class="form-control" name="web_info_shopee" value="{{ $webinfo->web_info_shopee }}">
                        </div>
                        <div class="form-group">
                            <label>Địa Chỉ<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào địa chỉ"  maxlength="200" class="form-control" name="web_info_address" value="{{ $webinfo->web_info_address }}">
                        </div>

                        <div class="form-group">
                            <label>Nội Dung<span class="text-danger">(*)</span>:</label>
                            <textarea class="form-control" placeholder="Nhập vào chi tiết" name="web_info_detail" rows="4" cols="50">{{ $webinfo->web_info_detail }}</textarea>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status"  name="active_status"{{ $webinfo->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Cập Nhật"/>
                        <a href="/admins/webinfo" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
<script src={{ url('/asset/plugins/ckeditor/ckeditor.js') }}></script>
<script>
    CKEDITOR.replace( 'web_info_detail' );
</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

$( document ).ready( function () {
        $( "form[name=create-webinfo]" ).validate( {
            rules: {
                web_info_title: {
                    required: true,
                    maxlength: 100
                },
                web_info_phone: {
                    required: true,
                    maxlength: 50
                },
                web_info_mail: {
                    required: true,
                    email: true,
                    maxlength: 50
                },
                web_info_zalo: {
                    required: true,
                    maxlength: 500
                },
                web_info_facebook: {
                    required: true,
                    maxlength: 500
                },
                web_info_youtube: {
                    required: true,
                    maxlength: 500
                },
                web_info_instagram: {
                    required: true,
                    maxlength: 500
                },
                web_info_shopee: {
                    required: true,
                    maxlength: 500
                },
                web_info_address: {
                    required: true,
                    maxlength: 200
                },
                web_info_detail: 'required'
            },
            messages: {
                web_info_title: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                web_info_phone: {
                    required: "Hãy nhập vào số điện thoại.",
                    maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                },
                web_info_mail: {
                    required: "Hãy nhập vào email.",
                    email: "Bạn cần nhập chính xác email.",
                    maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                },
                web_info_zalo: {
                    required: "Hãy nhập vào đường dẫn.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
                web_info_facebook: {
                    required: "Hãy nhập vào đường dẫn.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
                web_info_youtube: {
                    required: "Hãy nhập vào đường dẫn.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
                web_info_instagram: {
                    required: "Hãy nhập vào đường dẫn.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
                web_info_shopee: {
                    required: "Hãy nhập vào đường dẫn.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
                web_info_address: {
                    required: "Hãy nhập vào địa chỉ.",
                    maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                },
                web_info_detail: "Hãy nhập vào nội dung."

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@endsection
