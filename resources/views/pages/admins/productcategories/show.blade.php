@extends('layouts.admin')

@section('title', 'Danh Mục Sản Phẩm - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/product-category">Danh Mục Sản Phẩm</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Danh Mục  <span class="text-primary">{{$category->product_category_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $category->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($category->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($category->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($category->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($category->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($category->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($category->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($category->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($category->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <h3 class="widget-title">Thông Tin</h3>
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col" width="15%">LOẠI</th>
                            <td>{{$category->product_type_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TIÊU ĐỀ</th>
                            <td>{{$category->product_category_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">MÔ TẢ</th>
                            <td>{{$category->product_category_description}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SEO TIÊU ĐỀ</th>
                            <td>{{$category->web_title}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SEO TỪ KHÓA</th>
                            <td>{{$category->web_keywords}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SEO MÔ TẢ</th>
                            <td>{{$category->web_description}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SEO ĐƯỜNG DẪN</th>
                            <td>
                                <a href="{{url('/'.$category->type_web_canonical.'/'.$category->web_canonical)}}" class="text-primary">
                                    {{url('/'.$category->type_web_canonical.'/'.$category->web_canonical)}}
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <a href="/admins/product-category" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

    </div>
    <!-- /Main Content -->
@stop


