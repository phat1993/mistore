@extends('layouts.admin')


@section('title', 'Danh Mục Sản Phẩm - Tạo mới')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/product-category">Danh Mục Sản Phẩm</a></li>
        <li class="breadcrumb-item" active>Tạo mới</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Tạo thêm Danh Mục Sản Phẩm</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/product-category')}}" method="POST" enctype="multipart/form-data" name="create-category">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label>Loại<span class="text-danger">(*)</span>: </label>
                            <select class="form-control" name="product_type_id">
                                @forelse ($types as $item)
                                    <option value="{{ $item->product_type_id }}">{{ $item->product_type_name }}</option>
                                @empty
                                    <option value="">  </option>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tên Danh Mục<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào tiêu đề" maxlength="100" class="form-control" name="product_category_name" value="{{ $category->product_category_name }}">
                        </div>
                        <div class="form-group">
                            <label>Chi Tiết:</label>
                            <textarea class="form-control" maxlength="100" placeholder="Nhập vào chi tiết" name="product_category_description" rows="4" cols="50">{{ $category->product_category_description }}</textarea>
                        </div>

                        <div class="card card-body">
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Tiêu Đề Website : </label>
                                <input type="text" placeholder="Nhập vào tiêu đề" maxlength="20" class="form-control" name="web_title" value="{{ $category->web_title }}">
                            </div>
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Từ Khóa Tìm Kiếm Website: </label>
                                <input type="text" placeholder="Nhập vào từ khóa " maxlength="200" class="form-control" name="web_keywords" value="{{ $category->web_keywords }}">
                            </div>
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Mô Tả Website: </label>
                                <input type="text" placeholder="Nhập vào mô tả" maxlength="120" class="form-control" name="web_description" value="{{ $category->web_description }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status" name="active_status" {{ $category->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Tạo Mới"/>
                        <a href="/admins/product-category" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        $( "form[name=create-category]" ).validate( {
            rules: {
                product_category_name: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                product_category_name: {
                    required: "Hãy nhập vào tên hạng mục.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
{{--
<script>

		$.validator.setDefaults( {
			submitHandler: function () {
				alert( "submitted!" );
			}
		} );

		$( document ).ready( function () {
			$( "#signupForm" ).validate( {
				rules: {
					firstname: "required",
					lastname: "required",
					username: {
						required: true,
						minlength: 2
					},
					password: {
						required: true,
						minlength: 5
					},
					confirm_password: {
						required: true,
						minlength: 5,
						equalTo: "#password"
					},
					email: {
						required: true,
						email: true
					},
					agree: "required"
				},
				messages: {
					firstname: "Please enter your firstname",
					lastname: "Please enter your lastname",
					username: {
						required: "Please enter a username",
						minlength: "Your username must consist of at least 2 characters"
					},
					password: {
						required: "Please provide a password",
						minlength: "Your password must be at least 5 characters long"
					},
					confirm_password: {
						required: "Please provide a password",
						minlength: "Your password must be at least 5 characters long",
						equalTo: "Please enter the same password as above"
					},
					email: "Please enter a valid email address",
					agree: "Please accept our policy"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `invalid-feedback` class to the error element
					error.addClass( "invalid-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.next( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
				}
			} );

		} );

</script> --}}
@endsection
