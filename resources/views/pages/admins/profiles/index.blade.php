@extends('layouts.admin')
@section('styles')

    <style>
        /**
        * Profile
        */
        /*** Profile: Header  ***/
        .profile__avatar {
        float: left;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        margin-right: 20px;
        overflow: hidden;
        }
        @media (min-width: 768px) {
        .profile__avatar {
            width: 100px;
            height: 100px;
        }
        }
        .profile__avatar > img {
        width: 100%;
        height: auto;
        }
        .profile__header {
        overflow: hidden;
        }
        .profile__header p {
        margin: 5px 0;
        }
        /*** Profile: Table ***/
        @media (min-width: 992px) {
        .profile__table tbody th {
            width: 200px;
        }
        }
    </style>
@endsection
@section('content')

    <!-- Main Content -->
    <div class="row">
        <div class="col-sm-6">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="card-body">
                    <div class="profile__avatar">
                        <img src="{{ asset('storage/'.$user->admin_avatar) }}" alt="...">
                    </div>
                    <div class="profile__header">
                        <h4>{{ $user->admin_abbreviation }} <small class="text-info">{{ $user->role_name }} </small></h4>
                        <p class="text-muted">
                            {{ $user->special_notes }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- User info -->
                <div class="card-body">
                    <div class="card-title">
                    <h4 class="card-text">
                        <span class="text-danger">Thông Tin</span>
                        <a href="#" class="btn btn-outline-success rounded-circle text-right"><i class="ti-pencil"></i></a>
                    </h4>
                    </div>
                    <table class="table profile__table">
                        <tbody>
                            <tr>
                                <th width="15%">Họ Và Tên</th>
                                <td>{{ $user->admin_official_name }}</td>
                            </tr>
                            <tr>
                                <th>Tên Bổ Sung</th>
                                <td>{{ $user->admin_supplement_name }}</td>
                            </tr>
                            <tr>
                                <th>Mail</th>
                                <td>{{ $user->admin_email }}</td>
                            </tr>
                            <tr>
                                <th>Số Điện Thoại</th>
                                <td>{{ $user->admin_tel1 }} | {{ $user->admin_tel2 }}</td>
                            </tr>
                            <tr>
                                <th>Lần Cuối Đăng Nhập</th>
                                <td>
                                    @if ($user->last_login_time == null)
                                        <span class="color-red">Chưa Đăng Nhập</span>
                                    @else
                                        <span class="color-red">{{date('H:i:s', strtotime($user->last_login_time))}}</span>
                                        <span class="color-red">{{date('d-m-Y', strtotime($user->last_login_time))}}</span>
                                    @endif</td>
                            </tr>
                            <tr>
                                <th>Địa Chỉ</th>
                                <td>{{ $user->admin_address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6 d-none d-md-block">
{{--
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- User info -->
                <div class="card-body">
                    <div class="card-title">
                    <h4 class="card-text text-danger">
                        <span>Thống Kê</span>
                        <hr>
                    </h4>
                    </div>
                    <div id="lineMorris" class="chart-home" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="255" version="1.1" width="758.5" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="36.203125" y="216" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,216H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="168.25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">17.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,168.25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="120.5" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">35</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,120.5H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="72.75" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">52.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,72.75H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">70</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="607.7039803832116" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018</tspan></text><text x="303.6275091240876" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2017</tspan></text><path fill="none" stroke="#eb3349" d="M48.703125,161.42857142857144C67.86410812043795,164.83928571428572,106.18607436131387,179.84642857142856,125.34705748175182,175.07142857142856C144.50804060218977,170.29642857142855,182.8300068430657,120.48508977361436,201.99098996350364,123.22857142857143C220.94370152828466,125.94223263075722,258.8491246578467,195.8711325966851,277.80183622262774,196.9C296.54627623175185,197.91756116811365,334.03515625,135.12484301412871,352.7795962591241,131.4142857142857C371.94057937956205,127.6212715855573,410.26254562043795,180.1875,429.4235287408759,166.8857142857143C448.58451186131384,153.58392857142857,486.9064781021898,29.80109289617487,506.06746122262774,25C525.0201727874088,20.251092896174864,562.9255959169708,109.82314522494082,581.8783074817518,128.68571428571428C600.6227474908759,147.34100236779796,638.1116275091241,182.15521978021977,656.8560675182482,175.07142857142856C676.0170506386862,167.83021978021978,714.339016879562,97.30714285714285,733.5,71.38571428571427" stroke-width="2" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="48.703125" cy="161.42857142857144" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="125.34705748175182" cy="175.07142857142856" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="201.99098996350364" cy="123.22857142857143" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="277.80183622262774" cy="196.9" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="352.7795962591241" cy="131.4142857142857" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="429.4235287408759" cy="166.8857142857143" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="506.06746122262774" cy="25" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="581.8783074817518" cy="128.68571428571428" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="656.8560675182482" cy="175.07142857142856" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="733.5" cy="71.38571428571427" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 14.0156px; top: 92px; display: none;"><div class="morris-hover-row-label">2016 Q1</div><div class="morris-hover-point" style="color: #EB3349">
Item 1:
20
</div></div></div>
                </div>
            </div> --}}

            <div class="widget-area-2 lorvens-box-shadow">
                <!-- User info -->
                <div class="card-body">
                    <div class="card-title">
                        <h4 class="card-text text-danger">
                            <span>Lịch Sử Hoạt Động</span>
                        </h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>BĐS</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-success">Thêm</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Themeforest</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-warning">Cập Nhật</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Codecanyon</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-danger">Xóa</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>BĐS</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-success">Thêm</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Themeforest</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-warning">Cập Nhật</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Codecanyon</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-danger">Xóa</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>BĐS</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-success">Thêm</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Themeforest</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-warning">Cập Nhật</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Codecanyon</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-danger">Xóa</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>BĐS</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-success">Thêm</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Themeforest</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-warning">Cập Nhật</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Codecanyon</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-danger">Xóa</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Codecanyon</td>
                                    <td>12:12 01-02-2021</td>
                                    <td>
                                        <span class="badge badge-danger">Xóa</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">

            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Community -->
                <div class="card-body">
                    <div class="card-title">
                    <h4 class="card-text">XXXXXXXXXXXXXXXXXXXx</h4>
                    </div>
                    <table class="table profile__table">
                        <tbody>
                        <tr>
                            <th><strong>Comments</strong></th>
                            <td>58584</td>
                        </tr>
                        <tr>
                            <th><strong>Member since</strong></th>
                            <td>Jan 01, 2016</td>
                        </tr>
                        <tr>
                            <th><strong>Last login</strong></th>
                            <td>1 day ago</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content -->
@stop





