@extends('layouts.admin')
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Thống Kê</h3>
        </div>
    </div>
    <!-- /Page Title -->
    <div class="row">
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="fa fa-users"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Doanh Nghiệp</h4>
                    <span class="numeric color-red">25</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-blue">
                <div class="widget-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người Dùng</h4>
                    <span class="numeric color-blue">5000</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-green">
                <div class="widget-left">
                    <span class="fa fa-audio-description"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Quảng Cáo</h4>
                    <span class="numeric color-green">50</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-yellow">
                <div class="widget-left">
                    <span class="fa fa-building"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Dự Án</h4>
                    <span class="numeric color-yellow">100</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-yellow">
                <div class="widget-left">
                    <span class="fa fa-home"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Bất Động Sản</h4>
                    <span class="numeric color-yellow">50000</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-yellow">
                <div class="widget-left">
                    <span class="fa fa-edit"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Đàm Phán</h4>
                    <span class="numeric color-yellow">35000</span>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
    </div>
    <div class="row">
        <!-- Widget Item -->
        <div class="col-md-6">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Truy cập website theo tháng</h3>
                <div id="lineMorris" class="chart-home" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="255" version="1.1" width="758.5" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="36.203125" y="216" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,216H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="168.25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">17.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,168.25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="120.5" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">35</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,120.5H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="72.75" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">52.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,72.75H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">70</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="607.7039803832116" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">08</tspan></text><text x="303.6275091240876" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">07</tspan></text><path fill="none" stroke="#eb3349" d="M48.703125,161.42857142857144C67.86410812043795,164.83928571428572,106.18607436131387,179.84642857142856,125.34705748175182,175.07142857142856C144.50804060218977,170.29642857142855,182.8300068430657,120.48508977361436,201.99098996350364,123.22857142857143C220.94370152828466,125.94223263075722,258.8491246578467,195.8711325966851,277.80183622262774,196.9C296.54627623175185,197.91756116811365,334.03515625,135.12484301412871,352.7795962591241,131.4142857142857C371.94057937956205,127.6212715855573,410.26254562043795,180.1875,429.4235287408759,166.8857142857143C448.58451186131384,153.58392857142857,486.9064781021898,29.80109289617487,506.06746122262774,25C525.0201727874088,20.251092896174864,562.9255959169708,109.82314522494082,581.8783074817518,128.68571428571428C600.6227474908759,147.34100236779796,638.1116275091241,182.15521978021977,656.8560675182482,175.07142857142856C676.0170506386862,167.83021978021978,714.339016879562,97.30714285714285,733.5,71.38571428571427" stroke-width="2" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="48.703125" cy="161.42857142857144" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="125.34705748175182" cy="175.07142857142856" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="201.99098996350364" cy="123.22857142857143" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="277.80183622262774" cy="196.9" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="352.7795962591241" cy="131.4142857142857" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="429.4235287408759" cy="166.8857142857143" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="506.06746122262774" cy="25" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="581.8783074817518" cy="128.68571428571428" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="656.8560675182482" cy="175.07142857142856" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="733.5" cy="71.38571428571427" r="4" fill="#eb3349" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 14.0156px; top: 92px; display: none;"><div class="morris-hover-row-label">2016 Q1</div><div class="morris-hover-point" style="color: #EB3349">
Item 1:
20
</div></div></div>
            </div>
        </div>
        <!-- /Widget Item -->
        <!-- Widget Item -->
        <div class="col-md-6">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Đàm phán theo tháng</h3>
                <div id="barMorris" class="chart-home" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="255" version="1.1" width="758.5" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.5px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="26.1875" y="216" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M38.6875,216H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="26.1875" y="168.25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15</tspan></text><path fill="none" stroke="#eef0f2" d="M38.6875,168.25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="26.1875" y="120.5" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30</tspan></text><path fill="none" stroke="#eef0f2" d="M38.6875,120.5H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="26.1875" y="72.75" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">45</tspan></text><path fill="none" stroke="#eef0f2" d="M38.6875,72.75H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="26.1875" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">60</tspan></text><path fill="none" stroke="#eef0f2" d="M38.6875,25H733.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="683.8705357142857" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">08</tspan></text><text x="584.6116071428571" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">07</tspan></text><text x="485.35267857142856" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">06</tspan></text><text x="386.09375" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">05</tspan></text><text x="286.83482142857144" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">04</tspan></text><text x="187.57589285714286" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">03</tspan></text><text x="88.31696428571428" y="228.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">02</tspan></text><rect x="63.50223214285714" y="152.33333333333334" width="49.629464285714285" height="63.66666666666666" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="162.7611607142857" y="72.75" width="49.629464285714285" height="143.25" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="262.0200892857143" y="37.73333333333335" width="49.629464285714285" height="178.26666666666665" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="361.2790178571429" y="104.58333333333334" width="49.629464285714285" height="111.41666666666666" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="460.53794642857144" y="158.7" width="49.629464285714285" height="57.30000000000001" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="559.796875" y="126.86666666666667" width="49.629464285714285" height="89.13333333333333" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="659.0558035714286" y="152.33333333333334" width="49.629464285714285" height="63.66666666666666" r="0" rx="0" ry="0" fill="#86c417" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 51.9654px; top: 97px; display: none;"><div class="morris-hover-row-label">2012</div><div class="morris-hover-point" style="color: #86C417">
Clients:
20
</div></div></div>
            </div>
        </div>
        <!-- /Widget Item -->
    </div>
    <div class="row">
        <!-- Widget Item -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Payments</h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Products</th>
                                <th>Agent</th>
                                <th>Sales</th>
                                <th>Earnings</th>
                                <th>Technology</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Envato</td>
                                <td>Manoj Kumar</td>
                                <td>390</td>
                                <td>$400</td>
                                <td>
                                    <span class="badge badge-success">PHP</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Themeforest</td>
                                <td>John Deo</td>
                                <td>70</td>
                                <td>$3670</td>
                                <td>
                                    <span class="badge badge-warning">Java</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Codecanyon</td>
                                <td>Yokahona</td>
                                <td>150</td>
                                <td>$1400</td>
                                <td>
                                    <span class="badge badge-danger">Angular Js</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Videohive</td>
                                <td>Yokahona</td>
                                <td>150</td>
                                <td>$1400</td>
                                <td>
                                    <span class="badge badge-success">Python</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Themeforest</td>
                                <td>Yokahona</td>
                                <td>150</td>
                                <td>$1400</td>
                                <td>
                                    <span class="badge badge-warning">Dot Net</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Envato</td>
                                <td>Yokahona</td>
                                <td>150</td>
                                <td>$1400</td>
                                <td>
                                    <span class="badge badge-danger">Angular Js</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /Widget Item -->
    </div>
    <!-- /Main Content -->
@stop






