@extends('layouts.admin')


@section('title', 'Sản Phẩm - Tạo mới')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/product">Sản Phẩm</a></li>
        <li class="breadcrumb-item" active>Tạo mới</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Tạo thêm Sản Phẩm</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/product')}}" method="POST" enctype="multipart/form-data" name="create-banner">
                        {{csrf_field()}}

                        <div class="card card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Loại<span class="text-danger">(*)</span>: </label>
                                        <select class="form-control" onchange="changeType()" name="product_type_id">
                                            @forelse ($types as $item)
                                                <option value="{{ $item->product_type_id }}">{{ $item->product_type_name }}</option>
                                            @empty
                                                <option value="">  </option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Danh mục<span class="text-danger">(*)</span>: </label>
                                        <select class="form-control" name="product_category_id">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tên Sản Phẩm<span class="text-danger">(*)</span>: </label>
                                <input type="text" placeholder="Nhập vào tiêu đề" class="form-control" name="product_name" value="{{ $product->product_name }}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Giá Tiền<span class="text-danger">(*)</span>: </label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="price-addon2">VNĐ</span>
                                            <input type="text" placeholder="Nhập vào giá tiền của sản phẩm" class="form-control text-right" data-type='currency'
                                            name="product_price" value="{{$product->product_price}}"  aria-label="Giá"
                                                aria-describedby="price-addon2">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Khuyến Mãi: </label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="price-addon2">VNĐ</span>
                                            <input type="text" placeholder="Nhập vào số tiền khuyến mãi" class="form-control text-right" data-type='currency'
                                            name="product_discount" value="{{$product->product_discount}}"  aria-label="Khuyến Mãi"
                                                aria-describedby="price-addon2">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="form-group">
                                <label>Mô Tả<span class="text-danger">(*)</span>:</label>
                                <textarea class="form-control" placeholder="Nhập vào chi tiết" maxlength="120" name="product_description" rows="4" cols="50">{{ $product->product_description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Chi Tiết<span class="text-danger">(*)</span>:</label>
                                <textarea class="form-control" placeholder="Nhập vào chi tiết" name="product_detail" rows="4" cols="50">{{ $product->product_detail }}</textarea>
                            </div>
                        </div>

                        <br>
                        <div class="card card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Hình Ảnh 1 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_1" id="image_1">
                                                <label class="custom-file-label" for="customFile">{{ $product->product_img_1==''?'Chọn hình ảnh':$product->product_img_1 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_1" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_1" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 2 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_2" id="image_2">
                                                <label class="custom-file-label">{{ $product->product_img_2==''?'Chọn hình ảnh':$product->product_img_2 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_2" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_2" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 3 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_3" id="image_3">
                                                <label class="custom-file-label">{{ $product->product_img_3==''?'Chọn hình ảnh':$product->product_img_3 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_3" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_3" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 4 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_4" id="image_4">
                                                <label class="custom-file-label">{{ $product->product_img_4==''?'Chọn hình ảnh':$product->product_img_4 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_4" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_4" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 5 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_5" id="image_5">
                                                <label class="custom-file-label">{{ $product->product_img_5==''?'Chọn hình ảnh':$product->product_img_5 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_5" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_5" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 6 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_6" id="image_6">
                                                <label class="custom-file-label">{{ $product->product_img_6==''?'Chọn hình ảnh':$product->product_img_6 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_6" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_6" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình Ảnh 7 : </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image_7" id="image_7">
                                                <label class="custom-file-label">{{ $product->product_img_7==''?'Chọn hình ảnh':$product->product_img_7 }}</label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="clearImage_7" class="btn btn-default">Xóa</a>
                                        </div>
                                        <div class="col-10">
                                            <div class="img-thumbnail">
                                                <img  id="loadImage_7" style="object-fit: cover;width:31rem;height:18rem"src="{{asset('storage/images/no-image.jpg')}}"alt="Image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Video: </label>
                                    <div class="row">
                                        <div class="col-10">
                                            <input type="text" class="form-control" name="video">
                                        </div>
                                        <div class="col-md-10">
                                            <div class="ratio ratio-1x1">
                                                <iframe  style="width:40rem;height:18rem" id="loadVideo" title="YouTube video" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div  class="card card-body">
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Tiêu Đề Website : </label>
                                <input type="text" placeholder="Nhập vào tiêu đề" maxlength="20" class="form-control" name="web_title" value="{{ $product->web_title }}">
                            </div>
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Từ Khóa Tìm Kiếm Website: </label>
                                <input type="text" placeholder="Nhập vào từ khóa " maxlength="200" class="form-control" name="web_keywords" value="{{ $product->web_keywords }}">
                            </div>
                            <div class="form-group">
                                <label><span class="text-danger">SEO</span> Mô Tả Website: </label>
                                <input type="text" placeholder="Nhập vào mô tả" maxlength="120" class="form-control" name="web_description" value="{{ $product->web_description }}">
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status" name="active_status" {{ $product->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Tạo Mới"/>
                        <a href="/admins/product" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')

<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if(input.name == 'image_1')
                    $('#loadImage_1').attr('src', e.target.result);
                else if(input.name == 'image_2')
                    $('#loadImage_2').attr('src', e.target.result);
                else if(input.name == 'image_3')
                    $('#loadImage_3').attr('src', e.target.result);
                else if(input.name == 'image_4')
                    $('#loadImage_4').attr('src', e.target.result);
                else if(input.name == 'image_5')
                    $('#loadImage_5').attr('src', e.target.result);
                else if(input.name == 'image_6')
                    $('#loadImage_6').attr('src', e.target.result);
                else if(input.name == 'image_7')
                    $('#loadImage_7').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#clearImage_1').click(function() {
        $("input[name='image_1']").val('');
        $('#image_1').next('label').html('Chọn hình ảnh');
        $('#loadImage_1').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $('#clearImage_2').click(function() {
        $("input[name='image_2']").val('');
        $('#image_2').next('label').html('Chọn hình ảnh');
        $('#loadImage_2').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $('#clearImage_3').click(function() {
        $("input[name='image_3']").val('');
        $('#image_3').next('label').html('Chọn hình ảnh');
        $('#loadImage_3').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $('#clearImage_4').click(function() {
        $("input[name='image_4']").val('');
        $('#image_4').next('label').html('Chọn hình ảnh');
        $('#loadImage_4').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $('#clearImage_5').click(function() {
        $("input[name='image_5']").val('');
        $('#image_5').next('label').html('Chọn hình ảnh');
        $('#loadImage_5').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });

    $('#clearImage_6').click(function() {
        $("input[name='image_6']").val('');
        $('#image_6').next('label').html('Chọn hình ảnh');
        $('#loadImage_6').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $('#clearImage_7').click(function() {
        $("input[name='image_7']").val('');
        $('#image_7').next('label').html('Chọn hình ảnh');
        $('#loadImage_7').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });
    $("input[name='image_1']").change(function() {
        readURL(this);
    });
    $("input[name='image_2']").change(function() {
        readURL(this);
    });
    $("input[name='image_3']").change(function() {
        readURL(this);
    });
    $("input[name='image_4']").change(function() {
        readURL(this);
    });
    $("input[name='image_5']").change(function() {
        readURL(this);
    });
    $("input[name='image_6']").change(function() {
        readURL(this);
    });
    $("input[name='image_7']").change(function() {
        readURL(this);
    });
    $("input[name='video']").change(function() {
        $('#loadVideo').attr('src', $("input[name='video']").val());
    });


    function changeType() {
            var proType = $("select[name='product_type_id']").children("option:selected").val();
            $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/product/change-type/'+proType,
                success: function(data){
                   var selectElment = $("select[name='product_category_id']");
                   selectElment.find('option').remove().end();
                   $.each(data, function(i,item){
                       selectElment.append($('<option>', {
                                value: item['product_category_id'],
                                text : item['product_category_name']
                            }));
                    });
                }
            })
        }

</script>

<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>
    $( document ).ready( function () {
        changeType();
        $( "form[name=create-banner]" ).validate( {
            rules: {
                product_category_id: "required",
                product_name: {
                    required: true,
                    maxlength: 100
                },
                product_description: {
                    required: true,
                    maxlength: 120
                },
                product_price: "required",
            },
            messages: {
                product_category_id: "Hãy chọn danh mục cho sản phẩm",
                product_name: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                product_description: {
                    required: "Hãy nhập vào mô tả.",
                    maxlength: "Bạn chỉ được nhập tối đa 120 ký tự."
                },
                product_price: "Hãy nhập giá tiền cho sản phẩm",
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );

    $("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency($(this));
        },
        blur: function() {
            formatCurrency($(this), "blur");
        },
    });

    function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }


    function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    var original_len = input_val.length;

    // initial caret position
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

        // // get position of first decimal
        // // this prevents multiple decimals from
        // // being entered
        // var decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        // var right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = formatNumber(left_side);

        // // validate right side
        // right_side = formatNumber(right_side);

        // // On blur make sure 2 numbers after decimal
        // if (blur === "blur") {
        // right_side += "00";
        // }

        // // Limit decimal to only 2 digits
        // right_side = right_side.substring(0, 2);

        // join number by .
        input_val = left_side + "." + right_side;

    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = input_val;

        // // final formatting
        // if (blur === "blur") {
        // input_val += ".00";
        // }
    }

    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>
<script src={{ url('/asset/plugins/ckeditor/ckeditor.js') }}></script>
<script>
    CKEDITOR.replace( 'product_detail' );
</script>
@endsection
