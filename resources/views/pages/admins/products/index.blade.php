@extends('layouts.admin')
@section('title', 'Sản Phẩm')

@section('styles')
    <style>
        input[type=checkbox] {
            transform: scale(1.8);
        }
    </style>
@endsection
@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Sản Phẩm</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Sản Phẩm </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/product')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row align-items-center">
                            <div class="col-sm-2">
                                <select class="form-control" name="product_type_id" onchange="changeType()" >
                                    <option value="" {{Request::get('product_type_id') == '' ? 'selected': ''}}>Loại</option>
                                    @forelse ($types as $item)
                                        <option value="{{ $item->product_type_id }}"  {{Request::get('product_type_id') == $item->product_type_id ? 'selected': ''}}>{{ $item->product_type_name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="product_category_id">
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="product_name" value="{{Request::get('product_name')}}" placeholder="Tìm kiếm theo Tiêu đề">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="product_description" value="{{Request::get('product_description')}}" placeholder="Tìm kiếm theo Mô tả">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="product_price" value="{{Request::get('product_price')}}" data-type='currency' placeholder="Giá tiền">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="product_discount" value="{{Request::get('product_discount')}}" data-type='currency' placeholder="Khuyến Mãi">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name='active_status'>
                                    <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Hoạt Động</option>
                                    <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                    <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-inline"><input type="checkbox" class="form-control" name="view_all" value="1" {{ Request::get('view_all')==1?'checked':'' }}>
                                    <label for="view_all" class="ml-3" id="ckTitle">Hiển Thị Tất Cả</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th><a href="/admins/product/create" class="btn btn-outline-primary"><i class="ti-plus"></i></a></th>
                            <th>HÌNH ẢNH</th>
                            <th>TIÊU ĐỀ</th>
                            <th>MÔ TẢ</th>
                            <th>GIÁ TIỀN(VNĐ)</th>
                            <th>KHUYẾN MÃI(VNĐ)</th>
                            <th>HOẠT ĐỘNG</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($Products as $product)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td width="10%">
                                        <img style="object-fit: cover;width:100%;"
                                        src="{{asset('storage/'.$product->product_img_1)}}"
                                        alt="image"></td>
                                    <td>{{$product->product_name}}</td>
                                    <td>{{$product->product_description}}</td>
                                    <td>
                                        @if($product->product_discount!=-1)
                                        <del>
                                        @if (Str::length($product->product_price)>9)
                                            {{ is_int(($product->product_price/1000000000))?($product->product_price/1000000000).' Tỷ': number_format($product->product_price,0,'',',') }}
                                        @elseif(Str::length($product->product_price)>6)
                                            {{ is_int(($product->product_price/1000000))?($product->product_price/1000000).' Triệu': number_format($product->product_price,0,'',',') }}
                                        @else
                                            {{ number_format($product->product_price,0,'',',') }}
                                        @endif
                                        </del>
                                        @else
                                            @if (Str::length($product->product_price)>9)
                                                {{ is_int(($product->product_price/1000000000))?($product->product_price/1000000000).' Tỷ': number_format($product->product_price,0,'',',') }}
                                            @elseif(Str::length($product->product_price)>6)
                                                {{ is_int(($product->product_price/1000000))?($product->product_price/1000000).' Triệu': number_format($product->product_price,0,'',',') }}
                                            @else
                                                {{ number_format($product->product_price,0,'',',') }}
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->product_discount!=-1)
                                            @if (Str::length($product->product_discount)>9)
                                                {{ is_int(($product->product_discount/1000000000))?($product->product_discount/1000000000).' Tỷ': number_format($product->product_discount,0,'',',') }}
                                            @elseif(Str::length($product->product_discount)>6)
                                                {{ is_int(($product->product_discount/1000000))?($product->product_discount/1000000).' Triệu': number_format($product->product_discount,0,'',',') }}
                                            @else
                                                {{ number_format($product->product_discount,0,'',',') }}
                                            @endif
                                        @else
                                            {{-- Không Có --}}
                                        @endif

                                    </td>
                                    <td>@if($product->active_status == 1) BẬT @else TẮT @endif </td>
                                    <td>
                                        @if ($product->deleted_at!=null)
                                            <a class="btn btn-outline-info" href="javascript:void(0)" onclick="form_action('view', {{$product->product_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-outline-success" href="javascript:void(0)" onclick="form_action('recovered', {{$product->product_id}});" role="button">
                                                <i class="ti-reload"></i>
                                            </a>
                                            <a class="btn btn-outline-danger" href="javascript:void(0)" onclick="form_action('delete', {{$product->product_id}});" role="button">
                                                <i class="ti-close"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="form_action('view', {{$product->product_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-warning" href="javascript:void(0)" onclick="form_action('edit', {{$product->product_id}});" role="button">
                                                <i class="ti-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="form_action('destroy', {{$product->product_id}});" role="button">
                                                <i class="ti-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $Products->appends([
                    'product_type_id' => Request::get('product_type_id'),
                    'product_category_id' => Request::get('product_category_id'),
                    'product_name' => Request::get('product_name'),
                    'product_description' => Request::get('product_description'),
                    'product_price' => Request::get('product_price'),
                    'product_discount' => Request::get('product_discount'),
                    'active_status' => Request::get('active_status'),
                    'view_all' => Request::get('view_all'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="product_id" value="">
            </div>
        </form>

    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>


        function changeType() {
            var proType = $("select[name='product_type_id']").children("option:selected").val();
            if(proType=="")return;
            $.ajax({
                contentType: "application/json",
                dataType: "json",
                url: '/admins/product/change-type/'+proType,
                success: function(data){
                   var selectElment = $("select[name='product_category_id']");
                   selectElment.find('option').remove().end();
                   $.each(data, function(i,item){
                       var op =$('<option>', {
                                value: item['product_category_id'],
                                text : item['product_category_name']
                            });
                            if(item['product_category_id']=="{{Request::get('product_category_id')}}")
                            {
                                op.prop("selected", "selected");
                            }
                       selectElment.append(op);
                    });
                },
                error:function(){
                   var selectElment = $("select[name='product_category_id']");
                   selectElment.find('option').remove().end();
                    selectElment.append($('<option>', {
                            value: "",
                            text : "Danh Mục"
                        }));
                }
            })
        }
        function form_action (mode, product_id) {
           let frm = document.form1;

           if(mode == 'destroy'){
               var cf = confirm('Bạn có chắc chắn muốn xóa đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/product/destroy')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'delete'){
               var cf = confirm('Bạn có chắc chắn muốn xóa vĩnh viễn đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/product/delete')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'recovered'){
               frm.action = "{{url('/admins/product/recovered')}}";
               frm.method = 'get';
           }else if(mode == 'edit'){
               frm.action = "{{url('/admins/product/edit')}}";
               frm.method = 'get';
           }else if(mode == 'view'){
               frm.action = "{{url('/admins/product/show')}}";
               frm.method = 'get';
           }

            frm.product_id.value = product_id
            frm.submit();
        }
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        $("#ckTitle").click(function () {
            var $checkbox = $('input[name=view_all]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
        });
        initialSetup();
        changeType();

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });

        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        var original_len = input_val.length;

        // initial caret position
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // // get position of first decimal
            // // this prevents multiple decimals from
            // // being entered
            // var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            // var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // // validate right side
            // right_side = formatNumber(right_side);

            // // On blur make sure 2 numbers after decimal
            // if (blur === "blur") {
            // right_side += "00";
            // }

            // // Limit decimal to only 2 digits
            // right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // // final formatting
            // if (blur === "blur") {
            // input_val += ".00";
            // }
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        }


    </script>
@stop


