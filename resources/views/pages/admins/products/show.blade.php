@extends('layouts.admin')

@section('title', 'Sản Phẩm - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/product">Sản Phẩm</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Sản Phẩm <span class="text-primary">{{$product->product_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $product->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($product->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($product->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($product->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($product->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($product->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($product->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($product->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($product->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow"><div class="bd-example bd-example-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sản Phẩm</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">Đánh Giá</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card card-body">
                            <h3 class="widget-title">Thông Tin</h3>
                            <table class="table table-bordered">
                                <tr>
                                    <th scope="col" width="15%">LOẠI</th>
                                    <td>{{$product->product_type_name}}</td>
                                </tr>
                                <tr>
                                    <th scope="col" width="15%">DANH MỤC</th>
                                    <td>{{$product->product_category_name}}</td>
                                </tr>
                                <tr>
                                    <th scope="col" width="15%">TÊN</th>
                                    <td>{{$product->product_name}}</td>
                                </tr>
                                <tr>
                                    <th scope="col">GIÁ TIỀN</th>

                                    @if($product->product_discount!=-1)
                                    <td>
                                        <del>
                                            @if (Str::length($product->product_price)>9)
                                                {{ is_int(($product->product_price/1000000000))?($product->product_price/1000000000).' Tỷ': number_format($product->product_price,0,'',',') }}
                                            @elseif(Str::length($product->product_price)>6)
                                                {{ is_int(($product->product_price/1000000))?($product->product_price/1000000).' Triệu': number_format($product->product_price,0,'',',') }}
                                            @else
                                                {{ number_format($product->product_price,0,'',',') }}
                                            @endif
                                        </del>
                                            <span class="badge badge-danger text-light"> vnđ</span>
                                    </td>
                                    @else
                                    <td>
                                        @if (Str::length($product->product_price)>9)
                                            {{ is_int(($product->product_price/1000000000))?($product->product_price/1000000000).' Tỷ': number_format($product->product_price,0,'',',') }}
                                        @elseif(Str::length($product->product_price)>6)
                                            {{ is_int(($product->product_price/1000000))?($product->product_price/1000000).' Triệu': number_format($product->product_price,0,'',',') }}
                                        @else
                                            {{ number_format($product->product_price,0,'',',') }}
                                        @endif
                                        <span class="badge badge-danger text-light"> vnđ</span>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th scope="col">KHUYẾN MÃI</th>
                                    <td>
                                        @if($product->product_discount!=-1)
                                            @if (Str::length($product->product_discount)>9)
                                                {{ is_int(($product->product_discount/1000000000))?($product->product_discount/1000000000).' Tỷ': number_format($product->product_discount,0,'',',') }}
                                            @elseif(Str::length($product->product_discount)>6)
                                                {{ is_int(($product->product_discount/1000000))?($product->product_discount/1000000).' Triệu': number_format($product->product_discount,0,'',',') }}
                                            @else
                                                {{ number_format($product->product_discount,0,'',',') }}
                                            @endif
                                            <span class="badge badge-danger text-light"> vnđ</span>
                                        @else
                                            {{-- Không Có --}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="col">MÔ TẢ</th>
                                    <td>{{$product->product_description}}</td>
                                </tr>
                                <tr>
                                    <th scope="col"><span class="text-danger">SEO</span> TIÊU ĐỀ</th>
                                    <td>{{$product->web_title}}</td>
                                </tr>
                                <tr>
                                    <th scope="col"><span class="text-danger">SEO</span> TỪ KHÓA</th>
                                    <td>{{$product->web_keywords}}</td>
                                </tr>
                                <tr>
                                    <th scope="col"><span class="text-danger">SEO</span> MÔ TẢ</th>
                                    <td>{{$product->web_description}}</td>
                                </tr>
                                <tr>
                                    <th scope="col"><span class="text-danger">SEO</span> ĐƯỜNG DẪN</th>
                                    <td>
                                        <a href="{{url('/'.$product->type_web_canonical.'/'.$product->category_web_canonical.'/'.$product->web_canonical)}}" class="text-primary">
                                            {{url('/'.$product->type_web_canonical.'/'.$product->category_web_canonical.'/'.$product->web_canonical)}}
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <div class="card card-body">
                            <h3 class="widget-title">Chi Tiết</h3>
                            {!! $product->product_detail !!}
                        </div>
                        <br>
                        <div class="card card-body">
                            <h3 class="widget-title">Hình Ảnh</h3>
                            @isset($product->product_img_1)
                            <div class="row">
                                <div class="col-md-3">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_1)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_1)}}" alt="{{ $product->product_name }}"
                                        style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_2)
                                <div class="col-md-3">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_2)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_2)}}" alt="{{ $product->product_name }}"
                                        style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_3)
                                <div class="col-md-3">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_3)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_3)}}" alt="{{ $product->product_name }}"
                                            style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_4)
                                <div class="col-md-3">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_4)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_4)}}" alt="{{ $product->product_name }}"
                                            style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_5)
                                <div class="col-md-3 mt-2">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_5)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_5)}}" alt="{{ $product->product_name }}"
                                            style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_6)
                                <div class="col-md-3 mt-2">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_6)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_6)}}" alt="{{ $product->product_name }}"
                                            style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_img_7)
                                <div class="col-md-3 mt-2">
                                    <a target="_blank" class="bg-light" href="{{asset('storage/'.$product->product_img_7)}}">
                                        <div class="img-thumbnail">
                                            <img src="{{asset('storage/'.$product->product_img_7)}}" alt="{{ $product->product_name }}"
                                            style=" width: 15rem;height:15rem;">
                                        </div>
                                    </a>
                                </div>
                                @endisset
                                @isset($product->product_video)
                                    <div class="col-md-3 mt-2">
                                        <div class="img-thumbnail">
                                        <div class="ratio ratio-1x1">
                                            <iframe id="loadVideo" style="height:14.5rem;" src="{{ url($product->product_video) }}" title="YouTube video" allowfullscreen></iframe>
                                        </div>
                                        </div>
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">

                        <div class="card card-body">
                            <h3 class="widget-title">Danh sách Đánh Giá</h3>
                            <ul class="list-unstyled">
                                @forelse ($Review as $item)
                                    <li class="media m-2">
                                        <div class="mr-3">
                                            <p class="mt-0 mb-1">
                                                <strong class="text-danger">{{ $item->product_review_mail }}</strong>({{ $item->product_review_ip }})
                                                <small>{{date('d-m-Y H:i:s', strtotime($item->created_at))}}</small>
                                                <span class="text-warning">
                                                @for ($i = 1; $i <= 5; $i++)
                                                    @if ($item->product_review_star>=$i)
                                                        <i class="fa fa-star"></i>
                                                    @else
                                                        <i class="fa fa-star-o"></i>
                                                    @endif
                                                @endfor
                                                </span>
                                            </p>
                                            <hr>
                                            <p>
                                                <strong class="text-info">{{ $item->product_review_description }}</strong></p>
                                        </div>
                                    </li>
                                @empty
                                    Hiện tại chưa có đánh giá!
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                <a href="/admins/product" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>
    </div>
    <!-- /Main Content -->
@stop




@section('javascript')
    <script>
        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

    </script>
@stop


