@extends('layouts.admin')


@section('title', 'Menu - Cập Nhật')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/shopmenu">Menu</a></li>
        <li class="breadcrumb-item" active>Cập Nhật</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Cập Nhật Menu</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/shopmenu/update')}}" method="POST" name="edit-menu">
                        {{csrf_field()}}

                        <div class="form-group">
                            <input type="hidden" class="form-control" name="shop_menu_id" value="{{Request::get('shop_menu_id')}}">
                        </div>

                        <div class="form-group">
                            <label>Tiêu đề<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào tiêu đề" maxlength="100" class="form-control" name="shop_menu_name" value="{{ $shopmenu->shop_menu_name }}">
                        </div>
                        <div class="form-group">
                            <label>Vị Trí<span class="text-danger">(*)</span>: </label>
                            <input type="number" placeholder="Nhập vào vị trí" maxlength="4" class="form-control" name="shop_menu_position" value="{{ $shopmenu->shop_menu_position }}">
                        </div>
                        <div class="card card-body">

                            <div class="form-group" id="enterLink">
                                <label>Đường Dẫn<span class="text-danger">(*/)</span>: </label>
                                <input type="text" placeholder="Nhập vào đường dẫn" maxlength="100" class="form-control" name="shop_menu_link" value="{{ $shopmenu->shop_menu_link }}">
                            </div>
                            <div class="form-group"  id="selectLink">
                                <label>Loại Sản Phẩm<span class="text-danger">(*/)</span>: </label>
                                <select class="form-control" onchange="changeType()" name="product_type_id">
                                    <option value="">  </option>
                                    @forelse ($types as $item)
                                        <option value="{{ $item->product_type_id }}"
                                            @if ($item->product_type_id==$shopmenu->product_type_id) selected @endif>{{ $item->product_type_name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <br>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status" name="active_status" {{ $shopmenu->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Cập Nhật"/>
                        <a href="/admins/shopmenu" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')

<script>
    $("input[name='shop_menu_link']").keyup(function() {
        if($(this).val() != "") {
            $('#selectLink').hide();
        } else {
            $('#selectLink').show();
        }
    });

    function changeType() {
        var proType = $("select[name='product_type_id']").children("option:selected").val();
        if(proType != "") {
            $('#enterLink').hide();
        } else {
            $('#enterLink').show();
        }
    }

</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        changeType();
        $( "form[name=edit-menu]" ).validate( {
            rules: {
                shop_menu_name: {
                    required: true,
                    maxlength: 100
                },
                shop_menu_position: {
                    required: true,
                    digits: true,
                    maxlength: 4
                },
            },
            messages: {
                shop_menu_name: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                shop_menu_position: {
                    required: "Hãy nhập vào vị trí.",
                    digits: "Bạn chỉ được nhập số.",
                    maxlength: "Bạn chỉ được nhập tối đa 4 số."
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@endsection
