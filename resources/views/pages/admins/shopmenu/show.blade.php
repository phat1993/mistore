@extends('layouts.admin')

@section('title', 'Menu - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/shopmenu">Menu</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Menu  <span class="text-primary">{{$shopmenu->shop_menu_name}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $shopmenu->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($shopmenu->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($shopmenu->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($shopmenu->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($shopmenu->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($shopmenu->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($shopmenu->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($shopmenu->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($shopmenu->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col" width="15%">LOẠI</th>
                            <td>{{$shopmenu->product_type_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">TIÊU ĐỀ</th>
                            <td>{{$shopmenu->shop_menu_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">VỊ TRÍ</th>
                            <td>{{$shopmenu->shop_menu_position}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐƯỜNG DẪN</th>
                            <td>
                                @isset($shopmenu->shop_menu_link)
                                    <a href="{{$shopmenu->shop_menu_link}}" class="text-primary">
                                        {{$shopmenu->shop_menu_link}}
                                    </a>
                                @endisset
                                @isset($shopmenu->product_type_id)
                                    <a href="{{url('/'.$shopmenu->web_canonical)}}" class="text-primary">
                                        {{url('/'.$shopmenu->web_canonical)}}
                                    </a>
                                @endisset
                            </td>
                        </tr>
                    </table>
                </div>
                <a href="/admins/shopmenu" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

    </div>
    <!-- /Main Content -->
@stop


