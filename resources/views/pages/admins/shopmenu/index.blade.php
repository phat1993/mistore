@extends('layouts.admin')
@section('title', 'Menu')

@section('styles')
    <style>
        input[type=checkbox] {
            transform: scale(1.8);
        }
    </style>
@endsection
@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Menu</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Menu </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/shopmenu')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="shop_menu_name" value="{{Request::get('shop_menu_name')}}" placeholder="Tìm kiếm theo Tiêu đề">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="shop_menu_position" value="{{Request::get('shop_menu_position')}}" placeholder="Tìm kiếm theo Vị trí">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name='active_status'>
                                    <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Hoạt Động</option>
                                    <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                    <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-inline"><input type="checkbox" class="form-control" name="view_all" value="1" {{ Request::get('view_all')==1?'checked':'' }}>
                                    <label for="view_all" class="ml-3" id="ckTitle">Hiển Thị Tất Cả</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th><a href="/admins/shopmenu/create" class="btn btn-outline-primary"><i class="ti-plus"></i></a></th>
                            <th>TIÊU ĐỀ</th>
                            <th>VỊ TRÍ</th>
                            <th>ĐƯỜNG DẪN</th>
                            <th>LOẠI SẢN PHẨM</th>
                            <th>HOẠT ĐỘNG</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($ShopMenus as $smenu)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>{{$smenu->shop_menu_name}}</td>
                                    <td>{{$smenu->shop_menu_position}}</td>
                                    <td>
                                        @isset($shopmenu->shop_menu_link)
                                            <a href="{{$shopmenu->shop_menu_link}}" class="text-primary">
                                                {{$shopmenu->shop_menu_link}}
                                            </a>
                                        @endisset
                                        @isset($shopmenu->product_type_id)
                                            <a href="{{'/category/'.$shopmenu->product_type_id}}" class="text-primary">
                                                {{'/category/'.$shopmenu->product_type_id}}
                                            </a>
                                        @endisset
                                    </td>
                                    <td>{{$smenu->product_type_name}}</td>
                                    <td>@if($smenu->active_status == 1) BẬT @else TẮT @endif </td>
                                    <td>
                                        @if ($smenu->deleted_at!=null)
                                            <a class="btn btn-outline-info" href="javascript:void(0)" onclick="form_action('view', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-outline-success" href="javascript:void(0)" onclick="form_action('recovered', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-reload"></i>
                                            </a>
                                            <a class="btn btn-outline-danger" href="javascript:void(0)" onclick="form_action('delete', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-close"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="form_action('view', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-info"></i>
                                            </a>
                                            <a class="btn btn-warning" href="javascript:void(0)" onclick="form_action('edit', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="form_action('destroy', {{$smenu->shop_menu_id}});" role="button">
                                                <i class="ti-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $ShopMenus->appends([
                    'shop_menu_name' => Request::get('shop_menu_name'),
                    'shop_menu_position' => Request::get('shop_menu_position'),
                    'active_status' => Request::get('active_status'),
                    'view_all' => Request::get('view_all'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="shop_menu_id" value="">
            </div>
        </form>

    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, shop_menu_id) {
           let frm = document.form1;

           if(mode == 'destroy'){
               var cf = confirm('Bạn có chắc chắn muốn xóa đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/shopmenu/destroy')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'delete'){
               var cf = confirm('Bạn có chắc chắn muốn xóa vĩnh viễn đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/shopmenu/delete')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'recovered'){
               frm.action = "{{url('/admins/shopmenu/recovered')}}";
               frm.method = 'get';
           }else if(mode == 'edit'){
               frm.action = "{{url('/admins/shopmenu/edit')}}";
               frm.method = 'get';
           }else if(mode == 'view'){
               frm.action = "{{url('/admins/shopmenu/show')}}";
               frm.method = 'get';
           }

            frm.shop_menu_id.value = shop_menu_id
            frm.submit();
        }
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        $("#ckTitle").click(function () {
            var $checkbox = $('input[name=view_all]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
        });
        initialSetup();


    </script>
@stop


