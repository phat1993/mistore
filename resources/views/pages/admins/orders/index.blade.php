@extends('layouts.admin')
@section('title', 'Đơn Hàng')

@section('styles')
    <style>
        input[type=checkbox] {
            transform: scale(1.8);
        }
    </style>
@endsection
@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" active>Đơn Hàng</li>
    </ol>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Danh sách Đơn Hàng </span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <!-- Notifications Set  -->
        @if(session('success'))
            <div class="col-md-12" id="notificationCRUD">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        <div class="alert alert-success" >{{session('success')}}</div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow" id="notificationCRUD">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <!-- /Notifications Set  -->

        <!-- Seach Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow min-h100">
                <form action="{{url('/admins/order')}}" method="get">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row align-items-center">
                            <div class="col-sm-1">
                                <input type="text" class="form-control" name="order_code" value="{{Request::get('order_code')}}" placeholder="Mã đơn hàng">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name='order_state_id'>
                                    <option value="" {{Request::get('order_state_id') == 0 ? 'selected': ''}}>Trạng Thái</option>
                                    @forelse ($states as $item)
                                        <option value="{{ $item->order_state_id  }}" {{Request::get('order_state_id') == $item->order_state_id ? 'selected': ''}}>{{ $item->order_state_value }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="date" title="Khoảng Thời Gian" class="form-control" name="order_date" value="{{Request::get('order_date')}}" placeholder="Khoảng Thời Gian">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="order_full_name" value="{{Request::get('order_full_name')}}" placeholder="Khách Hàng">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="order_phone" value="{{Request::get('order_phone')}}" placeholder="Điện Thoại">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="order_amount" value="{{Request::get('order_amount')}}" data-type='currency' placeholder="Tổng Tiền">
                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Seach Set  -->
        <!-- Content Set  -->
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <table class="table table-hover table-data" id="banner-table">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>MÃ ĐƠN HÀNG</th>
                            <th>TRẠNG THÁI</th>
                            <th>THỜI GIAN ĐẶT</th>
                            <th>KHÁCH HÀNG</th>
                            <th>ĐIỆN THOẠI</th>
                            <th>TỔNG TIỀN(VNĐ)</th>
                            <th>XỬ LÝ</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $idx = $perPage*($currentPage-1); ?>
                            {{-- @if(isset($HouseCategories)||$HouseCategories->count()>0) --}}
                            @forelse ($ShopOrders as $shopOrder)
                                <tr>
                                    <?php $idx++; ?>
                                    <td>{{$idx}}</td>
                                    <td>{{ $shopOrder->order_id }}</td>
                                    {{-- <td>{{Str::padRight('MHD',9-strlen($shopOrder->order_id),0).$shopOrder->order_id}}</td> --}}
                                    <td>
                                        <span class="@switch($shopOrder->order_state_id)
                                            @case(1)
                                                text-warning
                                                @break
                                            @case(2) @case(3)
                                                text-info
                                                @break
                                            @case(4)
                                                text-success
                                                @break
                                            @case(5) @case(6) @case(7)
                                                text-danger
                                                @break
                                            @default
                                        @endswitch" >
                                            {{$shopOrder->order_state_value}}
                                        </span>
                                    </td>
                                    <td>
                                        <span>{{date('H:i:s', strtotime($shopOrder->created_at))}}</span>
                                        <span>{{date('d-m-Y', strtotime($shopOrder->created_at))}}</span>
                                    </td>
                                    <td>{{$shopOrder->order_full_name}}</td>
                                    <td>{{$shopOrder->order_phone}}</td>
                                    <td>{{ number_format($shopOrder->order_amount,0,'',',') }}</td>
                                    <td>
                                        <a class="btn btn-outline-info" href="javascript:void(0)" title="Thông Tin" onclick="form_action('view', {{$shopOrder->order_id}});" role="button">
                                            <i class="ti-info"></i>
                                        </a>
                                        @if ($shopOrder->order_state_id == 1 || $shopOrder->order_state_id==2 || $shopOrder->order_state_id==3)
                                            @if ($shopOrder->order_state_id<2)
                                                <a class="btn btn-info" href="javascript:void(0)" title="Lấy Hàng" onclick="form_action1('getPro', {{$shopOrder->order_id}}, 2);" role="button">
                                                    <i class="ti-archive"></i>
                                                </a>
                                            @endif
                                            @if ($shopOrder->order_state_id<3)
                                                <a class="btn btn-info" href="javascript:void(0)" title="Lấy Hàng" onclick="form_action1('sendPro', {{$shopOrder->order_id}}, 3);" role="button">
                                                    <i class="ti-truck"></i>
                                                </a>
                                            @endif
                                            <a class="btn btn-success" href="javascript:void(0)" title="Đã Giao Hàng" onclick="form_action1('successPro', {{$shopOrder->order_id}}, 4);" role="button">
                                                <i class="ti-check-box"></i>
                                            </a>
                                            <a class="btn btn-danger" href="javascript:void(0)" title="Trả Hàng" onclick="form_action1('successPro', {{$shopOrder->order_id}}, 6);" role="button">
                                                <i class="fa fa-reply"></i>
                                            </a>
                                            <a class="btn btn-danger" href="javascript:void(0)" title="Hết Hàng" onclick="form_action1('limitPro', {{$shopOrder->order_id}}, 7);" role="button">
                                                <i class="ti-close"></i>
                                            </a>
                                        @endif
                                        @if ($shopOrder->order_state_id == 5 && $shopOrder->customer_id == null)
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="form_action('destroy', {{$shopOrder->order_id}});" role="button">
                                                <i class="ti-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                </tr>
                            @endforelse
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <!-- /Content Set  -->

        <div class="col-md-12">
            <div class="lorvens-widget">
                {{ $ShopOrders->appends([
                    'order_code' => Request::get('order_code'),
                    'order_state_id' => Request::get('order_state_id'),
                    'order_date' => Request::get('order_date'),
                    'order_full_name' => Request::get('order_full_name'),
                    'order_phone' => Request::get('order_phone'),
                    'order_amount' => Request::get('order_amount'),
                    ])->links('admins.pagination.default') }}
            </div>
        </div>

        <form name="form1" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="order_id" value="">
            </div>
        </form>

        <form name="form2" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="order_id" value="">
                <input type="hidden" class="form-control" name="order_state_id" value="">
            </div>
        </form>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')
    <script>
        function form_action (mode, order_id) {
           let frm = document.form1;

           if(mode == 'destroy'){
               var cf = confirm('Bạn có chắc chắn muốn xóa đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/order/destroy')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'delete'){
               var cf = confirm('Bạn có chắc chắn muốn xóa vĩnh viễn đối tượng này!')
               if(cf == true)
               {
                frm.action = "{{url('/admins/order/delete')}}";
                frm.method = 'post';
               }else return;
           }else if(mode == 'recovered'){
               frm.action = "{{url('/admins/order/recovered')}}";
               frm.method = 'get';
           }else if(mode == 'edit'){
               frm.action = "{{url('/admins/order/edit')}}";
               frm.method = 'get';
           }else if(mode == 'view'){
               frm.action = "{{url('/admins/order/show')}}";
               frm.method = 'get';
           }

            frm.order_id.value = order_id
            frm.submit();
        }
        function form_action1 (mode, order_id, order_state_id) {
           let frm = document.form2;
            frm.action = "{{url('/admins/order/update')}}";
            frm.method = 'post';
            if(mode == 'limitPro'){
                var cf = confirm('Bạn đã gọi điện cho khách hàng thông báo! \nXác nhận hết hàng!')
                if(!cf)return;
            }
            frm.order_id.value = order_id
            frm.order_state_id.value = order_state_id
            frm.submit();
        }



        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        $("#ckTitle").click(function () {
            var $checkbox = $('input[name=view_all]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
        });
        initialSetup();

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });

        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        var original_len = input_val.length;

        // initial caret position
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // // get position of first decimal
            // // this prevents multiple decimals from
            // // being entered
            // var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            // var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // // validate right side
            // right_side = formatNumber(right_side);

            // // On blur make sure 2 numbers after decimal
            // if (blur === "blur") {
            // right_side += "00";
            // }

            // // Limit decimal to only 2 digits
            // right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // // final formatting
            // if (blur === "blur") {
            // input_val += ".00";
            // }
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        }


    </script>
@stop


