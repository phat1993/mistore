@extends('layouts.admin')

@section('title', 'Đơn Hàng - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/order">Đơn Hàng</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Đơn Hàng <span class="text-primary">{{Str::padRight('MHD',9-strlen($shopOrder->order_id),0).$shopOrder->order_id}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người Cập Nhật</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget-area lorvens-box-shadow color-red">
                @switch($shopOrder->order_state_id)
                    @case(1)
                        <div class="widget-left text-warning w-25">
                            <i class="ti-alert"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-warning" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                            @break
                    @case(2)
                        <div class="widget-left text-info w-25">
                            <i class="ti-archive"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-info" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @case(3)
                        <div class="widget-left text-info w-25">
                            <i class="ti-truck"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-info" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @case(4)
                        <div class="widget-left text-success w-25">
                            <i class="ti-check-box"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-success" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @case(5)
                        <div class="widget-left text-danger w-25">
                            <i class="fa fa-user-times"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-danger" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @case(6)
                        <div class="widget-left text-danger w-25">
                            <i class="fa fa-reply"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-danger" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @case(7)
                        <div class="widget-left text-danger w-25">
                            <i class="ti-close"></i>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">Trạng Thái</h4>
                            <span class="text-danger" >
                                {{$shopOrder->order_state_value}}
                            </span>
                        </div>
                        @break
                    @default
                @endswitch
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-time"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Đặt Hàng</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($shopOrder->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($shopOrder->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @if ($shopOrder->created_at!=$shopOrder->updated_at)
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật Trạng Thái</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($shopOrder->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($shopOrder->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông Tin Khách Hàng</h3>
                <div class="table-div">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col" width="30%">HỌ TÊN</th>
                            <td colspan="4">{{$shopOrder->order_full_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">SỐ ĐIỆN THOẠI</th>
                            <td colspan="4">{{$shopOrder->order_phone}}</td>
                        </tr>
                        <tr>
                            <th scope="col">EMAIL</th>
                            <td colspan="4">{{$shopOrder->order_email}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐỊA CHỈ</th>
                            <td colspan="4">{{$shopOrder->order_address}}</td>
                        </tr>
                        <tr>
                            <th scope="col">THỜI GIAN CÓ THỂ NHẬN HÀNG</th>
                            <td colspan="4">{{$shopOrder->order_delivery_time}}</td>
                        </tr>
                        <tr>
                            <th scope="col">GHI CHÚ</th>
                            <td colspan="4">{{$shopOrder->order_note}}</td>
                        </tr>
                        <tr>
                            <th scope="col">MÃ/SĐT KHUYẾN MÃI</th>
                            <td>
                                <span>Hiện Tại:</span>
                                <a href="javascript:void(0)" class="text-danger font-weight-bold" onclick="form_action('viewGift', {{$shopOrder->gift_id}});" role="button">{{$shopOrder->gift_code}}</a>
                                <td class="text-danger">{{number_format($shopOrder->gift_discount,0,'',',')}}<sup>đ</sup></td>
                            </td>
                            <td>
                                <span>Khách Hàng Nhập:</span>
                                <a href="javascript:void(0)" class="text-danger font-weight-bold" onclick="form_action('viewGift', {{$shopOrder->gift_id}});" role="button">{{$shopOrder->order_gift}}</a>
                                <td class="text-danger">{{number_format($shopOrder->order_gift_discount,0,'',',')}}<sup>đ</sup></td>
                            </td>
                        </tr>
                        <tr></tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="widget-title d-flex align-items-center">
                    <h3 class="mr-2">Thông Tin Đơn Hàng</h3>
                    <a href="javascript:void(0)" title="In Hóa Đơn" class="btn btn-outline-dark mr-1" onclick="show_alert();"><i class="ti-printer"></i></a>

                    @if ($shopOrder->order_state_id == 1 || $shopOrder->order_state_id==2 || $shopOrder->order_state_id==3)
                        @if ($shopOrder->order_state_id<2)
                            <a class="btn btn-info mr-1" href="javascript:void(0)" title="Lấy Hàng" onclick="form_action1('getPro', {{$shopOrder->order_id}}, 2);" role="button">
                                <i class="ti-archive"></i>
                            </a>
                        @endif
                        @if ($shopOrder->order_state_id<3)
                            <a class="btn btn-info mr-1" href="javascript:void(0)" title="Lấy Hàng" onclick="form_action1('sendPro', {{$shopOrder->order_id}}, 3);" role="button">
                                <i class="ti-truck"></i>
                            </a>
                        @endif
                        <a class="btn btn-success mr-1" href="javascript:void(0)" title="Đã Giao Hàng" onclick="form_action1('successPro', {{$shopOrder->order_id}}, 4);" role="button">
                            <i class="ti-check-box"></i>
                        </a>
                        <a class="btn btn-danger mr-1" href="javascript:void(0)" title="Trả Hàng" onclick="form_action1('successPro', {{$shopOrder->order_id}}, 6);" role="button">
                            <i class="fa fa-reply"></i>
                        </a>
                        <a class="btn btn-danger mr-1" href="javascript:void(0)" title="Hết Hàng" onclick="form_action1('limitPro', {{$shopOrder->order_id}}, 7);" role="button">
                            <i class="ti-close"></i>
                        </a>
                    @endif
                </div>
                <div class="table-div">
                    <div class="table-div">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-hover">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>STT</th>
                                        <th colspan="2">SẢN PHẨM</th>
                                        <th class="text-right">SỐ LƯỢNG</th>
                                        <th class="text-right">GIÁ TIỀN</th>
                                        <th class="text-right">GIẢM GIÁ</th>
                                        <th class="text-right">TỔNG TIỀN(VNĐ)</th>
                                        <th class="text-right">THÀNH TIỀN(VNĐ)</th>
                                        <th style="width:60px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $idx = 0; ?>
                                        @forelse ($details as $dt)
                                            <tr>
                                                <?php $idx++; ?>
                                                <td>{{$idx}}</td>
                                                <td width="5%">
                                                    <img style="object-fit: cover;width:100%;"
                                                    src="{{asset('storage/'.$dt->product_img_1)}}"
                                                    alt="image"></td>
                                                <td>{{$dt->product_name}}</td>
                                                <td class="text-right">{{$dt->order_detail_quantity}}</td>
                                                <td class="text-right">{{ number_format($dt->product_price,0,'',',') }}</td>
                                                <td class="text-right">{{ number_format($dt->order_detail_discount,0,'',',') }}</td>
                                                <td class="text-right">{{ number_format($dt->order_detail_total,0,'',',') }}</td>
                                                <td class="text-right">{{ number_format($dt->order_detail_amount,0,'',',') }}</td>
                                                <td class="text-right">
                                                    <a class="btn btn-outline-info" href="javascript:void(0)" onclick="form_action('viewPro', {{$dt->product_id}});" role="button">
                                                        <i class="ti-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="10" class="text-center"><h1>Không tìm thấy dữ liệu!</h1></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4 ml-auto">
                                <table class="table table-sm">
                                    <tbody>
                                        <tr>
                                            <td class="text-right">Tổng Tiền</td>
                                            <td colspan="2" class="text-right font-weight-bold">{{ number_format($shopOrder->order_total,0,'',',') }}</td>
                                        </tr>
                                    @isset($shopOrder->gift_id)
                                    <tr>
                                            <td class="text-right">Gift</td>
                                            <td class="text-right"><a  href="javascript:void(0)" onclick="form_action('viewGift', {{$shopOrder->gift_id}});" role="button" class="text-danger">{{ $shopOrder->order_gift }}</a></td>
                                            <td class="text-right">{{ number_format($shopOrder->order_gift_discount,0,'',',') }}</td>
                                        </tr>
                                    @endisset
                                        <tr>
                                            <td class="text-right">Khuyến Mãi</td>
                                            <td colspan="2" class="text-right">{{ number_format($shopOrder->order_discount,0,'',',') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">Phí Ship</td>
                                            <td colspan="2" class="text-right">{{ number_format($shopOrder->order_ship_fee,0,'',',') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">Thuế</td>
                                            <td colspan="2" class="text-right">{{ number_format($shopOrder->order_tax,0,'',',') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold">Thành Tiền</td>
                                            <td colspan="2" class="text-right font-weight-bold">{{ number_format($shopOrder->order_amount,0,'',',') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow text-center pb-5">
                <a href="/admins/order" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

        <form name="form" action="" method="" target="_blank">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="product_id" value="">
                <input type="hidden" class="form-control" name="gift_id" value="">
            </div>
        </form>

        <form name="form2" action="" method="">
            {{csrf_field()}}
            <div class="form-group ">
                <input type="hidden" class="form-control" name="order_id" value="">
                <input type="hidden" class="form-control" name="order_state_id" value="">
            </div>
        </form>
    </div>
    <!-- /Main Content -->
@stop



@section('javascript')
    <script>
        function form_action (mode, id) {
           let frm = document.form;
            if(mode == 'viewPro'){
               frm.action = "{{url('/admins/product/show')}}";
               frm.method = 'get';
            frm.product_id.value = id
           }else if(mode == 'viewGift'){
               frm.action = "{{url('/admins/shopgift/show')}}";
               frm.method = 'get';
            frm.gift_id.value = id
           }

            frm.submit();
        }

        function form_action1 (mode, order_id, order_state_id) {
           let frm = document.form2;
            frm.action = "{{url('/admins/order/update')}}";
            frm.method = 'post';
            if(mode == 'limitPro'){
                var cf = confirm('Bạn đã gọi điện cho khách hàng thông báo! \nXác nhận hết hàng!')
                if(!cf)return;
            }
            frm.order_id.value = order_id
            frm.order_state_id.value = order_state_id
            frm.submit();
        }



        function show_alert(){
            alert("Đang phát triển");
        }

    </script>
@stop
