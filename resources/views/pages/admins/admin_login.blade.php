@extends('layouts.admin_authentication')
@section('content')
    <div id="content" style="margin-top:10%">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 auth-box">
                    <div class="lorvens-box-shadow">
                        <h3 class="widget-title">Đăng Nhập</h3>
                        <form name="admin_login_form" method="POSt" action="{{URL('/admins')}}" class="widget-form">
                            {{csrf_field()}}
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input name="user_name" placeholder="Tên đăng nhập" value="trung001" class="form-control" required="" data-validation="length alphanumeric" data-validation-length="3-12"
                                           data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" data-validation-has-keyup-event="true">
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="password" placeholder="Mật khẩu" name="password" value="123" class="form-control" data-validation="strength" data-validation-strength="2"
                                           data-validation-has-keyup-event="true">
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- Check Box -->
                            <div class="form-check row">
                                <div class="col-sm-12 text-left">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="ex-check-2">
                                        <label class="custom-control-label" for="ex-check-2">Ghi nhớ</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /Check Box -->
                            <!-- Login Button -->
                            <div class="button-btn-block">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Đăng nhập</button>
                            </div>
                            <!-- /Login Button -->
                            <!-- Links -->
                            <div class="auth-footer-text">
                                @if(count($errors) > 0)
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger" id="notificationCRUD">{{$error}}</div>
                                    @endforeach
                                @endif
                            </div>
                            <!-- /Links -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        initialSetup();
    </script>
@stop
