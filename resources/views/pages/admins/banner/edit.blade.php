@extends('layouts.admin')


@section('title', 'Băng Rôn - Cập Nhật')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/banner">Băng Rôn</a></li>
        <li class="breadcrumb-item" active>Cập Nhật</li>
    </ol>
@endsection


@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Cập Nhật Băng rôn</h3>
        </div>
    </div>
    <!-- /Page Title -->

    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <h3 class="widget-title">Thông báo</h3>
                <div class="lorvens-widget">
                    <div class="alert alert-success" >{{session('success')}}</div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="widget-area-2 lorvens-box-shadow">
                    <h3 class="widget-title">Thông báo</h3>
                    <div class="lorvens-widget">
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger" >{{$error}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <!-- Form Item -->
                <div class="lorvens-widget">
                    <form action="{{url('/admins/banner/update')}}" method="POST" enctype="multipart/form-data" name="create-banner">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="banner_id" value="{{Request::get('banner_id')}}">
                            <input type="hidden" class="form-control" name="banner_img" value="{{$banner->banner_img}}">
                            <input type="hidden" class="form-control" name="banner_img_clear" value="{{$banner->banner_img}}">
                        </div>
                        <div class="form-group">
                            <label>Tiêu Đề<span class="text-danger">(*)</span>: </label>
                            <input type="text" placeholder="Nhập vào tiêu đề" class="form-control" name="banner_title" value="{{ $banner->banner_title }}">
                        </div>
                        <div class="form-group">
                            <label>Đường dẫn<span class="text-danger">(/*)</span>: </label>
                            <input type="text" placeholder="Nhập vào đường dẫn của băng rôn" class="form-control" name="banner_url" value="{{ $banner->banner_url }}">
                        </div>
                        <div class="form-group" id="enterLink">
                            <label>Đường dẫn Hình Ảnh<span class="text-danger">(/*)</span>: </label>
                            <input type="text" placeholder="Nhập vào đường dẫn" class="form-control" name="banner_link" value="{{ $banner->banner_link }}">
                        </div>
                        <div class="form-group" id="chooseLink">
                            <label>Hình Ảnh<span class="text-danger">(/*)</span>: </label>
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image" id="customFile">
                                        <label class="custom-file-label" for="customFile">{{ $banner->banner_img==''?'Chọn hình ảnh':$banner->banner_img }}</label>
                                    </div>
                                    {{-- <input type="file" name="image" id="customFile"> --}}
                                </div>
                                <div class="col-sm-1">
                                    <a href="javascript:void(0)" id="clearImage" class="btn btn-default">Xóa</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="img-thumbnail" style="width: 810px;height:510px">

                            @if ($banner->banner_img!='')
                                <img  id="loadImage" style="object-fit: cover;width: 800px;height:500px"
                                src="{{asset('storage/'.$banner->banner_img)}}"alt="Image">
                            @else
                                <img  id="loadImage" style="object-fit: cover;width: 800px;height:500px"
                                src="{{$banner->banner_link}}"alt="Image">
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Chi Tiết:</label>
                            <textarea class="form-control" placeholder="Nhập vào chi tiết" name="banner_description" rows="4" cols="50">{{ $banner->banner_description }}</textarea>
                        </div>


                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" id="active_status"  name="active_status"{{ $banner->active_status==1?'checked':'' }} >
                                <label class="custom-control-label" for="active_status" >HOẠT ĐỘNG</label>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Cập Nhật"/>
                        <a href="/admins/banner" class="btn btn-lg btn-outline-light">Quay Lại</a>

                    </form>
                </div>
                <!-- /Form Item -->
            </div>        </div>
    </div>
    <!-- /Main Content -->
@stop

@section('javascript')

<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#loadImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#clearImage').click(function() {
        $("input[name='image']").val('');
        $("input[name='banner_img_clear']").val('');
        $('#customFile').next('label').html('Chọn hình ảnh');
        $('#enterLink').show();
        $('#loadImage').attr('src', "{{asset('storage/images/no-image.jpg')}}");
    });

    $("input[name='image']").change(function() {
        readURL(this);
        if($(this).val() != "") {
            $('#enterLink').hide();
        }
    });

    $("input[name='banner_link']").keyup(function() {
        if($(this).val() != "") {
            $('#loadImage').attr('src', $(this).val());
            $('#chooseLink').hide();
        } else {
            $('#chooseLink').show();
            $('#loadImage').attr('src', "{{asset('storage/images/no-image.jpg')}}");
        }
    });
</script>
<script src={{ url('/asset/plugins/ckeditor/ckeditor.js') }}></script>
<script>
    CKEDITOR.replace( 'banner_description' );
</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        $( "form[name=create-banner]" ).validate( {
            rules: {
                banner_title: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                banner_title: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@endsection
