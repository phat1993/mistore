@extends('layouts.admin')

@section('title', 'Băng Rôn - Chi tiết')

@section('topnavigation')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admins/dashboard">
                <span class="ti-home"></span>
            </a>
        </li>
        <li class="breadcrumb-item" >
            <a href="/admins/banner">Băng Rôn</a></li>
        <li class="breadcrumb-item" active>Chi tiết</li>
    </ol>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="block-title">Chi tiết Băng Rôn <span class="text-primary">{{$banner->banner_title}}</span></h3>
        </div>
    </div>
    <!-- /Page Title -->
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <img src="{{ asset('storage/'.$admin->admin_avatar)}}" width="75" height="75" alt="" class="rounded-circle">
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Người {{ $banner->deleted_at!=null?'Xóa':'Cập Nhật' }}</h4>
                    <span class="color-red">{{$admin->admin_abbreviation}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="widget-area lorvens-box-shadow color-red">
                @if($banner->active_status == 1)
                    <div class="widget-left">
                        <i class="ti-unlock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">BẬT</span>
                    </div>
                @else
                    <div class="widget-left">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Hoạt Động</h4>
                        <span class="color-red">TẮT</span>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-calendar"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Tạo</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($banner->created_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($banner->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @isset ($banner->deleted_at)
        <div class="col-md-3">
            <div class="widget-area lorvens-box-shadow color-red">
                <div class="widget-left">
                    <span class="ti-trash"></span>
                </div>
                <div class="widget-right">
                    <h4 class="wiget-title">Ngày Xóa</h4>
                    <div>
                        <span class="color-blue">{{date('H:i:s', strtotime($banner->deleted_at))}}</span>
                        <span class="color-red">{{date('d-m-Y', strtotime($banner->deleted_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div class="widget-area lorvens-box-shadow color-red">
                    <div class="widget-left">
                        <span class="ti-reload"></span>
                    </div>
                    <div class="widget-right">
                        <h4 class="wiget-title">Ngày Cập Nhật</h4>
                        <div>
                            <span class="color-blue">{{date('H:i:s', strtotime($banner->updated_at))}}</span>
                            <span class="color-red">{{date('d-m-Y', strtotime($banner->updated_at))}}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        <div class="col-md-12">
            <div class="widget-area-2 lorvens-box-shadow">
                <div class="table-div">
                    <h3 class="widget-title">Thông Tin</h3>
                    <table class="table table-bordered">
                        <tr>
                            <th scope="col">TIÊU ĐỀ</th>
                            <td>{{$banner->banner_title}}</td>
                        </tr>
                        <tr>
                            <th scope="col">ĐƯỜNG DẪN</th>
                            <td><a href="{{ url($banner->banner_url) }}" class="text-primary">{{url($banner->banner_url)}}</a></td>
                        </tr>
                        <tr>
                            <th scope="col">MÔ TẢ</th>
                            <td>{{$banner->banner_description}}</td>
                        </tr>
                        <tr>
                            <th scope="col">HÌNH ẢNH</th>
                            <td>
                                @if ($banner->banner_link!='')
                                    <img style="object-fit: cover;width:100%;"
                                    src="{{url($banner->banner_link)}}"
                                    alt="{{$banner->banner_title}}">
                                @else
                                    <img style="object-fit: cover;width:100%;"
                                    src="{{asset('storage/'.$banner->banner_img)}}"
                                    alt="{{$banner->banner_title}}">
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <a href="/admins/banner" class="btn btn-lg btn-outline-dark">Quay Lại</a>
            </div>
        </div>

    </div>
    <!-- /Main Content -->
@stop


