@extends('layouts.default')

@section('title', 'Túi Xách Chính Hãng')
@section('styles')
    <meta name="keywords" content="hang chinh hang, hàng chính hãng, tui xach, túi xách">
    <meta name="description" content="MIKSTORE là nơi chỉ cung cấp những hàng chính hãng tốt nhất, là lựa chọn tuyệt vời cho khách hàng..">
    <link rel="canonical" href="{{ url('') }}">
@endsection
@section('content')
@include('clients.includes.parts.banner')
<div style="display: block">
    @isset($lsDiscount)
    <div>
        <div class="col-sale-owl-carousel">
            <div class="div-display-carousel-home">
                <div style="width: 70%">
                    <h2 >SẢN PHẨM KHUYẾN MÃI</h2>
                </div>
                <div style="width: 30%;text-align: right;align-self: flex-end;">
                    <a href="{{ url('/noi-bat/san-pham-khuyen-mai') }}"><span class="txt-color">Xem tất cả >></span></a>
                </div>
            </div>
            <div class="owl-carousel owl-carousel-sale">
                @foreach ($lsDiscount as $item)
                    <div class="cover-new-carousel">
                        <div class="cover-item-carousel" >
                            <a href="{{ url($item->url_seo) }}">
                                <img src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2 " />
                            </a>
                            <div class="color-item-tag">
                                <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button" class="data-image-small">
                                    <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                                <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});"  class="data-image-small">
                                    <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                            </div>
                        </div>
                        <div class="mt-3 pl-3">
                            <a href="{{ url($item->url_seo) }}" style="color: #6c757d; text-decoration: none;">
                                <h3 class="name-item-card">{{ $item->product_name }}</h3>
                            </a>
                            <div class="cover-text-money">
                                <span class="text-red-money20">
                                    @if($item->product_discount!=-1)
                                        {{ number_format($item->product_discount,0,'',',') }}
                                        <span class="fontsize-13">đ</span>
                                    @else
                                        {{ number_format($item->product_price,0,'',',') }}
                                        <span class="fontsize-13">đ</span>
                                    @endif
                                </span>
                                @if($item->product_discount>-1)
                                    <span class="text-money-del ml-2">
                                        {{ number_format($item->product_price,0,'',',') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endisset
    {{-- <div>
        <a href="tui-xach/handbag-hb0113.html">
                <img src=https://cdn.sablanca.vn/BlogManagerImage/blogmanager_030220211249405497.jpg class="img-banner-4" alt="Hinh lon" />
        </a>

    </div> --}}

    @isset($lsUpdate)
    <div >
        <div class="col-sale-owl-carousel">
            <div class="div-display-carousel-home">
                <div style="width: 70%">
                    <h2 style="color: #6c757d">SẢN PHẨM MỚI NHẤT</h2>
                </div>
                <div style="width: 30%;text-align: right;align-self: flex-end;">
                    <a href="{{ url('/noi-bat/san-pham-moi-nhat') }}"><span class="txt-color">Xem tất cả >></span></a>
                </div>
            </div>
            <div class="owl-carousel owl-carousel-sale">
                @foreach ($lsUpdate as $item)
                <div class="cover-new-carousel">
                    <div class="cover-item-carousel" >
                        <a href="{{ url($item->url_seo) }}">
                            <img src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2 " />
                        </a>
                        <div class="color-item-tag">
                            <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                            <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                        </div>
                    </div>
                    <div class="mt-3 pl-3">
                        <a href="{{ url($item->url_seo) }}" style="color: #6c757d; text-decoration: none;">
                            <h3 class="name-item-card">{{ $item->product_name }}</h3>
                        </a>
                        <div class="cover-text-money">
                            <span class="text-red-money20">
                                @if($item->product_discount!=-1)
                                    {{ number_format($item->product_discount,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @else
                                    {{ number_format($item->product_price,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @endif
                            </span>
                            @if($item->product_discount>-1)
                                <span class="text-money-del ml-2">
                                    {{ number_format($item->product_price,0,'',',') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    @endisset

    @isset($lsRandom)
    <div >
        <div class="col-sale-owl-carousel">
            <div class="div-display-carousel-home">
                <div style="width: 70%">
                    <h2 style="color: #6c757d">GỢI Ý CHO BẠN</h2>
                </div>
                <div style="width: 30%;text-align: right;align-self: flex-end;">
                    <a href="{{ url('/noi-bat/san-pham-goi-y') }}"><span class="txt-color">Xem tất cả >></span></a>
                </div>
            </div>
            <div class="owl-carousel owl-carousel-sale">
                @foreach ($lsRandom as $item)
                <div class="cover-new-carousel">
                    <div class="cover-item-carousel" >
                        <a href="{{ url($item->url_seo) }}">
                            <img src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2" />
                        </a>
                        <div class="color-item-tag">
                            <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                            <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                        </div>
                    </div>
                    <div class="mt-3 pl-3">
                        <a href="{{ url($item->url_seo) }}" style="color: #6c757d; text-decoration: none;">
                            <h3 class="name-item-card">{{ $item->product_name }}</h3>
                        </a>
                        <div class="cover-text-money">
                            <span class="text-red-money20">
                                @if($item->product_discount!=-1)
                                    {{ number_format($item->product_discount,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @else
                                    {{ number_format($item->product_price,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @endif
                            </span>
                            @if($item->product_discount>-1)
                                <span class="text-money-del ml-2">
                                    {{ number_format($item->product_price,0,'',',') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    @endisset
    {{-- <div>
        <a href="tui-xach/tote-to0051.html">
                <img src=https://cdn.sablanca.vn/BlogManagerImage/blogmanager_030220212941157857.jpg class="img-banner-4" alt="Hinh lon" />
        </a>

    </div> --}}

    @isset($lsHot)


    <div >
        <div class="col-sale-owl-carousel">
            <div class="div-display-carousel-home">
                <div style="width: 70%">
                    <h2 style="color: #6c757d">SẢN PHẨM NỔI BẬT</h2>
                </div>
                <div style="width: 30%;text-align: right;align-self: flex-end;">
                    <a href="{{ url('/noi-bat/san-pham-duoc-ban-nhieu-nhat') }}"><span class="txt-color">Xem tất cả >></span></a>
                </div>
            </div>
            <div class="owl-carousel owl-carousel-sale">
                @foreach ($lsHot as $item)
                <div class="cover-new-carousel">
                    <div class="cover-item-carousel" >
                        <a href="{{ url($item->url_seo) }}">
                            <img src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2" />
                        </a>
                        <div class="color-item-tag">
                            <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                            <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                        </div>
                    </div>
                    <div class="mt-3 pl-3">
                        <a href="{{ url($item->url_seo) }}" style="color: #6c757d; text-decoration: none;">
                            <h3 class="name-item-card">{{ $item->product_name }}</h3>
                        </a>
                        <div class="cover-text-money">
                            <span class="text-red-money20">
                                @if($item->product_discount!=-1)
                                    {{ number_format($item->product_discount,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @else
                                    {{ number_format($item->product_price,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @endif
                            </span>
                            @if($item->product_discount>-1)
                                <span class="text-money-del ml-2">
                                    {{ number_format($item->product_price,0,'',',') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>

    @endisset


    @isset($lsSearch)
    <div >
        <div class="col-sale-owl-carousel">
            <div class="div-display-carousel-home">
                <div style="width: 70%">
                    <h2 style="color: #6c757d">SẢN PHẨM TÌM KIẾM HÀNG ĐẦU</h2>
                </div>
                <div style="width: 30%;text-align: right;align-self: flex-end;">
                    <a href="{{ url('/noi-bat/san-pham-duoc-tim-kiem-nhieu-nhat') }}"><span class="txt-color">Xem tất cả >></span></a>
                </div>
            </div>
            <div class="owl-carousel owl-carousel-sale">
                @foreach ($lsSearch as $item)
                <div class="cover-new-carousel">
                    <div class="cover-item-carousel" >
                        <a href="{{ url($item->url_seo) }}">
                            <img src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2" />
                        </a>
                        <div class="color-item-tag">
                            <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button"  class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                            <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});" class="data-image-small">
                                <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                        </div>
                    </div>
                    <div class="mt-3 pl-3">
                        <a href="{{ url($item->url_seo) }}" style="color: #6c757d; text-decoration: none;">
                            <h3 class="name-item-card">{{ $item->product_name }}</h3>
                        </a>
                        <div class="cover-text-money">
                            <span class="text-red-money20">
                                @if($item->product_discount!=-1)
                                    {{ number_format($item->product_discount,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @else
                                    {{ number_format($item->product_price,0,'',',') }}
                                    <span class="fontsize-13">đ</span>
                                @endif
                            </span>
                            @if($item->product_discount>-1)
                                <span class="text-money-del ml-2">
                                    {{ number_format($item->product_price,0,'',',') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>

    @endisset

</div>

<form name="form1" action="" method="">
    {{csrf_field()}}
    <div class="form-group ">
        <input type="hidden" class="form-control" name="product_id" value="">
    </div>
</form>

</div>
<!-- /Main Content -->
@endsection

@section('javascript')
<script>
function form_action (mode, product_id) {
   let frm = document.form1;

   if(mode == 'order'){
       frm.action = "{{url('/don-hang/them')}}";
       frm.method = 'post';
   }else if(mode == 'favorite'){
       frm.action = "{{url('/khach-hang/add-favorite')}}";
       frm.method = 'get';
   }

    frm.product_id.value = product_id
    frm.submit();
}

function initialSetup() {
    if (document.getElementById("notificationCRUD") != null) {
        $("#notificationCRUD").delay(3000).fadeOut('slow');
    }
}
</script>
@stop
