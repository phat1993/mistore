@extends('layouts.default')

@section('title')
   Đăng Nhập Tài Khoản
@endsection
@section('styles')
    <style>
    .login-text {
        position: absolute;
        top: 20%;
        left: 54%;
        color: white;
        font-size: 8rem;
        filter: url(#gooey);
      }
      .login-text::before, .login-text::after {
        position: absolute;
        opacity: 0.1;
        transform: translate(-80%, -80%);
        animation-duration: 12s;
        animation-timing-function: ease;
        animation-iteration-count: infinite;
        filter: blur(1px);
      }
      .login-text::before {
        content: "R";
        animation-name: items-odd;
      }
      .login-text::after {
        content: "E";
        animation-name: items-even;
        animation-delay: 1.5s;
      }

      @keyframes items-odd {
        8.3333333333% {
          opacity: 1;
        }
        12.5% {
          opacity: 1;
        }
        20.8333333333% {
          opacity: 0;
        }
        25% {
          opacity: 0;
        }
        33.3333333333% {
          opacity: 1;
        }
        37.5% {
          opacity: 1;
        }
        45.8333333333% {
          opacity: 0;
        }
        50% {
          opacity: 0;
        }
        58.3333333333% {
          opacity: 1;
        }
        62.5% {
          opacity: 1;
        }
        70.8333333333% {
          opacity: 0;
        }
        75% {
          opacity: 0;
        }
        83.3333333333% {
          opacity: 1;
        }
        87.5% {
          opacity: 1;
        }
        95.8333333333% {
          opacity: 0;
        }
        100% {
          opacity: 0;
        }
        0% {
          content: "M";
        }
        24.9999% {
          content: "M";
        }
        25% {
          content: "K";
        }
        49.9999% {
          content: "K";
        }
        50% {
          content: "T";
        }
        74.9999% {
          content: "T";
        }
        75% {
          content: "R";
        }
        99.9999% {
          content: "R";
        }
      }
      @keyframes items-even {
        8.3333333333% {
          opacity: 1;
        }
        12.5% {
          opacity: 1;
        }
        20.8333333333% {
          opacity: 0;
        }
        25% {
          opacity: 0;
        }
        33.3333333333% {
          opacity: 1;
        }
        37.5% {
          opacity: 1;
        }
        45.8333333333% {
          opacity: 0;
        }
        50% {
          opacity: 0;
        }
        58.3333333333% {
          opacity: 1;
        }
        62.5% {
          opacity: 1;
        }
        70.8333333333% {
          opacity: 0;
        }
        75% {
          opacity: 0;
        }
        83.3333333333% {
          opacity: 1;
        }
        87.5% {
          opacity: 1;
        }
        95.8333333333% {
          opacity: 0;
        }
        100% {
          opacity: 0;
        }
        0% {
          content: "I";
        }
        24.9999% {
          content: "I";
        }
        25% {
          content: "S";
        }
        49.9999% {
          content: "S";
        }
        50% {
          content: "O";
        }
        74.9999% {
          content: "O";
        }
        75% {
          content: "E";
        }
        99.9999% {
          content: "E";
        }
      }


      .card-header h1 {
            color: #fff;
            text-transform: uppercase;
            font-weight: 700;
            background: linear-gradient(to right,black 20%, white 60%);
            /* background: linear-gradient(to right,black 10%, red 50%, white 60%); */
            background-size: auto auto;
            background-clip: border-box;
            background-size: 200% auto;
            color: #fff;
            background-clip: text;
            text-fill-color: transparent;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            animation: textclip 1.5s linear infinite;
            display: inline-block;
            text-align: center;
        }



    @keyframes textclip {
        to {
            background-position: 200% center;
        }
    }


    </style>
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
            Tài Khoản
        </div>

    </div>
    <div class="container-fluid">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
                Tài Khoản
            </div>

        </div>
        <div class="display-flow-root">
            <div class="row d-flex justify-content-center my-5">
                <div class="col-12 text-center"><h1>ĐĂNG NHẬP</h1></div>
                <div class="w-75 d-md-none"><hr class="bg-danger"></div>
                <div class="w-25 d-none d-md-block"><hr class="bg-danger"></div>
                <div class="col-12"></div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 p-3">
                    <form class="card border border-dark" action="" method="" name="login-form">
                        <div class="card-header bg-dark d-flex justify-content-center">
                            {{-- <div class="login-text"></div>
                            <div style="height:90px;">
                                <svg>
                                    <filter id="gooey">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="9" result="blur"></feGaussianBlur>
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 28 -8" result="gooey"></feColorMatrix>
                                        <feComposite in="SourceGraphic" in2="gooey" operator="atop"></feComposite>
                                    </filter>
                                </svg>
                            </div> --}}
                            <h1 class="display-4 text-light font-weight-bold">MIKSTORE</h1>
                        </div>
                        <div class="card-body p-5">
                            <div class="form-group">
                                <label>Email : </label>
                                <input type="text" placeholder="Nhập vào địa chỉ email" maxlength="20" class="form-control" name="email" value="">
                            </div>
                            <div class="form-group">
                                <label>Mật Khẩu: </label>
                                <input type="password" placeholder="Nhập vào mật khẩu" maxlength="200" class="form-control" name="password" value="">
                            </div>
                            <div class="form-group">
                                <p class="card-text text-center"><small class="text-muted">Bạn quên mật khẩu? <a href="#">Lấy lại</a></small></p>
                            </div>
                            <div class="form-group d-flex justify-content-center mt-5">
                                <button type="submit" class="btn btn-dark btn-lg mr-2"><strong>Đăng Nhập</strong></button>
                                <a href="{{ url('/dang-ky') }}" class="btn btn-outline-dark btn-lg"><strong>Đăng Ký</strong></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('javascript')
<script>
    function form_action (mode, id) {
       let frm = document.form1;

       if(mode == 'add'){
           frm.action = "{{url('/don-hang/them')}}";
           frm.method = 'post';
       }else if(mode == 'minus'){
           frm.action = "{{url('/don-hang/bot')}}";
           frm.method = 'post';
       }else if(mode == 'remove'){
           frm.action = "{{url('/don-hang/xoa')}}";
           frm.method = 'post';
       }

        frm.product_id.value = id
        frm.submit();
    }

</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        $( "form[name='login-form']" ).validate( {
            rules: {
                email: {
                    required: true,
                    maxlength: 100,
                    email:true
                },
                password: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                email: {
                    email: "Hãy nhập vào địa chỉ email chính xác.",
                    required: "Hãy nhập vào Họ Tên của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                password: {
                    required: "Hãy nhập vào Họ Tên của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@stop
