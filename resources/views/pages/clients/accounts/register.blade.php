@extends('layouts.default')

@section('title')
   Đăng Nhập Tài Khoản
@endsection
@section('styles')
    <style>
      .card-header h1 {
            color: #fff;
            text-transform: uppercase;
            font-weight: 700;
            background: linear-gradient(to right,black 20%, white 60%);
            /* background: linear-gradient(to right,black 10%, red 50%, white 60%); */
            background-size: auto auto;
            background-clip: border-box;
            background-size: 200% auto;
            color: #fff;
            background-clip: text;
            text-fill-color: transparent;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            animation: textclip 1.5s linear infinite;
            display: inline-block;
            text-align: center;
        }



    @keyframes textclip {
        to {
            background-position: 200% center;
        }
    }


    </style>
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
            Tài Khoản
        </div>

    </div>
    <div class="container-fluid">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
                Tài Khoản
            </div>

        </div>
        <div class="display-flow-root">
            <div class="row d-flex justify-content-center my-5">
                <div class="col-12 text-center"><h1>ĐĂNG KÝ</h1></div>
                <div class="w-75 d-md-none"><hr class="bg-danger"></div>
                <div class="w-25 d-none d-md-block"><hr class="bg-danger"></div>
                <div class="col-12"></div>
                <div class="col-12 col-md-10 col-lg-8 p-3">
                    <form class="card border border-dark" name="register-form" action="{{ url('/dang-ky') }}" method="POST">
                        {{csrf_field()}}
                        <div class="card-header bg-dark d-flex justify-content-center">
                            {{-- <div class="login-text"></div>
                            <div style="height:90px;">
                                <svg>
                                    <filter id="gooey">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="9" result="blur"></feGaussianBlur>
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 28 -8" result="gooey"></feColorMatrix>
                                        <feComposite in="SourceGraphic" in2="gooey" operator="atop"></feComposite>
                                    </filter>
                                </svg>
                            </div> --}}
                            <h1 class="display-4 text-light font-weight-bold">MIKSTORE</h1>
                        </div>
                        <div class="card-body p-5">
                           <div class="row">

                            <!-- Notifications Set  -->
                            @if(session('success'))
                                <div class="col-md-12" id="notificationCRUD">
                                    <div class="alert alert-success" >{{session('success')}}</div>
                                </div>
                            @endif

                            @if(count($errors) > 0)
                                <div class="col-md-12" id="notificationCRUD">
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger" >{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif
                            <!-- /Notifications Set  -->

                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào địa chỉ email" maxlength="50" class="form-control" name="email" value="">
                                </div>
                                <div class="form-group">
                                    <label>Mật Khẩu <span class="text-danger">(*)</span>: </label>
                                    <input type="password" placeholder="Nhập vào mật khẩu" maxlength="16" class="form-control" name="password" value="">
                                </div>
                                <div class="form-group">
                                    <label>Xác Nhận Mật Khẩu <span class="text-danger">(*)</span>: </label>
                                    <input type="password" placeholder="Nhập lại mật khẩu một lần nữa" maxlength="16" class="form-control" name="password_confirmation" value="">
                                </div>
                           </div>
                           <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Họ Tên  <span class="text-danger">(*)</span>: </label>
                                    <input type="text" placeholder="Nhập vào họ và tên" maxlength="100" class="form-control" name="fullname" value="">
                                </div>
                                <div class="form-group">
                                    <label>Điện Thoại <span class="text-danger">(*)</span>: </label>
                                    <input type="tel" placeholder="Nhập vào số điện thoại" maxlength="20" class="form-control" name="phone" value="">
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ  <span class="text-danger">(*)</span>: </label>
                                    <textarea maxlength="200" class="form-control" name="address" value=""></textarea>
                                </div>
                           </div>
                           </div>
                            <div class="form-group d-flex justify-content-center mt-5">
                                <button type="submit" class="btn btn-dark btn-lg mr-2"><strong>Đăng Ký</strong></button>
                                <a href="{{ url('/dang-nhap') }}" class="btn btn-outline-dark btn-lg"><strong>Đăng Nhập</strong></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
<script>
    function initialSetup() {
        if (document.getElementById("notificationCRUD") != null) {
            $("#notificationCRUD").delay(8000).fadeOut('slow');
        }
    }
    initialSetup();
    function form_action (mode, id) {
       let frm = document.form1;

       if(mode == 'add'){
           frm.action = "{{url('/don-hang/them')}}";
           frm.method = 'post';
       }else if(mode == 'minus'){
           frm.action = "{{url('/don-hang/bot')}}";
           frm.method = 'post';
       }else if(mode == 'remove'){
           frm.action = "{{url('/don-hang/xoa')}}";
           frm.method = 'post';
       }

        frm.product_id.value = id
        frm.submit();
    }

</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/i.test(value);
        }, "Hãy nhập password từ 8 đến 16 ký tự bao gồm chữ hoa, chữ thường và ít nhất một chữ số");
        $.validator.addMethod("validatePhone", function (value, element) {
            return this.optional(element) || /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/i.test(value);
        }, "Hãy nhập số điện thoại chính xác");

        $( "form[name='register-form']" ).validate( {
            rules: {
                email: {
                    required: true,
                    maxlength: 50,
                    email:true
                },
                password: {
                    required: true,
                    maxlength: 16,
                    // validatePassword: 'password'
                },
                password_confirmation : {
                    required: true,
                    equalTo : "input[name='password']",
                    maxlength: 16
                },
                fullname: {
                    required: true,
                    maxlength: 100
                },
                phone: {
                    required: true,
                    maxlength: 20,
                    number: true,
                    // validatePhone:'phone'
                },
                address: {
                    required: true,
                    maxlength: 200
                },
            },
            messages: {
                email: {
                    email: "Hãy nhập vào địa chỉ email chính xác.",
                    required: "Hãy nhập vào địa chỉ email của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                },
                password: {
                    required: "Hãy nhập vào mật khẩu.",
                    maxlength: "Bạn chỉ được nhập tối đa 16 ký tự."
                },
                password_confirmation : {
                    required: "Hãy nhập lại mật khẩu một lần nữa.",
                    equalTo:"Nhập lại mật khẩu không chính xác.",
                    maxlength: "Bạn chỉ được nhập tối đa 16 ký tự."
                },
                fullname: {
                    required: "Hãy nhập vào Họ Tên của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                phone: {
                    required: "Hãy nhập vào số điện thoại của Khách Hàng.",
                    number: "Hãy nhập số điện thoại chính xác.",
                    maxlength: "Bạn chỉ được nhập tối đa 20 ký tự."
                },
                address: {
                    required: "Hãy nhập vào địa chỉ của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                },
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@stop
