@extends('layouts.default')

@section('title')
Sản Phẩm Yêu Thích
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
            Sản Phẩm Yêu Thích
        </div>

    </div>
    <div class="container-fluid">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
                Sản Phẩm Yêu Thích
            </div>

        </div>

        <div class="display-flow-root">
            @forelse ($favorites as $item)
            <div class="class-item-category">
                <div class="cover-img-icon-color">
                    <a href="{{ url($item->link) }}">
                        <img src="{{ asset('storage/'.$item->image) }}" class="p-2"  alt="{{ $item->name }}">
                    </a>
                    <div class="color-item-tag">
                        <a href="javascript:void(0)" onclick="form_action('order', {{$item->product}});" role="button"  class="data-image-small">
                            <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                        <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product}});"  class="data-image-small">
                            <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                    </div>
                </div>
                <div class="mt-3 pl-3">
                    <div class="name-item-category">
                        <a class="a-name-item" href="#"><h3 class="name-item-card">{{ $item->name }}</h3></a>
                    </div>
                    <div class="cover-text-money">
                        <span class="text-red-money20">
                            @if($item->discount!=-1)
                                {{ number_format($item->discount,0,'',',') }}
                            @else
                                {{ number_format($item->price,0,'',',') }}
                            @endif

                            <span class="fontsize-13">đ</span> </span>
                        <span class="text-money-del ml-2">
                            @if($item->discount!=-1)
                                {{ number_format($item->price,0,'',',') }}
                            @elseif(($item->discount>0))
                                {{ number_format($item->price,0,'',',') }}
                            @endif
                        </span>
                    </div>
                    </div>
                    <div style="height: 48px;">{{-- <div class="item-sale-discount">-3%</div>
                    <div class="item-sale-aqua-green">NEW</div>
                    <div class="item-sale-new">ĐỒNG GIÁ</div> --}}
                </div>
            </div>
            @empty

            @endforelse
        </div>
    </div>

@endsection

@section('javascript')
<script>
function form_action (mode, product_id) {
   let frm = document.form1;

   if(mode == 'order'){
       frm.action = "{{url('/don-hang/them')}}";
       frm.method = 'post';
   }else if(mode == 'favorite'){
        frm.action = "{{url('/khach-hang/add-favorite')}}";
        frm.method = 'get';
    }

    frm.product_id.value = product_id
    frm.submit();
}
</script>
@stop
