@extends('layouts.default')

@section('title','Tìm Kiếm')
@section('styles')
    <meta name="keywords" content="hang chinh hang, hàng chính hãng, tui xach, túi xách">
    <meta name="description" content="MIKSTORE là nơi chỉ cung cấp những hàng chính hãng tốt nhất, là lựa chọn tuyệt vời cho khách hàng..">
    <link rel="canonical" href="{{ url('') }}">
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
            TÌM KIẾM: {{ $key }}
        </div>

    </div>
    <div class="container-fluid">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> >
                TÌM KIẾM: {{ $key }}
                </a>
            </div>
        </div>
        <div class="display-flow-root">
            @forelse ($categories as $item)
            <div class="class-item-category">
                <div class="cover-img-icon-color">
                    <a href="{{ url($item->url_seo) }}">
                        <img id="5050BA0020" src="{{ asset('storage/'.$item->product_img_1) }}" class="p-2" alt="{{ $item->product_name }}">
                    </a>
                    <div class="color-item-tag">
                        <a href="javascript:void(0)" onclick="form_action('order', {{$item->product_id}});" role="button"  class="data-image-small">
                            <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                        <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product_id}});"  class="data-image-small">
                            <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                    </div>
                </div>
                <div class="mt-3 pl-3">
                    <div class="name-item-category">
                        <a class="a-name-item" href="{{ url($item->url_seo) }}"><h3 class="name-item-card">{{ $item->product_name }}</h3></a>
                    </div>
                    <div class="cover-text-money">
                        <span class="text-red-money20">
                            @if($item->product_discount!=-1)
                                {{ number_format($item->product_discount,0,'',',') }}
                            @else
                                {{ number_format($item->product_price,0,'',',') }}
                            @endif

                            <span class="fontsize-13">đ</span> </span>
                        <span class="text-money-del ml-2">
                            @if($item->product_discount!=-1)
                                {{ number_format($item->product_price,0,'',',') }}
                            @elseif(($item->product_discount>0))
                                {{ number_format($item->product_price,0,'',',') }}
                            @endif
                        </span>
                    </div>
                    </div>
                    <div style="height: 48px;">{{-- <div class="item-sale-discount">-3%</div>
                    <div class="item-sale-aqua-green">NEW</div>
                    <div class="item-sale-new">ĐỒNG GIÁ</div> --}}
                </div>
            </div>
            @empty
            <div class="d-flex align-items-center bd-highlight mb-3" style="height: 250px">
                <div class="p-2 bd-highlight">
                    <h1>Không tìm thấy sản phẩm với từ khóa '{{ $rkey }}'</h1>
                </div>
              </div>
            @endforelse
        </div>
    </div>

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="product_id" value="">
        </div>
    </form>

    </div>
    <!-- /Main Content -->
@endsection

@section('javascript')
<script>
    function form_action (mode, product_id) {
       let frm = document.form1;

       if(mode == 'order'){
           frm.action = "{{url('/don-hang/them')}}";
           frm.method = 'post';
       }else if(mode == 'favorite'){
       frm.action = "{{url('/khach-hang/add-favorite')}}";
       frm.method = 'get';
   }

        frm.product_id.value = product_id
        frm.submit();
    }

    function initialSetup() {
        if (document.getElementById("notificationCRUD") != null) {
            $("#notificationCRUD").delay(3000).fadeOut('slow');
        }
    }
    initialSetup();
    </script>
@stop
