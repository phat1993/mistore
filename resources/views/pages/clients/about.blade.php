@extends('layouts.default')

@section('title')
    Giới Thiệu
@endsection
@section('styles')
    <meta name="keywords" content="hang chinh hang, hàng chính hãng, tui xach, túi xách">
    <meta name="description" content="MIKSTORE là nơi chỉ cung cấp những hàng chính hãng tốt nhất, là lựa chọn tuyệt vời cho khách hàng..">
    <link rel="canonical" href="{{ url('/gioi-thieu') }}">
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
            {{ $about->web_info_title }}
        </div>

    </div>
    <div class="container">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
                {{ $about->web_info_title }}
            </div>

        </div>
        <div class="display-flow-root" style="padding-top: 5rem;padding-bottom: 5rem;">
            {!! $about->web_info_detail !!}
        </div>
    </div>

@endsection
