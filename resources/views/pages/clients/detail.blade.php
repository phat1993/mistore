@extends('layouts.default')

@section('title')
    {{ $product->web_title }}
@endsection
@section('styles')
    <meta name="keywords" content="{{ $product->web_keywords }}">
    <meta name="description" content="{{ $product->web_description }}">
    <link rel="canonical" href="{{ url($product->url_seo) }}">
@endsection

@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            @if($type_obj!=null && $category_obj!=null)
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a>>
                <a class="txt-color" href="{{ url('cua-hang/'.$type_obj->web_canonical) }}">{{ $type_obj->product_type_name }}</a>
                <a class="txt-color" href="{{ url('cua-hang/'.$type_obj->web_canonical.'/'.$category_obj->web_canonical) }}">>
                    {{ $category_obj->product_category_name }}
                </a> &gt;
                {{ $product->product_name }}
            @elseif($category_obj!=null)
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a>>
                <a class="txt-color" href="{{ url('danh-muc/'.$category_obj->web_canonical) }}">>
                    {{ $category_obj->product_category_name }}
                </a> &gt;
                {{ $product->product_name }}
            @else
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> &gt;
                {{ $product->product_name }}
            @endif
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('.owl-carousel-deal-soc').owlCarousel({
                autoplay: true,
                loop: false,
                margin: 10,
                nav: true,
                dots: false,
                lazyLoad: true,
                lazyLoadEager: true,
                responsive: {
                    0: {
                        slideBy: 2,
                        items: 2
                    },
                    850: {
                        slideBy: 4,
                        items: 4
                    },
                    1200: {
                        slideBy: 6,
                        items: 6
                    }
                }
            });

            $('.owl-carousel-mob-ban').owlCarousel({
                autoplay: false,
                loop: false,
                margin: 10,
                nav: true,
                slideBy: 1,
                dots: false,
                lazyLoad: true,
                lazyLoadEager: true,
                items: 1
            });
        });
    </script>

    <div class="container-fluid">
        <div class="title-nav-detail view-web">
            @if($type_obj!=null && $category_obj!=null)
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a>>
                <a class="txt-color" href="{{ url('cua-hang/'.$type_obj->web_canonical) }}">{{ $type_obj->product_type_name }}</a>
                <a class="txt-color" href="{{ url('cua-hang/'.$type_obj->web_canonical.'/'.$category_obj->web_canonical) }}">>
                    {{ $category_obj->product_category_name }}
                </a> &gt;
                {{ $product->product_name }}
            @elseif($category_obj!=null)
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a>>
                <a class="txt-color" href="{{ url('danh-muc/'.$category_obj->web_canonical) }}">>
                    {{ $category_obj->product_category_name }}
                </a> &gt;
                {{ $product->product_name }}
            @else
                <a class="txt-color" href="{{ url('/trang-chu') }}">TRANG CHỦ</a> &gt;
                {{ $product->product_name }}
            @endif
        </div>
            <div class="view-web-detail" style="width: 90%; margin: 0 auto;">
                <div class="d-flex">
                    <div class="width60 padding-right-20px">
                        <div class="item-list-image">
                            <div class="d-flex">
                                @if($product->product_img_1!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_1) }}">
                                        <img src="{{ asset('storage/'.$product->product_img_1) }}" class="float-left" style="padding: 0 1% 1% 1%;">
                                    </a>
                                @endif
                                @if($product->product_img_2!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_2) }}">
                                        <img src="{{ asset('storage/'.$product->product_img_2) }}" class="float-left" style="padding: 0 1% 1% 1%;">
                                    </a>
                                @endif
                            </div>
                            <div class="d-flex">
                                @if($product->product_img_3!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_3) }}">
                                        <img src="{{ asset('storage/'.$product->product_img_3) }}" class="float-left" style="padding: 0 1% 1% 1%;">
                                    </a>
                                @endif
                                @if($product->product_img_4!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_4) }}">
                                        <img src="{{ asset('storage/'.$product->product_img_4) }}" class="float-left" style="padding: 0 1% 1% 1%;">
                                    </a>
                                @endif
                            </div>
                            <div class="d-flex">
                                @if($product->product_img_5!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_5) }}" class="img-detail-sub2">
                                        <img src="{{ asset('storage/'.$product->product_img_5) }}" style="padding:1%;">
                                    </a>
                                @endif
                                @if($product->product_img_6!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_6) }}" class="img-detail-sub2">
                                        <img src="{{ asset('storage/'.$product->product_img_6) }}" style="padding:1%;">
                                    </a>
                                @endif
                                @if($product->product_img_7!='')
                                    <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_7) }}" class="img-detail-sub2">
                                        <img src="{{ asset('storage/'.$product->product_img_7) }}" style="padding:1%;">
                                    </a>
                                @endif
                                @if($product->product_video!='')
                                <div class="ratio ratio-1x1" style="padding: 0.5%">
                                        <iframe style="width: 15rem;height:15rem" src="{{ url($product->product_video) }}" title="{{ $product->product_name }}" allowfullscreen></iframe>
                                </div>
                                @endif
                            </div>
                        </div>
                        <hr style="margin: 25px 0px;">
                    </div>
                    <div class="width40 background-width" style="max-width: 650px; margin: 0 auto;">
                        <div class="info-item-detail">
                            <h1 class="title-item-detail">{{ $product->product_name }}</h1>
                            {{-- <div class="masp-item-detail" data-id="5050DX0029">Mã SP: DX0029</div> --}}
                            <div>
                                <span class="price-item-detail text-red-money20">
                                    @if($product->product_discount!=-1)
                                        {{ number_format($product->product_discount,0,'',',') }}
                                        <sup>đ</sup>
                                    @else
                                        {{ number_format($product->product_price,0,'',',') }}
                                        <sup>đ</sup>
                                    @endif
                                </span>
                                @if($product->product_discount>-1)
                                    <span class="price-sale-item-detail">
                                        {{ number_format($product->product_price,0,'',',') }}
                                        <sup>đ</sup>
                                    </span>
                                @endif
                                {{-- <span class="item-sale-discount">-40% </span>
                                <div class="text-red-money20 fontsize-16">Tiết kiệm: -178,000<sup>đ</sup></div> --}}
                            </div>
                            {{-- <div class="item-sale-new hhonline" style="display:none">Hết hàng online</div> --}}
                            {{-- <div class="color-item-detail">Màu sắc: Vàng tươi</div> --}}
                        </div>
                        <div class="cover-description">
                            <div class="discription-detail">MÔ TẢ SẢN PHẨM</div>
                            <div class="txt-description" style="word-wrap: break-word;">
                                {{ $product->product_description }}
                            </div>
                        </div>
                        <div class="row px-3">
                            <div class="col-lg-4 my-1"><a href="javascript:void(0)" onclick="form_action('order', {{$product->product_id}});" role="button" class="btn btn-danger form-control">THÊM GIỎ HÀNG</a></div>
                            <div class="col-lg-4 my-1"> <a href="javascript:void(0)" onclick="form_action('favorite', {{$product->product_id}});" class="btn btn-outline-danger form-control">YÊU THÍCH</a></div>
                            <div class="col-lg-4 my-1"><a href="#" class="btn btn-secondary form-control">LIÊN HỆ NGAY</a></div>
                        </div>
                        {{-- <div class="add-to-cart-detail background-colo-red" onclick="form_action('order', {{$product->product_id}});" role="button">THÊM GIỎ HÀNG</div>
                        <div class="add-to-cart-detail background-colo-red">YÊU THÍCH</div>
                         <div class="add-to-cart-detail background-colo-sab">LIÊN HỆ NGAY</div> --}}

                        <div class="cover-description">
                            <hr>
                            <div class="discription-detail">CHI TIẾT SẢN PHẨM</div>
                            <div class="txt-description">
                                {!! $product->product_detail !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="view-web" style="width: 90%; margin: 0 auto;">
                <div class="header-txt-detail">SẢN PHẨM ĐÃ XEM</div>
                <hr>
                <div class="owl-carousel owl-carousel-deal-soc owl-loaded owl-drag">

                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 283px;">
                            @foreach ($viewed as $item)
                            <div class="owl-item active" style="width: 272.615px; margin-right: 10px;">
                                <div class="cover-deal-soc-carousel">
                                        <div class="cover-item-carousel" >
                                            <a href="{{ url($item->link) }}">
                                                <img src="{{ asset('storage/'.$item->image) }}" class="p-2" />
                                            </a>
                                            <div class="color-item-tag">
                                                <a href="javascript:void(0)" onclick="form_action('order', {{$item->product}});" role="button"  class="data-image-small">
                                                    <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                                                <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product}});"  class="data-image-small">
                                                    <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                                            </div>
                                        </div>
                                        <div class="mt-3 pl-3">
                                            <a href="{{ url($item->link) }}" style="color: #6c757d; text-decoration: none;">
                                                <h3 class="name-item-card">{{ $item->name }}</h3>
                                            </a>
                                            <div class="cover-text-money">
                                                <span class="text-red-money20">
                                                    @if($item->discount!=-1)
                                                        {{ number_format($item->discount,0,'',',') }}
                                                        <span class="fontsize-13">đ</span>
                                                    @else
                                                        {{ number_format($item->price,0,'',',') }}
                                                        <span class="fontsize-13">đ</span>
                                                    @endif
                                                </span>
                                                @if($item->discount>-1)
                                                    <span class="text-money-del ml-2">
                                                        {{ number_format($item->price,0,'',',') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next disabled"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>


                <div class="header-txt-detail">SẢN PHẨM YÊU THÍCH</div>
                <hr>
                <div class="owl-carousel owl-carousel-deal-soc owl-loaded owl-drag">
                <div class="owl-stage-outer">
                    <div class="owl-stage" style="transform: translate3d(-1130px, 0px, 0px); transition: all 1s ease 0s; width: 2827px;">
                        @foreach ($favorites as $item)
                        <div class="owl-item active" style="width: 272.615px; margin-right: 10px;">
                            <div class="cover-deal-soc-carousel">
                                    <div class="cover-item-carousel" >
                                        <a href="{{ url($item->link) }}">
                                            <img src="{{ asset('storage/'.$item->image) }}" class="p-2" />
                                        </a>
                                        <div class="color-item-tag">
                                            <a href="javascript:void(0)" onclick="form_action('order', {{$item->product}});" role="button"  class="data-image-small">
                                                <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                                            <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product}});"  class="data-image-small">
                                                <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                                        </div>
                                    </div>
                                    <div class="mt-3 pl-3">
                                        <a href="{{ url($item->link) }}" style="color: #6c757d; text-decoration: none;">
                                            <h3 class="name-item-card">{{ $item->name }}</h3>
                                        </a>
                                        <div class="cover-text-money">
                                            <span class="text-red-money20">
                                                @if($item->discount!=-1)
                                                    {{ number_format($item->discount,0,'',',') }}
                                                    <span class="fontsize-13">đ</span>
                                                @else
                                                    {{ number_format($item->price,0,'',',') }}
                                                    <span class="fontsize-13">đ</span>
                                                @endif
                                            </span>
                                            @if($item->discount>-1)
                                                <span class="text-money-del ml-2">
                                                    {{ number_format($item->price,0,'',',') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="owl-nav disabled">
                    <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">‹</span></button>
                    <button type="button" role="presentation" class="owl-next disabled"><span aria-label="Next">›</span></button>
                </div>
                <div class="owl-dots disabled"></div>
            </div>
                <hr>
            </div>
            <div class="view-mob-detail">
                <div id="view-image-detail" class="view-image-detail">
                    <div class="owl-carousel owl-carousel-mob-ban mt-0 owl-loaded owl-drag">
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2220px;">

                                @if($product->product_img_1!='')
                                <div class="owl-item active" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_1) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_1) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_2!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_2) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_2) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_3!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_3) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_3) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_4!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_4) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_4) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_5!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                        <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_5) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_5) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_6!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_6) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_6) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_img_7!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <a data-fancybox="gallery" href="{{ url('storage/'.$product->product_img_7) }}">
                                                <img src="{{ asset('storage/'.$product->product_img_7) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($product->product_video!='')
                                <div class="owl-item" style="width: 360px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                        <div class="cover-deal-soc-carousel">
                                            <div class="ratio ratio-1x1" style="padding: 0.5%">
                                                <iframe style="width: 20rem;height:20rem" src="{{ url($product->product_video) }}" title="{{ $product->product_name }}" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="owl-nav">
                            <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">‹</span></button>
                            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                        </div>
                        <div class="owl-dots disabled"></div>
                    </div>
                </div>
                <div class="cover-description">
                    <h1 class="title-item-detail" style="font-size: 16px;">{{ $product->product_name }}</h1>
                    {{-- <div class="masp-item-detail" data-id="5050DX0029" style="font-size: 12px;">Mã SP: DX0029</div> --}}
                    <div>
                        <span class="price-item-detail text-red-money20">
                            @if($product->product_discount!=-1)
                                {{ number_format($product->product_discount,0,'',',') }}
                                <sup>đ</sup>
                            @else
                                {{ number_format($product->product_price,0,'',',') }}
                                <sup>đ</sup>
                            @endif
                        </span>
                        @if($product->product_discount>-1)
                            <span class="price-sale-item-detail">
                                {{ number_format($product->product_price,0,'',',') }}
                                <sup>đ</sup>
                            </span>
                        @endif
                        {{-- <span class="item-sale-discount">-40% </span>
                        <div class="text-red-money20 fontsize-16">Tiết kiệm: -178,000<sup>đ</sup></div> --}}
                    </div>
                    {{-- <div class="item-sale-new hhonline" style="display:none">Hết hàng online</div> --}}
                    {{-- <div class="color-item-detail" style="font-size: 12px;">Màu sắc: Vàng tươi</div> --}}
                </div>
                <div class="cover-description">
                    <div class="discription-detail">MÔ TẢ SẢN PHẨM</div>
                    <div class="txt-description" style="word-wrap: break-word;">
                        {{ $product->product_description }}
                    </div>
                </div>

                <a href="javascript:void(0)" onclick="form_action('order', {{$product->product_id}});" role="button" class="btn btn-danger form-control m-1">THÊM GIỎ HÀNG</a>
                <a href="javascript:void(0)" onclick="form_action('favorite', {{$product->product_id}});" class="btn btn-outline-danger form-control m-1">YÊU THÍCH</a>
                <a href="#" class="btn btn-secondary form-control m-1">LIÊN HỆ NGAY</a>
                {{-- <div class="add-to-cart-detail background-colo-red" onclick="form_action('order', {{$product->product_id}});" role="button">THÊM GIỎ HÀNG</div>
                <div class="add-to-cart-detail background-colo-sab">LIÊN HỆ NGAY</div> --}}
                <div class="grid-iconcard" style="background-color: #ffffff;height: 58px;">
                    {{-- <div class="buy-now-mob-cover">
                        <a onclick="goog_report_conversion('https://m.me/Sablanca.vn')" href="https://m.me/Sablanca.vn" target="_blank" rel="noopener">
                            <img src="/Images/icon/chat-gio hang.png" style="width: 75%">
                        </a>
                    </div>
                    <div style="border-right: 1px solid #929496; margin: 10px 0px;"></div>
                    <div class="buy-now-mob-cover">
                        <img id="btn-add-cart-grid" data-itemid="5050DX0029" data-itemname="Dép xẹp bít mũi nhọn phối nơ DX0029" data-colorid="BLK" class="" src="/Images/icon/cart-gio hang.png" style="width: 75%">
                    </div> --}}
                    <div style="width: 100%; text-align:center">
                        <div class="add-to-cart-detail background-colo-red" onclick="form_action('order', {{$product->product_id}});" role="button">THÊM GIỎ HÀNG</div>
                        {{-- <div class="add-to-cart-detail background-colo-red" style=" margin: 5px auto;">LIÊN HỆ NGAY</div> --}}
                    </div>
                </div>

                <div class="cover-description">
                    <hr>
                    <div class="discription-detail">CHI TIẾT SẢN PHẨM</div>
                    <div class="txt-description">
                        {!! $product->product_detail !!}
                    </div>
                </div>
                <br>
                <div class="new-arival-item-mob">
                    <div class="div-display-flow-root">
                        <div class="float-left"><span class="text-best-combo">SẢN PHẨM ĐÃ XEM</span></div>
                        <div class="float-right"><a href="{{ url('/khach-hang/viewed') }}"><span class="txt-color">Xem tất cả &gt;&gt;</span></a></div>
                    </div>
                    <div class="owl-carousel owl-carousel-deal-soc mt-3 owl-loaded owl-drag">

                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 169px;">
                            @foreach ($viewed as $item)
                                <div class="owl-item active" style="width: 272.615px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                            <div class="cover-item-carousel" >
                                                <a href="{{ url($item->link) }}">
                                                    <img src="{{ asset('storage/'.$item->image) }}" class="p-2" />
                                                </a>
                                                <div class="color-item-tag">
                                                    <a href="javascript:void(0)" onclick="form_action('order', {{$item->product}});" role="button"  class="data-image-small">
                                                        <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                                                    <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product}});"  class="data-image-small">
                                                        <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                                                </div>
                                            </div>
                                            <div class="mt-3 pl-3">
                                                <a href="{{ url($item->link) }}" style="color: #6c757d; text-decoration: none;">
                                                    <h3 class="name-item-card">{{ $item->name }}</h3>
                                                </a>
                                                <div class="cover-text-money">
                                                    <span class="text-red-money20">
                                                        @if($item->discount!=-1)
                                                            {{ number_format($item->discount,0,'',',') }}
                                                            <span class="fontsize-13">đ</span>
                                                        @else
                                                            {{ number_format($item->price,0,'',',') }}
                                                            <span class="fontsize-13">đ</span>
                                                        @endif
                                                    </span>
                                                    @if($item->discount>-1)
                                                        <span class="text-money-del ml-2">
                                                            {{ number_format($item->price,0,'',',') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="owl-nav disabled">
                            <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">‹</span></button>
                            <button type="button" role="presentation" class="owl-next disabled"><span aria-label="Next">›</span></button>
                        </div>
                        <div class="owl-dots disabled"></div>
                        </div>
                </div>
                <div class="new-arival-item-mob">
                    <div class="div-display-flow-root">
                        <div class="float-left"><span class="text-best-combo">SẢN PHẨM YÊU THÍCH</span></div>
                        <div class="float-right"><a href="{{ url('/khach-hang/favorites') }}"><span class="txt-color">Xem tất cả &gt;&gt;</span></a></div>
                    </div>
                    <div class="owl-carousel owl-carousel-deal-soc mt-3 owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1350px, 0px, 0px); transition: all 0.5s ease 0s; width: 1689px;">
                            @foreach ($favorites as $item)
                                <div class="owl-item active" style="width: 272.615px; margin-right: 10px;">
                                    <div class="cover-deal-soc-carousel">
                                            <div class="cover-item-carousel" >
                                                <a href="{{ url($item->link) }}">
                                                    <img src="{{ asset('storage/'.$item->image) }}" class="p-2" />
                                                </a>
                                                <div class="color-item-tag">
                                                    <a href="javascript:void(0)" onclick="form_action('order', {{$item->product}});" role="button"  class="data-image-small">
                                                        <span class="style-color-items"><i class="fa fa-cart-plus"></i></span></a>
                                                    <a href="javascript:void(0)" onclick="form_action('favorite', {{$item->product}});"  class="data-image-small">
                                                        <span class="style-color-items"><i class="fa fa-heart"></i></span></a>
                                                </div>
                                            </div>
                                            <div class="mt-3 pl-3">
                                                <a href="{{ url($item->link) }}" style="color: #6c757d; text-decoration: none;">
                                                    <h3 class="name-item-card">{{ $item->name }}</h3>
                                                </a>
                                                <div class="cover-text-money">
                                                    <span class="text-red-money20">
                                                        @if($item->discount!=-1)
                                                            {{ number_format($item->discount,0,'',',') }}
                                                            <span class="fontsize-13">đ</span>
                                                        @else
                                                            {{ number_format($item->price,0,'',',') }}
                                                            <span class="fontsize-13">đ</span>
                                                        @endif
                                                    </span>
                                                    @if($item->discount>-1)
                                                        <span class="text-money-del ml-2">
                                                            {{ number_format($item->price,0,'',',') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="owl-nav disabled">
                            <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">‹</span></button>
                            <button type="button" role="presentation" class="owl-next disabled"><span aria-label="Next">›</span></button>
                        </div>
                        <div class="owl-dots disabled"></div>
                        </div>
                </div>
            </div>
    </div>

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="product_id" value="">
        </div>
    </form>

    </div>
    <!-- /Main Content -->
    @endsection

    @section('javascript')
    <script>
    function form_action (mode, product_id) {
       let frm = document.form1;

       if(mode == 'order'){
           frm.action = "{{url('/don-hang/them')}}";
           frm.method = 'post';
       }else if(mode == 'favorite'){
           frm.action = "{{url('/khach-hang/add-favorite')}}";
           frm.method = 'get';
       }

        frm.product_id.value = product_id
        frm.submit();
    }

    </script>
    @stop
