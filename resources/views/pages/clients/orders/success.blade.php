@extends('layouts.default')

@section('title')
Thông Tin Đơn Hàng
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
            Thông Tin Đơn Hàng
        </div>

    </div>
    <div class="container">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
                Thông Tin Đơn Hàng
            </div>

        </div>
        <div class="display-flow-root">
            @isset($msgSuccess)
            <div class="alert alert-success" id="notificationCRUD" >{!! $msgSuccess !!}</div>

            <h1 class="pb-5 text-center">THÔNG TIN ĐƠN HÀNG <strong class="text-primary">{{ $showOr }}</strong></h1>
            <div class="row mb-5">
                <div class="col-5">
                    <table>
                        <tbody>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Khách Hàng:</th>
                                <td>{{ $shopOrder->fullname }}<hr></td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Điện Thoại:</th>
                                <td>{{ $shopOrder->phonenumber }}<hr></td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Email:</th>
                                <td>{{ $shopOrder->email }}<hr></td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Địa Chỉ:</th>
                                <td>{{ $shopOrder->address }}<hr></td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Thời Gian Nhận:</th>
                                <td>{{ $shopOrder->delivery_time }}<hr></td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: top;">Ghi Chú:</th>
                                <td>{{ $shopOrder->delivery_note }}<hr></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-7">
                    <!-- Product #1 -->
                    @foreach ($orders as $item)
                    <div class="shadow d-md-flex bg-white rounded border border-default mr-1 row mb-2">
                        <div class="p-1 image col-md-2 col-4">
                            <img src="{{ asset('storage/'.$item->image) }}" alt="">
                        </div>
                        <div class="p-1 description  align-self-start col-md-6 col-8">
                            <span>{{ $item->name }}</span>
                            <span>{{ $item->description }}</span>
                        </div>
                        <div class="col-md-2 col-6 p-2 align-self-center text-center h6">
                            <span class="badge badge-dark">Số Lượng {{ $item->quantity }}</span>
                        </div>
                        <div class="col-md-2 col-4 align-self-center">
                            <span class="text-danger">
                                @if($item->discount!=-1)
                                    {{ number_format($item->amount,0,'',',') }}<sup>đ</sup>
                                @else
                                    {{ number_format($item->total,0,'',',') }}<sup>đ</sup>
                                @endif
                            </span>
                            @if($item->discount>-1)
                                <span class="text-money-del">
                                    {{ number_format($item->total,0,'',',') }}<sup>đ</sup>
                                </span>
                            @endif
                        </div>
                    </div>
                    @endforeach

                    <div class="d-flex justify-content-between" style="margin-top: 20px">
                    <div>Tổng Tiền:</div>
                    <div style="font-weight: 100;"><span>{{ number_format($shopOrder->total,0,'',',') }}</span><sup>đ</sup></div>
                    </div>

                    @if($shopOrder->discount!=0)
                    <div class="d-flex justify-content-between">
                    <div>Giảm giá:</div>
                    <div style="color: #5EC3AE"><span>-{{ number_format($shopOrder->discount,0,'',',') }}</span><sup>đ</sup></div>
                    </div>
                    @endif


                    <div class="d-flex justify-content-between">
                    @isset($shopOrder->gift)
                        <div class="row w-100">
                            <div class="col-4">Mã/SĐT giảm giá:</div>
                            <div class="col-8 text-danger font-weight-bold">
                                {{ $shopOrder->gift }}
                            </div>
                        </div>
                        <div style="color: #5EC3AE" ><span>-{{ number_format($shopOrder->gift_discount ,0,'',',') }}</span><sup>đ</sup></div>
                    @endisset
                    </div>

                    <div class="d-flex justify-content-between mt-2">
                    <div>Thành tiền:</div>
                    <div style="font-weight: 100;"><span>{{ number_format($shopOrder->amount,0,'',',') }}</span><sup>đ</sup></div>
                    </div>
                    {{-- <div class="bdtietkiem d-flex justify-content-between">
                    <div class="hint">Phí ship (Vui lòng chọn tỉnh thành để xem phí ship):</div>
                    <div><span id="lb-ship-cost">0</span><sup>đ</sup></div>
                    </div> --}}
                    <hr>

                    <div class="d-flex justify-content-between">
                    <div>Tổng tiền thanh toán:</div>
                    <div style="color: red"><span>{{ number_format($shopOrder->amount,0,'',',') }}</span><sup>đ</sup></div>
                    </div>
                </div>
            </div>
            @endisset

            @isset($msgFail)
            <div class="row">
                <div class="alert alert-danger">{!!$msgFail!!}</div>
            </div>
            @endisset
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        function initialSetup() {
            if (document.getElementById("notificationCRUD") != null) {
                $("#notificationCRUD").delay(3000).fadeOut('slow');
            }
        }
        initialSetup();


    </script>
@stop

