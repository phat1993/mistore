@extends('layouts.default')

@section('title')
    Giỏ Hàng
@endsection
@section('content')

    <div class="nav-cata-mob">
        <div class="title-nav-detail w90">
            <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
            Giỏ Hàng
        </div>

    </div>
    <div class="container">
        <div class="d-flex nav-cata-web">
            <div class="title-nav-detail w90">
                <a class="txt-color" href="{{ url('/trang-chu') }}">Trang chủ</a> >
                Giỏ Hàng
            </div>

        </div>
        <div class="display-flow-root">
            @if($orders!=null && $orders->count()>0)
                <div class="row">
                    <div class="col-md-8 mb-5">
                        <!-- Product #1 -->
                        @foreach ($orders as $item)
                            <div class="shadow d-md-flex bg-white rounded border border-default mr-1 row mb-2">
                                <div class="p-1 image col-md-2 col-4">
                                    <img src="{{ asset('storage/'.$item->image) }}" alt="">
                                </div>
                                <div class="p-1 description  align-self-start col-md-4 col-8">
                                    <span>{{ $item->name }}</span>
                                    <span>{{ $item->description }}</span>
                                </div>
                                <div class="col-md-3 col-6 p-2 align-self-center h6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <a href="javascript:void(0)" onclick="form_action('minus', {{$item->id}});" role="button" class="input-group-text"><i class="fa fa-minus"></i></a>
                                        </div>
                                        <input type="number" class="form-control" style="text-align: center" readonly="readonly" aria-label="Amount (to the nearest dollar)"  value="{{ $item->quantity }}">
                                        <div class="input-group-append">
                                            <a href="javascript:void(0)" onclick="form_action('add', {{$item->id}});" role="button" class="input-group-text"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-4 align-self-center">
                                    <span class="text-danger">
                                        @if($item->discount!=-1)
                                            {{ number_format($item->amount,0,'',',') }}<sup>đ</sup>
                                        @else
                                            {{ number_format($item->total,0,'',',') }}<sup>đ</sup>
                                        @endif
                                    </span>
                                    @if($item->discount>-1)
                                        <span class="text-money-del">
                                            {{ number_format($item->total,0,'',',') }}<sup>đ</sup>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-1 col-2 p-2 align-self-center" style="margin-left:-1rem">
                                    <a href="javascript:void(0)" onclick="form_action('remove', {{$item->id}});" role="button" class="close">
                                        <span><i class="fa fa-close"></i></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach

                        <div class="d-flex justify-content-between" style="margin-top: 20px">
                            <div>Tổng Tiền:</div>
                            <div style="font-weight: 100;"><span>{{ number_format($order_total,0,'',',') }}</span><sup>đ</sup></div>
                        </div>

                        @if($order_discount!=0)
                        <div class="d-flex justify-content-between">
                            <div>Giảm giá:</div>
                            <div style="color: #5EC3AE"><span>-{{ number_format($order_discount,0,'',',') }}</span><sup>đ</sup></div>
                        </div>
                        @endif


                        <div class="d-flex justify-content-between">
                            @if($gift==null)
                                <form class="row w-100" action="{{ url('/don-hang/discount') }}" method="post">
                                    {{csrf_field()}}
                                    <div class="col-4">Mã/SĐT giảm giá:</div>
                                    <div class="col-8 d-flex flex-row">
                                        <div class="input-group-sm">
                                            {{-- is-invalid  is-valid--}}
                                            <input type="text" class="form-control input-sm" placeholder="Nhập mã hoặc sđt được giảm giá" aria-label="Nhập mã"
                                            name="giftcode" id="giftcode" aria-describedby="giftError" required>
                                            <small class="text-danger" id="giftError">
                                                @if(count($errors) > 0)
                                                    @foreach($errors->all() as $error)
                                                        {!! $error !!}
                                                    @endforeach
                                                @endif
                                            </small>
                                        </div>
                                        <div>
                                            <button class="btn btn-secondary btn-sm" type="submit" id="button-apply">Áp Dụng</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <div class="row w-100">
                                    <div class="col-4">Mã giảm giá/SĐT giảm giá:</div>
                                    <div class="col-8 text-danger font-weight-bold">
                                        {{ $gift['code'] }}
                                        <span class="pl-3">@if ($gift['pattern']==0)
                                            {{ $gift['percent'] }}%
                                        @endif</span>
                                    </div>
                                </div>
                            @endif
                            @if($gift!=null)
                            <div style="color: #5EC3AE" ><span>-{{ number_format($gift['discount'] ,0,'',',') }}</span><sup>đ</sup></div>
                            @endif
                        </div>

                        <div class="d-flex justify-content-between mt-2">
                            <div>Thành tiền:</div>
                            <div style="font-weight: 100;"><span>{{ number_format($order_amount,0,'',',') }}</span><sup>đ</sup></div>
                        </div>
                        {{-- <div class="bdtietkiem d-flex justify-content-between">
                            <div class="hint">Phí ship (Vui lòng chọn tỉnh thành để xem phí ship):</div>
                            <div><span id="lb-ship-cost">0</span><sup>đ</sup></div>
                        </div> --}}
                        <hr>

                        <div class="d-flex justify-content-between">
                            <div>Tổng tiền thanh toán:</div>
                            <div style="color: red"><span>{{ number_format($order_amount,0,'',',') }}</span><sup>đ</sup></div>
                        </div>
                    </div>

                    <div class="col-md-4 border border-warning p-2 mb-5">
                        <div class="infoCheckout">
                            <span>NHẬP THÔNG TIN NHẬN HÀNG</span>
                            <hr>
                        </div>
                        <form class="form" action="{{ url('/don-hang/kiem-tra') }}" method="post" name="checkout-form">
                             {{csrf_field()}}
                                @isset($shopOrder)
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Họ tên <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <input type="text" name="fullname" maxlength="100" value="{{ $shopOrder->fullname }}" class="form-control" placeholder="Người nhận hàng (bắt buộc)">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Phone <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <input type="tel" maxlength="20" name="phonenumber" value="{{ $shopOrder->phonenumber }}" class="form-control" placeholder="Số điện thoại (bắt buộc)">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Email:</span>
                                        </div>
                                        <input type="text" name="email" maxlength="50" value="{{ $shopOrder->email }}" class="form-control" placeholder="Email người nhận">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Địa Chỉ <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <textarea type="text" name="address" maxlength="200" class="form-control" placeholder="Địa chỉ nhận hàng(bắt buộc)">{{ $shopOrder->address }}</textarea>
                                    </div>
                                @else
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Họ tên <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <input type="text" name="fullname" maxlength="100" class="form-control" placeholder="Người nhận hàng (bắt buộc)">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Phone <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <input type="tel" maxlength="20" name="phonenumber" class="form-control" placeholder="Số điện thoại (bắt buộc)">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Email:</span>
                                        </div>
                                        <input type="text" name="email" maxlength="50" class="form-control" placeholder="Email người nhận">
                                    </div>
                                    <div class="input-group mb-3 mt-3">
                                        <div class="input-group-prepend info-group-persion">
                                            <span class="input-group-text">Địa Chỉ <span class="text-danger">(*)</span>:</span>
                                        </div>
                                        <textarea type="text" name="address" maxlength="200" class="form-control" placeholder="Địa chỉ nhận hàng(bắt buộc)"></textarea>
                                    </div>
                                @endisset

                                <div class="infoCheckout">
                                    <span>THỜI GIAN NHẬN HÀNG</span>
                                    <hr>
                                </div>
                                <div class="mb-3 d-flex flex-wrap">
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 2</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T2">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 3</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T3">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 4</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T4">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 5</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T5">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 6</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T6">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>Thứ 7</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="T7">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customcheckbox">
                                        <span><strong>CN</strong></span>
                                        <input type="checkbox" name="dWeekdays[]" onchange="checkNotNull()" checked value="CN">
                                        <span class="checkmark"></span>
                                    </label>
                                    <span class="text-danger d-none" id="checkMessage">Bạn cần chọn ít nhất một ngày trong tuần.</span>
                                </div>
                                <div class="mb-3 d-flex flex-wrap">
                                    <label class="deliveryradio">
                                        <span><strong>8H - 12H</strong></span>
                                        <input type="radio" name="dTimeday" value="8h-12h" checked="checked">
                                        <span class="deliverycheckmark mt-1"></span>
                                    </label>
                                    <label class="deliveryradio">
                                        <span><strong>12H - 17H</strong></span>
                                        <input type="radio" name="dTimeday" value="12h-17h">
                                        <span class="deliverycheckmark mt-1"></span>
                                    </label>
                                    <label class="deliveryradio">
                                        <span><strong>17H - 21H</strong></span>
                                        <input type="radio" name="dTimeday" value="17h-21h" >
                                        <span class="deliverycheckmark mt-1"></span>
                                    </label>
                                </div>
                            <div class="form-group">
                                <textarea name="delivery_note" maxlength="500" class="form-control" rows="3" placeholder="Ghi chú thêm (nếu có)">@isset($shopOrder){{ $shopOrder->delivery_note }}@endisset</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="sbCheckout" class="form-control btn btn-dark btn-lg">
                                    GỬI ĐƠN HÀNG
                                </button>
                            </div>

                            @if(count($errors) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($errors->all() as $error)
                                            <div class="alert alert-danger" >{{$error}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            @else
                <h1>Giỏ hàng đang trống. <a href="{{ url('/trang-chu') }}">Hãy tiếp tục mua sắm nào.</a></h1>
                <div style="height: 24rem"></div>
            @endisset
        </div>
    </div>

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="product_id" value="">
        </div>
    </form>
@endsection

@section('javascript')
<script>
    function checkNotNull() {
        var atLeastOneIsChecked = $('input[name="dWeekdays[]"]:checked').length > 0;
        if(atLeastOneIsChecked){
            $('#checkMessage').addClass('d-none');
            $('#sbCheckout').removeAttr('disabled','disabled');
        }
        else{
            $('#checkMessage').removeClass('d-none');
            $('#sbCheckout').attr('disabled','disabled');
        }

    }

    function form_action (mode, id) {
       let frm = document.form1;

       if(mode == 'add'){
           frm.action = "{{url('/don-hang/them')}}";
           frm.method = 'post';
       }else if(mode == 'minus'){
           frm.action = "{{url('/don-hang/bot')}}";
           frm.method = 'post';
       }else if(mode == 'remove'){
           frm.action = "{{url('/don-hang/xoa')}}";
           frm.method = 'post';
       }

        frm.product_id.value = id
        frm.submit();
    }

</script>
<!-- Validation Library-->
<script src="{{asset('/asset/js/jquery.validate.min.js')}}"></script>
<script>

    $( document ).ready( function () {
        checkNotNull();
        $( "form[name='checkout-form']" ).validate( {
            rules: {
                fullname: {
                    required: true,
                    maxlength: 100
                },
                phonenumber: {
                    required: true,
                    maxlength: 20,
                },
                email: {
                    email: true,
                },
                address: {
                    required: true,
                    maxlength: 200
                },
                delivery_note: {
                    maxlength: 500
                }
            },
            messages: {
                fullname: {
                    required: "Hãy nhập vào Họ Tên của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                phonenumber: {
                    required: "Hãy nhập vào số điện thoại của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 20 ký tự."
                },
                email: {
                    email: "Hãy nhập vào địa chỉ email chính xác.",
                },
                address: {
                    required: "Hãy nhập vào địa chỉ của Khách Hàng.",
                    maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                },
                delivery_note: {
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" ).attr('style','padding-left:6rem');

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
</script>
@stop
