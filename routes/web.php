<?php

use App\Http\Controllers\Admins\AdministratorController;
use App\Http\Controllers\Admins\AuthenticationController;
use App\Http\Controllers\Admins\BannerController;
use App\Http\Controllers\Admins\CustomerController;
use App\Http\Controllers\Admins\OrdersController;
use App\Http\Controllers\Admins\ProductCategoryController;
use App\Http\Controllers\Admins\ProductController;
use App\Http\Controllers\Admins\ProfileController;
use App\Http\Controllers\Admins\ShopGiftController;
use App\Http\Controllers\Admins\ShopMenuController;
use App\Http\Controllers\Admins\WebInfoController;

use App\Http\Controllers\Customers\IndexController;
use App\Http\Controllers\Customers\ClientOrdersController;
use App\Http\Controllers\Customers\ClientCustomerController;
use App\Http\Controllers\Customers\ClientAuthenticationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Client

Route::get('/', [IndexController::class, 'index'])->name('home');
Route::get('/trang-chu', [IndexController::class, 'index'])->name('index');
Route::get('cua-hang/{type_slug}', [IndexController::class, 'type'])->name('type.show');
Route::get('cua-hang/{type_slug}/{category_slug}', [IndexController::class, 'category'])->name('category.show');
Route::get('cua-hang/{type_slug}/{category_slug}/{slug}', [IndexController::class, 'detail'])->name('detail.show');

// //phong truong hop khong co doi tuong
Route::get('/danh-muc/{category_slug}', [IndexController::class, 'category'])->name('category.only');
Route::get('/danh-muc/{category_slug}/{slug}', [IndexController::class, 'detail'])->name('category.list');
Route::get('/san-pham/{slug}', [IndexController::class, 'detail'])->name('detail.only');

Route::get('/noi-bat/{trend_obj}', [IndexController::class, 'trend'])->name('trend.show');
Route::get('/tim-kiem', [IndexController::class, 'search'])->name('search.show');

Route::get('/gioi-thieu', [IndexController::class, 'about'])->name('about.show');

// Route::get('/search-product/{key}', [IndexController::class, 'searchProduct']);

//Customer
//Auth::routes(['verify' => true]);

Route::match(['get', 'post'], '/dang-nhap', [ClientAuthenticationController::class, 'login'])->name('client.login');
Route::match(['get', 'post'], '/dang-ky', [ClientAuthenticationController::class, 'register'])->name('client.register');;
Route::get('/lay-lai-mat-khau', [ClientCustomerController::class, 'forget']);

Route::resource('/khach-hang', AdministratorController::class)->middleware('auth:customers');
Route::get('/khach-hang/ho-so', [ClientCustomerController::class, 'profile'])->name('client.profile')->middleware('verified');;
Route::get('/khach-hang/add-favorite', [ClientCustomerController::class, 'addFavorite']);
Route::get('/khach-hang/viewed', [ClientCustomerController::class, 'productViewed']);
Route::get('/khach-hang/favorites', [ClientCustomerController::class, 'productFavorites']);
Route::get('/khach-hang/logout', [ClientAuthenticationController::class, 'logout'])->name('client.logout');
//End Customer
//Order

Route::get('/don-hang/danh-sach', [ClientOrdersController::class, 'listProduct']);
Route::post('/don-hang/them', [ClientOrdersController::class, 'addProduct']);
Route::post('/don-hang/bot', [ClientOrdersController::class, 'minusProduct']);
Route::post('/don-hang/xoa', [ClientOrdersController::class, 'removeProduct']);
Route::post('/don-hang/kiem-tra', [ClientOrdersController::class, 'viewCheckout']);
Route::post('/don-hang/thanh-toan', [ClientOrdersController::class, 'checkout']);

//End Order

//Help

Route::post('/don-hang/discount', [ClientOrdersController::class, 'discountProduct']);
Route::get('/don-hang/count', [ClientOrdersController::class, 'getCount']);


//End Help

//End Client

//-----------------------------------------------------------------------------------------------------
//Authentication-------------------------------
Route::match(['get', 'post'], '/admins', [AuthenticationController::class, 'login'])->name('admin.login');
Route::match(['get'], '/admins/logout', [AuthenticationController::class, 'logout'])->name('admin.logout');
// Route::get('/unauth', function () {
//     return view('layouts.unauthorized');
// })->name('unauth');
//END Authentication

//-----------------------------------------------------------------------------------------------------
//Profile-------------------------------
Route::match(['get'], '/admins/profile', [ProfileController::class, 'index'])->name('admin.profile')->middleware('auth:admins');
//END Profile

//------------------------------------------------------------------------------------------------------------------------------------

//--QUAN LY NHAN VIEN

// Administrators
Route::match('get', '/admins/administrator/edit', [AdministratorController::class, 'edit']);
Route::match('post', '/admins/administrator/update', [AdministratorController::class, 'update']);
Route::match('get', '/admins/administrator/show', [AdministratorController::class, 'show']);
Route::match('post', '/admins/administrator/destroy', [AdministratorController::class, 'destroy']);
Route::match('post', '/admins/administrator/delete', [AdministratorController::class, 'delete']);
Route::match('get', '/admins/administrator/recovered', [AdministratorController::class, 'recovered']);
Route::resource('/admins/administrator', AdministratorController::class)->middleware('auth:admins');
// end Administrators

//--END QUAN LY NHAN VIEN
//------------------------------------------------------------------------------------------------------------------------------------

//--QUAN LY WEBSITE

//webinfo
Route::match('get', '/admins/webinfo/edit', [WebInfoController::class, 'edit']);
Route::match('post', '/admins/webinfo/update', [WebInfoController::class, 'update']);
Route::match('get', '/admins/webinfo/show', [WebInfoController::class, 'show']);
Route::match('post', '/admins/webinfo/destroy', [WebInfoController::class, 'destroy']);
Route::match('post', '/admins/webinfo/delete', [WebInfoController::class, 'delete']);
Route::match('get', '/admins/webinfo/recovered', [WebInfoController::class, 'recovered']);
Route::match('get', '/admins/webinfo/demo/{web_info_id}', [WebInfoController::class, 'demo']);
Route::resource('/admins/webinfo', WebInfoController::class)->middleware('auth:admins');
//end webinfo
//shopmenu
Route::match('get', '/admins/shopmenu/edit', [ShopMenuController::class, 'edit']);
Route::match('post', '/admins/shopmenu/update', [ShopMenuController::class, 'update']);
Route::match('get', '/admins/shopmenu/show', [ShopMenuController::class, 'show']);
Route::match('post', '/admins/shopmenu/destroy', [ShopMenuController::class, 'destroy']);
Route::match('post', '/admins/shopmenu/delete', [ShopMenuController::class, 'delete']);
Route::match('get', '/admins/shopmenu/recovered', [ShopMenuController::class, 'recovered']);
Route::resource('/admins/shopmenu', ShopMenuController::class)->middleware('auth:admins');
//end shopmenu
//shopgift
Route::match('get', '/admins/shopgift/edit', [ShopGiftController::class, 'edit']);
Route::match('post', '/admins/shopgift/update', [ShopGiftController::class, 'update']);
Route::match('get', '/admins/shopgift/show', [ShopGiftController::class, 'show']);
Route::match('post', '/admins/shopgift/destroy', [ShopGiftController::class, 'destroy']);
Route::match('post', '/admins/shopgift/delete', [ShopGiftController::class, 'delete']);
Route::match('get', '/admins/shopgift/recovered', [ShopGiftController::class, 'recovered']);
Route::match('get', '/admins/shopgift/coderandom', [ShopGiftController::class, 'getRandom']);
Route::resource('/admins/shopgift', ShopGiftController::class)->middleware('auth:admins');
//end shopgift
//banners
Route::match('get', '/admins/banner/edit', [BannerController::class, 'edit']);
Route::match('post', '/admins/banner/update', [BannerController::class, 'update']);
Route::match('get', '/admins/banner/show', [BannerController::class, 'show']);
Route::match('post', '/admins/banner/destroy', [BannerController::class, 'destroy']);
Route::match('post', '/admins/banner/delete', [BannerController::class, 'delete']);
Route::match('get', '/admins/banner/recovered', [BannerController::class, 'recovered']);
Route::resource('/admins/banner', BannerController::class)->middleware('auth:admins');
//end banners

//--END QUAN LY WEBSITE
//------------------------------------------------------------------------------------------------------------------------------------

//--QUAN LY SAN PHAM
// ProductCategory
Route::match('get', '/admins/product-category/edit', [ProductCategoryController::class, 'edit']);
Route::match('post', '/admins/product-category/update', [ProductCategoryController::class, 'update']);
Route::match('get', '/admins/product-category/show', [ProductCategoryController::class, 'show']);
Route::match('post', '/admins/product-category/destroy', [ProductCategoryController::class, 'destroy']);
Route::match('post', '/admins/product-category/delete', [ProductCategoryController::class, 'delete']);
Route::match('get', '/admins/product-category/recovered', [ProductCategoryController::class, 'recovered']);
Route::resource('/admins/product-category', ProductCategoryController::class)->middleware('auth:admins');
// end ProductCategory


// Product
Route::match('get', '/admins/product/edit', [ProductController::class, 'edit']);
Route::match('post', '/admins/product/update', [ProductController::class, 'update']);
Route::match('get', '/admins/product/show', [ProductController::class, 'show']);
Route::match('post', '/admins/product/destroy', [ProductController::class, 'destroy']);
Route::match('post', '/admins/product/delete', [ProductController::class, 'delete']);
Route::match('get', '/admins/product/recovered', [ProductController::class, 'recovered']);
Route::match('get', '/admins/product/change-type/{product_type_id}', [ProductController::class, 'changeType']);
Route::resource('/admins/product', ProductController::class)->middleware('auth:admins');
// end Product

//--END QUAN LY SAN PHAM
//------------------------------------------------------------------------------------------------------------------------------------

//--QUAN LY KHACH HANG
// Customer
// Route::match('get', '/admins/customer/edit', [CustomerController::class, 'edit']);
// Route::match('post', '/admins/customer/update', [CustomerController::class, 'update']);
Route::match('get', '/admins/customer/show', [CustomerController::class, 'show']);
Route::match('post', '/admins/customer/active', [CustomerController::class, 'update']);
// Route::match('post', '/admins/customer/destroy', [CustomerController::class, 'destroy']);
// Route::match('post', '/admins/customer/delete', [CustomerController::class, 'delete']);
// Route::match('get', '/admins/customer/recovered', [CustomerController::class, 'recovered']);
Route::resource('/admins/customer', CustomerController::class)->middleware('auth:admins');
// end Customer


// Order
Route::match('post', '/admins/order/update', [OrdersController::class, 'update']);
Route::match('get', '/admins/order/show', [OrdersController::class, 'show']);
Route::match('post', '/admins/order/destroy', [OrdersController::class, 'destroy']);
Route::match('post', '/admins/order/delete', [OrdersController::class, 'delete']);
Route::match('get', '/admins/order/recovered', [OrdersController::class, 'recovered']);
Route::resource('/admins/order', OrdersController::class)->middleware('auth:admins');
// end Order


//--END QUAN LY KHACH HANG
//------------------------------------------------------------------------------------------------------------------------------------



////END ADMIN
